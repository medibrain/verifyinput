﻿Public Class frmComp

    Dim strProj As String = String.Empty '案件名
    Dim strMonth As String = String.Empty '年月
    Dim strProjID As String = String.Empty '案件ID
    Dim strName As String = String.Empty '入力者
    Dim InputCount As Integer = 1 '入力回数
    Dim ctlFirstFocus1 As New Control '最初のフォーカス移動するコントロールを取得
    Dim ctlFirstFocus2 As New Control '最初のフォーカス移動するコントロールを取得

    Private Enum FlgProc
        Insert
        Update
    End Enum

#Region "ページ移動プロパティ"

    Public Property page() As Integer
        Get
            Return Me.txtFind.Text
        End Get
        Set(ByVal value As Integer)
            Me.txtFind.Text = value
        End Set
    End Property
#End Region

#Region "コンストラクタ"
    Public Sub New(ByVal _strProj As String, _
                   ByVal _strMonth As String, _
                   ByVal _strProjID As String, _
                   ByVal _strName As String)

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        strProj = _strProj
        strMonth = _strMonth
        strProjID = _strProjID
        strName = _strName

    End Sub
#End Region

#Region "新規入力ボタン"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'ClearControl(True)
        LoadData(GetMaxID(InputCount))
        txtFind.Text = GetMaxID(InputCount)

        Select Case InputCount
            Case 1
                txtName1.Text = strName

            Case 2
                txtname2.Text = strName


        End Select

        Compare()

        txtname2.Text = strName
        txtpc2.Text = Environment.MachineName
        txttime2.Text = Now.ToString("yyyy/MM/dd HH:mm:ss")

    End Sub
#End Region

#Region "入力回数ボタン"
    Private Sub chkInp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInp.CheckedChanged
        If chkInp.Checked = False Then
            chkInp.Text = "入力回数　1回目"
            chkInp.ForeColor = Color.Blue
            SplitCnt.Panel2.Enabled = False
            SplitCnt.Panel1.Enabled = True

            InputCount = 1
            txtName1.Text = strName

        Else
            chkInp.Text = "入力回数　2回目"
            chkInp.ForeColor = Color.LimeGreen

            SplitCnt.Panel1.Enabled = False
            SplitCnt.Panel2.Enabled = True
            InputCount = 2
            txtname2.Text = strName
            txtInNo2.Text = txtInNo1.Text

        End If
        Compare()
        DispInfo()

    End Sub
#End Region

#Region "フォームキーダウン"
    'ページアップキーで登録処理
    Private Sub frmComp_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.PageUp Then
            Select Case InputCount
                Case 1 : btnEnt1_Click(sender, e)
                Case 2 : btnEnt2_Click(sender, e)
            End Select
        End If
    End Sub
#End Region

#Region "フォームロード"

    Private Sub frmComp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.KeyPreview = True


        SplitCnt.Panel2.Enabled = False
        Me.lblProj.Text = strProj
        Me.lblMonth.Text = strMonth

        ClearControl()
        txtInNo1.Text = GetMaxID(InputCount)
        txtFind.Text = GetMaxID(InputCount)
        Loadcmb()
        LoadData()
        UseFields()

        txtName1.Text = strName
        txtname2.Text = strName
        GetFirstCtl()

    End Sub
#End Region

#Region "1回目登録ボタン"

    Private Sub btnEnt1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnt1.Click

        '未入力チェック不要？
        If Not InputCheck() Then Exit Sub


        '登録処理
        If DataEntry(InputCount) = FlgProc.Insert Then
            ClearControl(True)
            txtInNo1.Text = GetMaxID(InputCount)

            '20160415162910 st ////////////////////////
            '最初のフォーカス移動するコントロールにフォーカスする
            'txtKigo1.Focus()
            ctlFirstFocus1.Focus()
            '20160415162910 ed ////////////////////////

        End If

    End Sub
#End Region

#Region "2回目登録ボタン"

    Private Sub btnEnt2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnt2.Click
        If Not InputCheck() Then Exit Sub



        '比較がFalseの場合抜ける
        If Not Compare() Then
            If MessageBox.Show("不一致がありますが登録しますか？", _
                               Application.ProductName, _
                               MessageBoxButtons.YesNo, _
                               MessageBoxIcon.Question, _
                               MessageBoxDefaultButton.Button2) = vbNo Then
                Exit Sub
            End If
        End If


        '登録処理
        If DataEntry(InputCount) = FlgProc.Insert Then
            ClearControl(True)
            txtInNo2.Text = GetMaxID(InputCount)

            LoadData(txtInNo2.Text)

            '20160415162939 st ////////////////////////
            '最初のフォーカス移動するコントロールにフォーカスする
            'txtKigo2.Focus()
            ctlFirstFocus2.Focus()
            '20160415162939 ed ////////////////////////

        End If

    End Sub
#End Region

#Region "郵便番号テキスト"

    Private Sub txtzip1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtzip1.Validating
        txtadd1.Text = GetAddress(InputCount, txtzip1)
    End Sub
    Private Sub txtzip2_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtzip2.Validating
        txtadd2.Text = GetAddress(InputCount, txtzip2)
    End Sub
#End Region

#Region "検索ボタン"

    Private Sub btnSpin2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSpin2.Click
        '20160415163244 st ////////////////////////
        '検索テキストが空白、または数字でない場合抜ける
        If txtFind.Text = String.Empty Then Exit Sub
        If Not IsNumeric(txtFind.Text) Then Exit Sub
        '20160415163244 ed ////////////////////////


        If Integer.Parse(GetMaxID(InputCount)) <= Integer.Parse(txtFind.Text) Then
            txtFind.Text = Integer.Parse(GetMaxID(InputCount))
        Else
            txtFind.Text = Integer.Parse(txtFind.Text) + 1
        End If
    End Sub

    Private Sub txtFind_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFind.TextChanged
        '20160415163148 st ////////////////////////
        '検索テキストが空白、または数字でない場合抜ける
        If txtFind.Text = String.Empty Then Exit Sub
        If Not IsNumeric(txtFind.Text) Then Exit Sub
        '20160415163148 ed ////////////////////////

        LoadData(txtFind.Text)
        'Compare()
        DispInfo()

    End Sub

    Private Sub btnSpin1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSpin1.Click
        '20160415163243 st ////////////////////////
        '検索テキストが空白、または数字でない場合抜ける
        If txtFind.Text = String.Empty Then Exit Sub
        If Not IsNumeric(txtFind.Text) Then Exit Sub
        '20160415163243 ed ////////////////////////

        If Integer.Parse(txtFind.Text) <= 1 Then
            txtFind.Text = 1
        Else
            txtFind.Text = Integer.Parse(txtFind.Text) - 1
        End If
    End Sub
#End Region

#Region "疑義コンボEnter押下時"

    Private Sub cmbgigi1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)

        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{tab}")
        End If
    End Sub
#End Region

#Region "削除ボタン"

    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click


        Dim arrsql As New ArrayList
        Dim strsql As String = String.Empty
        Dim clsdb As New clsDB
        Dim trans As SQLite.SQLiteTransaction = Nothing

        If MessageBox.Show("削除しますか？", _
                           Application.ProductName, _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question, _
                           MessageBoxDefaultButton.Button2) = vbNo Then Exit Sub



        strsql = "delete from inp1 where F001='" & strProjID & "' and F002='" & txtInNo1.Text & "'"
        arrsql.Add(strsql)

        strsql = "delete from inp2 where F001='" & strProjID & "' and F002='" & txtInNo2.Text & "'"
        arrsql.Add(strsql)

        If clsdb.SQL_Execute(arrsql, trans) Then
            MessageBox.Show("削除しました", _
                            Application.ProductName, _
                            MessageBoxButtons.OK, _
                            MessageBoxIcon.Information)
        End If

        LoadData()

        Me.chkDel.Checked = False

    End Sub

    '20160318163122 st ////////////////////////
    '削除ボタンを１，２回目に分ける
#Region "削除ボタン１回目"

    Private Sub btnDel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel1.Click
        Dim strsql As String = String.Empty
        Dim clsdb As New clsDB

        If MessageBox.Show("1回目入力を削除しますか？", _
                           Application.ProductName, _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question, _
                           MessageBoxDefaultButton.Button2) = vbNo Then Exit Sub



        strsql = "delete from inp1 where F001='" & strProjID & "' and F002='" & txtInNo1.Text & "'"

        If clsdb.SQL_Execute(strsql) Then
            MessageBox.Show("削除しました", _
                            Application.ProductName, _
                            MessageBoxButtons.OK, _
                            MessageBoxIcon.Information)
        End If

        LoadData()

    End Sub
#End Region

#Region "削除ボタン２回目"

    Private Sub btnDel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel2.Click
        Dim strsql As String = String.Empty
        Dim clsdb As New clsDB

        If MessageBox.Show("2回目入力を削除しますか？", _
                           Application.ProductName, _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question, _
                           MessageBoxDefaultButton.Button2) = vbNo Then Exit Sub



        strsql = "delete from inp2 where F001='" & strProjID & "' and F002='" & txtInNo2.Text & "'"

        If clsdb.SQL_Execute(strsql) Then
            MessageBox.Show("削除しました", _
                            Application.ProductName, _
                            MessageBoxButtons.OK, _
                            MessageBoxIcon.Information)
        End If

        LoadData()

    End Sub
#End Region
    '20160318163122 ed ////////////////////////

#End Region


#Region "疑義コンボ1"
    Private Sub txtGigi1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '   InputIndex(cmbgigi1, txtGigi1)
    End Sub
#End Region

#Region "疑義コンボ2"
    Private Sub txtGigi2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'InputIndex(cmbgigi2, txtGigi2)
    End Sub
#End Region




#Region "使用項目設定"
    Private Function UseFields() As Boolean
        Dim clsdb As New clsDB
        Dim strsql As String = String.Empty
        Dim dt As New DataTable
        Dim arrctl As New ArrayList

        dt = clsdb.SQL_Data("select * from usefields")
        If dt.Rows.Count = 0 Then Return False


        For r As Integer = 0 To dt.Rows.Count - 1
            For c As Integer = 2 To dt.Columns.Count - 2
                If dt.Rows(r)(c).ToString <> String.Empty And dt.Rows(r)("FlgUse") = False Then
                    arrctl.Add(dt.Rows(r)(c).ToString)
                End If
            Next
        Next

        For r As Integer = 0 To arrctl.Count - 1
            DisableControl(arrctl(r).ToString, Me.SplitCnt.Panel1)
            DisableControl(arrctl(r).ToString, Me.SplitCnt.Panel2)
        Next

    End Function
#End Region

#Region "コントロール不可化"
    Private Sub DisableControl(ByVal strctlname As String, ByVal cnt As Control)
        Dim ctl As Control
        For Each ctl In cnt.Controls
            If ctl.Name = strctlname Then
                ctl.Enabled = False
            End If
        Next
    End Sub
#End Region

#Region "情報ラベル"

    Private Sub DispInfo(Optional ByVal strInfo As String = "")
        lblInfo.Text = strInfo
    End Sub
#End Region

#Region "データロード"

    Private Function LoadData(Optional ByVal InpNo As Integer = 0) As Boolean

        Dim clsdb As New clsDB
        Dim dt As New DataTable("dt")
        Dim strsql As String = String.Empty

        Try
            'クリア
            ClearControl(True)


            strsql &= "SELECT "


            strsql &= "a.F001 as aF001,"
            strsql &= "a.F002 as aF002,"
            strsql &= "a.F003 as aF003,"
            strsql &= "a.F004 as aF004,"
            strsql &= "a.F005 as aF005,"
            strsql &= "a.F006 as aF006,"
            strsql &= "a.F007 as aF007,"

            '20160304104208 st ////////////////////////
            '疑義取得のSQL文を変更
            strsql &= "a.F008 as aF008,"
            'strsql &= "(select F003 from gigi where F001='" & strProjID & "' and F002=a.F008) as aF008,"
            '20160304104208 ed ////////////////////////


            strsql &= "a.F009 as aF009,"
            strsql &= "a.F010 as aF010,"
            strsql &= "a.F011 as aF011,"
            strsql &= "a.F012 as aF012,"
            strsql &= "a.F013 as aF013,"
            strsql &= "a.F014 as aF014,"
            strsql &= "a.F015 as aF015,"
            strsql &= "a.F016 as aF016,"
            strsql &= "a.F017 as aF017,"
            strsql &= "a.F018 as aF018,"

            strsql &= "a.F019 as aF019,"
            strsql &= "a.F020 as aF020,"
            strsql &= "a.F021 as aF021,"
            strsql &= "a.F022 as aF022,"
            strsql &= "a.F023 as aF023,"
            strsql &= "a.F024 as aF024,"
            strsql &= "a.F025 as aF025,"
            strsql &= "a.F026 as aF026,"
            strsql &= "a.F027 as aF027"

            strsql &= ",b.F001 as bF001,"

            strsql &= "b.F002 as bF002,"
            strsql &= "b.F003 as bF003,"
            strsql &= "b.F004 as bF004,"
            strsql &= "b.F005 as bF005,"
            strsql &= "b.F006 as bF006,"
            strsql &= "b.F007 as bF007,"

            '20160304104208 st ////////////////////////
            '疑義取得のSQL文を変更
            strsql &= "b.F008 as bF008,"

            'strsql &= "(select F003 from gigi where F001='" & strProjID & "' and F002=b.F008) as bF008,"
            '20160304104208 ed ////////////////////////

            strsql &= "b.F009 as bF009,"
            strsql &= "b.F010 as bF010,"
            strsql &= "b.F011 as bF011,"
            strsql &= "b.F012 as bF012,"
            strsql &= "b.F013 as bF013,"
            strsql &= "b.F014 as bF014,"
            strsql &= "b.F015 as bF015,"
            strsql &= "b.F016 as bF016,"
            strsql &= "b.F017 as bF017,"
            strsql &= "b.F018 as bF018,"

            strsql &= "b.F019 as bF019,"
            strsql &= "b.F020 as bF020,"
            strsql &= "b.F021 as bF021,"
            strsql &= "b.F022 as bF022,"
            strsql &= "b.F023 as bF023,"
            strsql &= "b.F024 as bF024,"
            strsql &= "b.F025 as bF025,"
            strsql &= "b.F026 as bF026,"
            strsql &= "b.F027 as bF027 "

            strsql &= " FROM INP1 a "
            strsql &= "LEFT JOIN INP2 b "
            strsql &= " ON a.F001=b.F001 "
            strsql &= " AND a.F002=b.F002 "



            '案件で抽出
            strsql &= "WHERE a.F001='" & strProjID & "'"


            If InpNo > 0 Then
                '検索の場合
                strsql &= " and aF002 =" & InpNo
            Else
                Select Case InputCount
                    Case 1
                        strsql &= " and aF002 =(select max(F002) as id from inp1 where F001='" & strProjID & "')+1"
                    Case 2
                        strsql &= " and bF002 =(select max(F002) as id from inp2 where F001='" & strProjID & "')+1"
                End Select
            End If



            'レコード取得
            dt = clsdb.SQL_Data(strsql)



            If dt.Rows.Count = 0 Then
                'ClearControl(True)
                txtInNo1.Text = txtFind.Text 'GetMaxID(1)
                txtInNo2.Text = txtFind.Text 'GetMaxID(2)
                txtMax1.Text = clsdb.SQL_ExecuteScalar("select count(*) as cnt from inp1 where F001='" & strProjID & "'")
                txtMax2.Text = clsdb.SQL_ExecuteScalar("select count(*) as cnt from inp2 where F001='" & strProjID & "'")


                Return True
            End If
            txtMax1.Text = clsdb.SQL_ExecuteScalar("select count(*) as cnt from inp1 where F001='" & strProjID & "'")
            txtMax2.Text = clsdb.SQL_ExecuteScalar("select count(*) as cnt from inp2 where F001='" & strProjID & "'")



            'データをセット
            SetData(txtInNo1, dt.Rows(0)("aF002"))
            SetData(txtKigo1, dt.Rows(0)("aF003"))
            SetData(txtHihoNm1, dt.Rows(0)("aF004"))
            SetData(txtJuryoNm1, dt.Rows(0)("aF005"))
            SetData(txtzip1, dt.Rows(0)("aF006"))
            SetData(txtadd1, dt.Rows(0)("aF007"))

            '20160304104041 st ////////////////////////
            'DBから取得した疑義を各チェックボックスへ代入
            SetDataChk(dt.Rows(0)("aF008"), 1)

            'SetData(cmbgigi1, dt.Rows(0)("aF008"))
            '20160304104041 ed ////////////////////////

            SetData(txtSejutu1, dt.Rows(0)("aF009"))
            SetData(txtMonth11, dt.Rows(0)("aF010"))
            SetData(txtMonth12, dt.Rows(0)("aF011"))
            SetData(txtMonth13, dt.Rows(0)("aF012"))
            SetData(txtNissu11, dt.Rows(0)("aF013"))
            SetData(txtNissu12, dt.Rows(0)("aF014"))
            SetData(txtNissu13, dt.Rows(0)("aF015"))

            SetData(txtGokei11, dt.Rows(0)("aF016"))
            SetData(txtGokei12, dt.Rows(0)("aF017"))
            SetData(txtGokei13, dt.Rows(0)("aF018"))
            SetData(txtFutan11, dt.Rows(0)("aF019"))
            SetData(txtFutan12, dt.Rows(0)("aF020"))
            SetData(txtFutan13, dt.Rows(0)("aF021"))
            SetData(txtSeikyu11, dt.Rows(0)("aF022"))
            SetData(txtSeikyu12, dt.Rows(0)("aF023"))
            SetData(txtSeikyu13, dt.Rows(0)("aF024"))


            SetData(txtName1, dt.Rows(0)("aF025"))
            SetData(txttime1, dt.Rows(0)("aF026"))
            SetData(txtPC1, dt.Rows(0)("aF027"))




            SetData(txtInNo2, dt.Rows(0)("bF002"))
            If txtInNo2.Text = String.Empty Then txtInNo2.Text = txtInNo1.Text

            SetData(txtKigo2, dt.Rows(0)("bF003"))
            SetData(txtHihoNm2, dt.Rows(0)("bF004"))
            SetData(txtJuryoNm2, dt.Rows(0)("bF005"))
            SetData(txtzip2, dt.Rows(0)("bF006"))
            SetData(txtadd2, dt.Rows(0)("bF007"))

            '20160304104041 st ////////////////////////
            'DBから取得した疑義を各チェックボックスへ代入
            SetDataChk(dt.Rows(0)("bF008"), 2)

            'SetData(cmbgigi2, dt.Rows(0)("bF008"))
            '20160304104041 ed ////////////////////////

            SetData(txtSejutu2, dt.Rows(0)("bF009"))
            SetData(txtMonth21, dt.Rows(0)("bF010"))
            SetData(txtMonth22, dt.Rows(0)("bF011"))
            SetData(txtMonth23, dt.Rows(0)("bF012"))
            SetData(txtNissu21, dt.Rows(0)("bF013"))
            SetData(txtNissu22, dt.Rows(0)("bF014"))
            SetData(txtNissu23, dt.Rows(0)("bF015"))


            SetData(txtGokei21, dt.Rows(0)("bF016"))
            SetData(txtGokei22, dt.Rows(0)("bF017"))
            SetData(txtGokei23, dt.Rows(0)("bF018"))
            SetData(txtFutan21, dt.Rows(0)("bF019"))
            SetData(txtFutan22, dt.Rows(0)("bF020"))
            SetData(txtFutan23, dt.Rows(0)("bF021"))
            SetData(txtSeikyu21, dt.Rows(0)("bF022"))
            SetData(txtSeikyu22, dt.Rows(0)("bF023"))
            SetData(txtSeikyu23, dt.Rows(0)("bF024"))


            SetData(txtname2, dt.Rows(0)("bF025"))
            SetData(txttime2, dt.Rows(0)("bF026"))
            SetData(txtpc2, dt.Rows(0)("bF027"))




            Compare()

            Return True


        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return False
        End Try


        Return True

    End Function
#End Region

#Region "疑義ロード"

    Private Function Loadcmb() As Boolean
        Dim clsdb As New clsDB
        Dim dt1 As New DataTable("dt1")
        Dim dt2 As New DataTable("dt2")
        Dim strsql As String = String.Empty

        Try
            strsql &= "SELECT "
            strsql &= "F002,F003 "
            strsql &= "From gigi "
            strsql &= "WHERE F001='" & strProjID & "'"

            dt1 = clsdb.SQL_Data(strsql)
            dt2 = clsdb.SQL_Data(strsql)

            '疑義内容がない場合は設定しない
            If dt1.Rows.Count = 0 Then Return True


            '20160303155902 st ////////////////////////
            'コンボボックスからチェックボックスに変更し、疑義内容をチェックボックスのテキストとして表示する
            Dim chk As CheckBox = Nothing

            For r As Integer = 0 To dt1.Rows.Count - 1

                '//20170428112756 furukawa st ////////////////////////
                '//疑義10個目以降無視
                If r >= 10 Then Exit For
                '//20170428112756 furukawa ed ////////////////////////


                '1回目用画面
                Dim chkName As String = "chkGigi1" & Integer.Parse(r).ToString("00")
                chk = Me.SplitCnt.Panel1.Controls.Find(chkName, True)(0)


                '文字列をセット＋可視化
                'If dt1.Rows.Count > r - 1 Then Exit For


                '//20170428112857 furukawa st ////////////////////////
                '//20文字以上の場合フォントを小さく
                Dim f As Font
                If dt1.Rows(r)("F003").ToString.Length > 20 Then
                    f = New Font("メイリオ", 8, FontStyle.Regular)
                    chk.Text = dt1.Rows(r)("F003").ToString.Substring(0, 20)
                Else
                    f = New Font("メイリオ", 9.75, FontStyle.Regular)
                    chk.Text = dt1.Rows(r)("F003").ToString
                End If
                chk.Font = f

                'chk.Text = dt1.Rows(r)("F003").ToString

                '//20170428112857 furukawa ed ////////////////////////


                chk.Visible = True


                '2回目用画面
                chkName = "chkGigi2" & Integer.Parse(r).ToString("00")
                chk = Me.SplitCnt.Panel2.Controls.Find(chkName, True)(0)


                '文字列をセット＋可視化
                chk.Text = dt1.Rows(r)("F003").ToString
                chk.Visible = True

            Next

            '20160303155902 ed ////////////////////////





            Dim dr2 As DataRow = dt2.NewRow
            dr2(0) = String.Empty
            dr2(1) = String.Empty
            dt2.Rows.InsertAt(dr2, 0)

            'cmbgigi2.DataSource = dt2
            'cmbgigi2.DisplayMember = "F003"
            'cmbgigi2.ValueMember = "F002"


            Return True


        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return False
        End Try

    End Function
#End Region

#Region "コントロールへ代入"

    ''' <summary>
    ''' テキストボックスコントロールへ代入
    ''' </summary>
    ''' <param name="txt"></param>
    ''' <param name="data"></param>
    ''' <remarks></remarks>
    Private Overloads Sub SetData(ByRef txt As Control, ByVal data As Object)
        If IsNothing(data) Then
            txt.Text = String.Empty
            Exit Sub
        End If
        If IsDBNull(data) Then
            txt.Text = String.Empty
            Exit Sub
        End If
        If String.IsNullOrEmpty(data) Then
            txt.Text = String.Empty
            Exit Sub
        End If
        If data = "0" Then
            txt.Text = String.Empty
            Exit Sub
        End If


        Dim dat As DateTime
        DateTime.TryParseExact(data, _
                                      "yyyyMMddHHmmss", _
                                      System.Globalization.CultureInfo.InvariantCulture, _
                                      Globalization.DateTimeStyles.AdjustToUniversal, _
                                      dat)
        If dat.Year > 1900 Then
            txt.Text = dat.ToString("yyyy/MM/dd HH:mm:ss")
            Exit Sub
        End If


        txt.Text = data.ToString
    End Sub

    ''' <summary>
    ''' コンボボックスへ取得データ代入
    ''' </summary>
    ''' <param name="txt"></param>
    ''' <param name="data"></param>
    ''' <remarks></remarks>
    Private Overloads Sub SetData(ByRef txt As ComboBox, ByVal data As Object)
        If IsNothing(data) Then
            txt.Text = String.Empty
            Exit Sub
        End If
        If IsDBNull(data) Then
            txt.Text = String.Empty
            Exit Sub
        End If
        If String.IsNullOrEmpty(data) Then
            txt.Text = String.Empty
            Exit Sub
        End If
        txt.Text = data.ToString
    End Sub


    '20160304103959 st ////////////////////////
    'チェックボックスへの代入
    ''' <summary>
    ''' チェックボックスへ取得データ代入
    ''' </summary>
    ''' <param name="data"></param>
    ''' <remarks></remarks>
    Private Overloads Sub SetDataChk(ByVal data As Object, ByVal inputCnt As Integer)

        Dim c As CheckBox

        Select Case inputCnt
            Case 1 '１回目入力のチェックボックスを設定

                'nothing,null,string.emptyの場合はチェックをすべてoff
                If IsNothing(data) OrElse IsDBNull(data) OrElse String.IsNullOrEmpty(data) Then
                    For Each ctl As Control In Me.SplitCnt.Panel1.Controls
                        If TypeOf (ctl) Is CheckBox Then
                            DirectCast(ctl, CheckBox).Checked = False
                        Else
                            Continue For
                        End If
                    Next

                Else
                    Dim arrChk() As String = Split(data, ",")
                    If arrChk.Length = 0 Then Exit Sub

                    For r As Integer = 0 To arrChk.Length - 1
                        c = Me.SplitCnt.Panel1.Controls.Find("chkGigi1" & Integer.Parse(arrChk(r)).ToString("00"), False)(0)
                        c.Checked = True
                    Next

                End If

            Case 2

                'nothing,null,string.emptyの場合はチェックをすべてoff
                If IsNothing(data) OrElse IsDBNull(data) OrElse String.IsNullOrEmpty(data) Then
                    For Each ctl As Control In Me.SplitCnt.Panel2.Controls
                        If TypeOf (ctl) Is CheckBox Then
                            DirectCast(ctl, CheckBox).Checked = False
                        Else
                            Continue For
                        End If
                    Next


                Else
                    Dim arrChk() As String = Split(data, ",")
                    If arrChk.Length = 0 Then Exit Sub

                    For r As Integer = 0 To arrChk.Length - 1
                        c = Me.SplitCnt.Panel2.Controls.Find("chkGigi2" & Integer.Parse(arrChk(r)).ToString("00"), False)(0)
                        c.Checked = True
                    Next

                End If


        End Select


    End Sub

    '20160304103959 ed ////////////////////////

#End Region

#Region "登録処理"
    Private Function DataEntry(ByVal cnt As Integer) As FlgProc
        Dim clsDB As New clsDB
        Dim strsql As String = String.Empty
        Dim strTable As String = String.Empty
        Dim dt As New DataTable
        Dim _flgproc As FlgProc

        Try
            strsql = String.Empty

            Select Case cnt
                Case 1

                    strTable = "inp1"

                    strsql &= "SELECT F001,F002 FROM " & strTable
                    strsql &= " WHERE F001='" & strProjID & "'"
                    strsql &= " AND F002=" & Me.txtInNo1.Text

                    dt = clsDB.SQL_Data(strsql)

                    strsql = String.Empty

                    If dt.Rows.Count = 0 Then


                        strsql &= "INSERT INTO " & strTable
                        strsql &= " VALUES ("

                        strsql &= "'" & strProjID & "',"
                        strsql &= "'" & GetMaxID(InputCount) & "',"
                        strsql &= "'" & txtKigo1.Text.Trim & "',"
                        strsql &= "'" & txtHihoNm1.Text.Trim & "',"
                        strsql &= "'" & txtJuryoNm1.Text.Trim & "',"
                        strsql &= "'" & txtzip1.Text.Trim & "',"
                        strsql &= "'" & txtadd1.Text.Trim & "',"

                        '20160304101132 st ////////////////////////
                        '疑義登録はチェックボックスに変更し、チェックされている番号を取得
                        strsql &= "'" & GetGigi() & "',"

                        'If cmbgigi1.Text = String.Empty Then
                        '    strsql &= "null,"
                        'Else
                        '    strsql &= "'" & cmbgigi1.SelectedValue.ToString() & "',"
                        'End If
                        '20160304101132 ed ////////////////////////

                        strsql &= "'" & txtSejutu1.Text.Trim & "',"
                        strsql &= "'" & txtMonth11.Text.Trim & "',"
                        strsql &= "'" & txtMonth12.Text.Trim & "',"
                        strsql &= "'" & txtMonth13.Text.Trim & "',"

                        strsql &= "'" & txtNissu11.Text.Trim & "',"
                        strsql &= "'" & txtNissu12.Text.Trim & "',"
                        strsql &= "'" & txtNissu13.Text.Trim & "',"

                        strsql &= "'" & txtGokei11.Text.Trim & "',"
                        strsql &= "'" & txtGokei12.Text.Trim & "',"
                        strsql &= "'" & txtGokei13.Text.Trim & "',"

                        strsql &= "'" & txtFutan11.Text.Trim & "',"
                        strsql &= "'" & txtFutan12.Text.Trim & "',"
                        strsql &= "'" & txtFutan13.Text.Trim & "',"

                        strsql &= "'" & txtSeikyu11.Text.Trim & "',"
                        strsql &= "'" & txtSeikyu12.Text.Trim & "',"
                        strsql &= "'" & txtSeikyu13.Text.Trim & "',"

                        strsql &= "'" & strName & "',"
                        strsql &= "'" & Now.ToString("yyyyMMddHHmmss") & "',"
                        strsql &= "'" & System.Environment.MachineName & "')"

                        _flgproc = FlgProc.Insert

                    Else

                        strsql &= "UPDATE " & strTable & " SET "
                        strsql &= "F003='" & txtKigo1.Text & "',"
                        strsql &= "F004='" & txtHihoNm1.Text & "',"
                        strsql &= "F005='" & txtJuryoNm1.Text & "',"
                        strsql &= "F006='" & txtzip1.Text & "',"
                        strsql &= "F007='" & txtadd1.Text & "',"


                        '20160304101132 st ////////////////////////
                        '疑義登録はチェックボックスに変更し、チェックされている番号を取得
                        strsql &= "F008='" & GetGigi() & "',"

                        'If cmbgigi1.Text = String.Empty Then
                        '    strsql &= "F008=null,"
                        'Else
                        '    strsql &= "F008='" & cmbgigi1.SelectedValue.ToString() & "',"
                        'End If
                        '20160304101132 ed ////////////////////////


                        strsql &= "F009='" & txtSejutu1.Text & "',"
                        strsql &= "F010='" & txtMonth11.Text & "',"
                        strsql &= "F011='" & txtMonth12.Text & "',"
                        strsql &= "F012='" & txtMonth13.Text & "',"
                        strsql &= "F013='" & txtNissu11.Text & "',"
                        strsql &= "F014='" & txtNissu12.Text & "',"
                        strsql &= "F015='" & txtNissu13.Text & "',"

                        strsql &= " F016='" & txtGokei11.Text.Trim & "',"
                        strsql &= " F017='" & txtGokei12.Text.Trim & "',"
                        strsql &= " F018='" & txtGokei13.Text.Trim & "',"

                        strsql &= " F019='" & txtFutan11.Text.Trim & "',"
                        strsql &= " F020='" & txtFutan12.Text.Trim & "',"
                        strsql &= " F021='" & txtFutan13.Text.Trim & "',"

                        strsql &= " F022='" & txtSeikyu11.Text.Trim & "',"
                        strsql &= " F023='" & txtSeikyu12.Text.Trim & "',"
                        strsql &= " F024='" & txtSeikyu13.Text.Trim & "',"

                        strsql &= "F025='" & txtName1.Text & "',"
                        strsql &= "F026='" & Now.ToString("yyyyMMddHHmmss") & "',"
                        strsql &= "F027='" & txtPC1.Text & "'"
                        strsql &= " where F001='" & strProjID & "' and F002='" & txtInNo1.Text & "'"

                        _flgproc = FlgProc.Update


                    End If


                Case 2
                    strTable = "inp2"



                    strsql &= "SELECT F001,F002 FROM " & strTable
                    strsql &= " WHERE F001='" & strProjID & "'"
                    strsql &= " AND F002=" & Me.txtInNo1.Text

                    dt = clsDB.SQL_Data(strsql)

                    strsql = String.Empty

                    If dt.Rows.Count = 0 Then

                        strsql &= "INSERT INTO " & strTable
                        strsql &= " VALUES ("

                        strsql &= "'" & strProjID & "',"
                        strsql &= "'" & GetMaxID(InputCount) & "',"
                        strsql &= "'" & txtKigo2.Text.Trim & "',"
                        strsql &= "'" & txtHihoNm2.Text.Trim & "',"
                        strsql &= "'" & txtJuryoNm2.Text.Trim & "',"
                        strsql &= "'" & txtzip2.Text.Trim & "',"
                        strsql &= "'" & txtadd2.Text.Trim & "',"

                        '20160304101132 st ////////////////////////
                        '疑義登録はチェックボックスに変更し、チェックされている番号を取得
                        strsql &= "'" & GetGigi() & "',"

                        'If cmbgigi2.Text = String.Empty Then
                        '    strsql &= "null,"
                        'Else
                        '    strsql &= "'" & cmbgigi2.SelectedValue.ToString() & "',"
                        'End If
                        '20160304101132 ed ////////////////////////


                        strsql &= "'" & txtSejutu2.Text.Trim & "',"
                        strsql &= "'" & txtMonth21.Text.Trim & "',"
                        strsql &= "'" & txtMonth22.Text.Trim & "',"
                        strsql &= "'" & txtMonth23.Text.Trim & "',"
                        strsql &= "'" & txtNissu21.Text.Trim & "',"
                        strsql &= "'" & txtNissu22.Text.Trim & "',"
                        strsql &= "'" & txtNissu23.Text.Trim & "',"


                        strsql &= "'" & txtGokei21.Text.Trim & "',"
                        strsql &= "'" & txtGokei22.Text.Trim & "',"
                        strsql &= "'" & txtGokei23.Text.Trim & "',"

                        strsql &= "'" & txtFutan21.Text.Trim & "',"
                        strsql &= "'" & txtFutan22.Text.Trim & "',"
                        strsql &= "'" & txtFutan23.Text.Trim & "',"

                        strsql &= "'" & txtSeikyu21.Text.Trim & "',"
                        strsql &= "'" & txtSeikyu22.Text.Trim & "',"
                        strsql &= "'" & txtSeikyu23.Text.Trim & "',"


                        strsql &= "'" & strName & "',"
                        strsql &= "'" & Now.ToString("yyyyMMddHHmmss") & "',"
                        strsql &= "'" & System.Environment.MachineName & "')"

                        _flgproc = FlgProc.Insert

                    Else

                        strsql &= "UPDATE " & strTable & " SET "
                        strsql &= "F002='" & txtInNo2.Text & "',"
                        strsql &= "F003='" & txtKigo2.Text & "',"
                        strsql &= "F004='" & txtHihoNm2.Text & "',"
                        strsql &= "F005='" & txtJuryoNm2.Text & "',"
                        strsql &= "F006='" & txtzip2.Text & "',"
                        strsql &= "F007='" & txtadd2.Text & "',"

                        '20160304101132 st ////////////////////////
                        '疑義登録はチェックボックスに変更し、チェックされている番号を取得
                        strsql &= "F008='" & GetGigi() & "',"

                        'If cmbgigi2.Text = String.Empty Then
                        '    strsql &= "F008=null,"
                        'Else
                        '    strsql &= "F008='" & cmbgigi2.SelectedValue.ToString() & "',"
                        'End If
                        '20160304101132 ed ////////////////////////

                        strsql &= "F009='" & txtSejutu2.Text & "',"
                        strsql &= "F010='" & txtMonth21.Text & "',"
                        strsql &= "F011='" & txtMonth22.Text & "',"
                        strsql &= "F012='" & txtMonth23.Text & "',"
                        strsql &= "F013='" & txtNissu21.Text & "',"
                        strsql &= "F014='" & txtNissu22.Text & "',"
                        strsql &= "F015='" & txtNissu23.Text & "',"

                        strsql &= " F016='" & txtGokei21.Text.Trim & "',"
                        strsql &= " F017='" & txtGokei22.Text.Trim & "',"
                        strsql &= " F018='" & txtGokei23.Text.Trim & "',"

                        strsql &= " F019='" & txtFutan21.Text.Trim & "',"
                        strsql &= " F020='" & txtFutan22.Text.Trim & "',"
                        strsql &= " F021='" & txtFutan23.Text.Trim & "',"

                        strsql &= " F022='" & txtSeikyu21.Text.Trim & "',"
                        strsql &= " F023='" & txtSeikyu22.Text.Trim & "',"
                        strsql &= " F024='" & txtSeikyu23.Text.Trim & "',"

                        strsql &= "F025='" & txtname2.Text & "',"
                        strsql &= "F026='" & Now.ToString("yyyyMMddHHmmss") & "',"
                        strsql &= "F027='" & txtpc2.Text & "' "
                        strsql &= " where F001='" & strProjID & "' and F002='" & txtInNo1.Text & "'"

                        _flgproc = FlgProc.Update

                    End If

            End Select


            clsDB.SQL_Execute(strsql)

            If _flgproc = FlgProc.Insert Then
                txtFind.Text = GetMaxID(InputCount)
                DispInfo("登録しました")
            Else
                DispInfo("更新しました")
            End If


            Return _flgproc

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)

        Finally
            clsDB = Nothing
        End Try



    End Function
#End Region

#Region "全コントロールの比較"
    Private Function Compare() As Boolean

        If Not ChkBlank() Then Return True


        Dim errflg As Boolean = False


        If Not CompareControl(Me.txtKigo1, Me.txtKigo2) Then errflg = True
        If Not CompareControl(Me.txtHihoNm1, Me.txtHihoNm2) Then errflg = True
        If Not CompareControl(Me.txtJuryoNm1, Me.txtJuryoNm2) Then errflg = True

        If Not CompareControl(Me.txtzip1, Me.txtzip2) Then errflg = True
        If Not CompareControl(Me.txtadd1, Me.txtadd2) Then errflg = True


        '疑義チェックボックス
        'If Not CompareControl(Me.cmbgigi1, Me.cmbgigi2) Then errflg = True
        If Not CompareControl(chkGigi100, chkGigi200) Then errflg = True
        If Not CompareControl(chkGigi101, chkGigi201) Then errflg = True
        If Not CompareControl(chkGigi102, chkGigi202) Then errflg = True
        If Not CompareControl(chkGigi103, chkGigi203) Then errflg = True
        If Not CompareControl(chkGigi104, chkGigi204) Then errflg = True
        If Not CompareControl(chkGigi105, chkGigi205) Then errflg = True
        If Not CompareControl(chkGigi106, chkGigi206) Then errflg = True
        If Not CompareControl(chkGigi107, chkGigi207) Then errflg = True
        If Not CompareControl(chkGigi108, chkGigi208) Then errflg = True
        If Not CompareControl(chkGigi109, chkGigi209) Then errflg = True




        If Not CompareControl(Me.txtSejutu1, Me.txtSejutu2) Then errflg = True

        '診療年月
        If Not CompareControl(Me.txtMonth11, Me.txtMonth21) Then errflg = True
        If Not CompareControl(Me.txtMonth12, Me.txtMonth22) Then errflg = True
        If Not CompareControl(Me.txtMonth13, Me.txtMonth23) Then errflg = True

        '日数
        If Not CompareControl(Me.txtNissu11, Me.txtNissu21) Then errflg = True
        If Not CompareControl(Me.txtNissu12, Me.txtNissu22) Then errflg = True
        If Not CompareControl(Me.txtNissu13, Me.txtNissu23) Then errflg = True

        '合計
        If Not CompareControl(Me.txtGokei11, Me.txtGokei21) Then errflg = True
        If Not CompareControl(Me.txtGokei12, Me.txtGokei22) Then errflg = True
        If Not CompareControl(Me.txtGokei13, Me.txtGokei23) Then errflg = True


        '一部負担金
        If Not CompareControl(Me.txtFutan11, Me.txtFutan21) Then errflg = True
        If Not CompareControl(Me.txtFutan12, Me.txtFutan22) Then errflg = True
        If Not CompareControl(Me.txtFutan13, Me.txtFutan23) Then errflg = True


        '請求金額
        If Not CompareControl(Me.txtSeikyu11, Me.txtSeikyu21) Then errflg = True
        If Not CompareControl(Me.txtSeikyu12, Me.txtSeikyu22) Then errflg = True
        If Not CompareControl(Me.txtSeikyu13, Me.txtSeikyu23) Then errflg = True


        If errflg Then
            DispInfo("不一致が発見されました")
            Return False
        End If

        Return True

    End Function
#End Region

#Region "比較（テキストボックス）"

    ''' <summary>
    ''' 比較（CstTxt用）
    ''' </summary>
    ''' <param name="ctl1"></param>
    ''' <param name="ctl2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Overloads Function CompareControl(ByRef ctl1 As CstTxt, ByRef ctl2 As CstTxt) As Boolean

        If (ctl1.Text <> ctl2.Text) Then
            ctl1.BackColor = Color.Coral
            ctl2.BackColor = Color.Coral
            Return False
        Else
            ctl1.BackColor = Nothing
            ctl2.BackColor = Nothing
            Return True

        End If

        Return True
    End Function
#End Region

    '20160304100936 st ////////////////////////
    'チェックボックス同士の比較
#Region "比較（チェックボックス）"
    ''' <summary>
    ''' 比較（チェックボックス用）
    ''' </summary>
    ''' <param name="ctl1"></param>
    ''' <param name="ctl2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Overloads Function CompareControl(ByRef ctl1 As CheckBox, ByRef ctl2 As CheckBox) As Boolean

        If (ctl1.Checked <> ctl2.Checked) Then
            ctl1.BackColor = Color.Coral
            ctl2.BackColor = Color.Coral
            Return False
        Else
            ctl1.BackColor = Nothing
            ctl2.BackColor = Nothing
            Return True

        End If

        Return True
    End Function

#End Region
    '20160304100936 ed ////////////////////////


#Region "比較（コンボ）"
    ''' <summary>
    ''' 比較（コンボ用）
    ''' </summary>
    ''' <param name="ctl1"></param>
    ''' <param name="ctl2"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Overloads Function CompareControl(ByRef ctl1 As ComboBox, ByRef ctl2 As ComboBox) As Boolean
        If (ctl1.Text <> ctl2.Text) Then
            ep.SetError(ctl1, "不一致")
            ep.SetError(ctl2, "不一致")
            Return False
        Else
            ep.Dispose()
            Return True

        End If

        Return True
    End Function
#End Region

#Region "ID取得"

    ''' <summary>
    ''' ID取得
    ''' </summary>
    ''' <param name="cnt">入力回数</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetMaxID(ByVal cnt As Integer) As String
        Dim res As String = String.Empty
        Dim clsDB As New clsDB
        Dim strsql As String = String.Empty
        Dim strTable As String = String.Empty

        Select Case cnt
            Case 1 : strTable = "inp1"
            Case 2 : strTable = "inp2"

        End Select

        strsql = "select max(cast(F002 as integer))+1 as maxid from " & strTable
        strsql &= " where F001='" & strProjID & "'"


        res = clsDB.SQL_ExecuteScalar(strsql)

        If res = String.Empty Then res = 1

        Return res

    End Function
#End Region

#Region "住所取得"

    Private Function GetAddress(ByVal cnt As Integer, ByRef sender As CstTxt) As String
        Dim res As String = String.Empty
        Dim clsDB As New clsDB
        Dim strsql As String = String.Empty

        strsql = "select F007 || F008 || F009 as address from zip"
        strsql &= " where F003='" & sender.Text & "'"

        res = clsDB.SQL_ExecuteScalar(strsql)
        Return res

    End Function
#End Region


#Region "空欄チェック（2回目入力パネルのみ）"
    ''' <summary>
    ''' 空欄チェック
    ''' </summary>
    ''' <returns>true:一つでも空白でない,false:対象パネル内コントロールが空白</returns>
    ''' <remarks></remarks>
    Private Function ChkBlank() As Boolean

        For Each ctl As Control In Me.SplitCnt.Panel2.Controls
            Select Case ctl.Name
                Case "txtInNo2", "txtMax2", "txtname2", "txtpc2", "txttime2"
                Case Else
                    If TypeOf (ctl) Is CstTxt Then
                        If ctl.Text <> String.Empty Then Return True
                    ElseIf TypeOf (ctl) Is ComboBox Then
                        If ctl.Text <> String.Empty Then Return True
                    End If
            End Select

        Next

        Return False


    End Function
#End Region

#Region "コントロールのクリア（パネル内）"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="flgAll">true:全項目,false:1,2回目別</param>
    ''' <remarks></remarks>
    Private Sub ClearControl(Optional ByVal flgAll As Boolean = False)

        If flgAll Then
            For Each ctl As Control In Me.SplitCnt.Panel1.Controls
                If TypeOf (ctl) Is CstTxt Then
                    ctl.Text = String.Empty
                    ctl.BackColor = Nothing

                ElseIf TypeOf (ctl) Is ComboBox Then
                    ctl.Text = String.Empty
                    ctl.BackColor = Nothing

                    '20160304103444 st ////////////////////////
                    'チェックボックスのクリア
                ElseIf TypeOf (ctl) Is CheckBox Then
                    DirectCast(ctl, CheckBox).Checked = False
                    ctl.BackColor = Nothing
                    '20160304103444 ed ////////////////////////

                End If

            Next
            For Each ctl As Control In Me.SplitCnt.Panel2.Controls
                If TypeOf (ctl) Is CstTxt Then
                    ctl.Text = String.Empty
                    ctl.BackColor = Nothing

                ElseIf TypeOf (ctl) Is ComboBox Then
                    ctl.Text = String.Empty
                    ctl.BackColor = Nothing

                    '20160304103444 st ////////////////////////
                    'チェックボックスのクリア
                ElseIf TypeOf (ctl) Is CheckBox Then
                    DirectCast(ctl, CheckBox).Checked = False
                    ctl.BackColor = Nothing
                    '20160304103444 ed ////////////////////////

                End If

            Next
        Else

            Select Case InputCount
                Case 1
                    For Each ctl As Control In Me.SplitCnt.Panel1.Controls
                        If TypeOf (ctl) Is CstTxt Then
                            ctl.Text = String.Empty
                            ctl.BackColor = Nothing

                        ElseIf TypeOf (ctl) Is ComboBox Then
                            ctl.Text = String.Empty
                            ctl.BackColor = Nothing

                        End If
                    Next

                Case 2
                    For Each ctl As Control In Me.SplitCnt.Panel2.Controls
                        If TypeOf (ctl) Is CstTxt Then
                            ctl.Text = String.Empty
                            ctl.BackColor = Nothing

                        ElseIf TypeOf (ctl) Is ComboBox Then
                            ctl.Text = String.Empty
                            ctl.BackColor = Nothing

                        End If
                    Next
            End Select
        End If


        '固定項目
        txtName1.Text = strName
        txtPC1.Text = Environment.MachineName
        txttime1.Text = Now.ToString("yyyy/MM/dd HH:mm:ss")

        txtname2.Text = strName
        txtpc2.Text = Environment.MachineName
        txttime2.Text = Now.ToString("yyyy/MM/dd HH:mm:ss")


    End Sub
#End Region


#Region "全項目空白チェック"
    ''' <summary>
    ''' 全項目空白チェック(1,2両方)
    ''' </summary>
    ''' <returns>false:一つでも空白でない場合,true:すべて空白</returns>
    ''' <remarks></remarks>
    Private Function ChkAllBlank() As Boolean
        Dim clsdb As New clsDB
        Dim dt As New DataTable
        Dim flgNotEmpty As Boolean = True

        dt = clsdb.SQL_Data("select * from UseFields")

        For r As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(r)("flgUse").ToString = "True" Then

                For c As Integer = 2 To dt.Columns.Count - 2
                    Dim ctl As Control
                    ctl = FindControl(dt.Rows(r)(c).ToString)
                    If IsNothing(ctl) Then Continue For

                    Select Case True
                        Case TypeOf (ctl) Is TextBox
                            If ctl.Text <> String.Empty Then flgNotEmpty = False
                        Case TypeOf (ctl) Is ComboBox
                            If DirectCast(ctl, ComboBox).SelectedIndex > 0 Then flgNotEmpty = False

                    End Select

                Next
            End If
        Next

        Return flgNotEmpty

    End Function
#End Region

#Region "未入力チェック"

    Private Function InputCheck() As Boolean

        If ChkAllBlank() Then
            If MessageBox.Show("全項目が未入力ですがよろしいですか？", _
                               Application.ProductName, _
                               MessageBoxButtons.YesNo, _
                               MessageBoxIcon.Question, _
                               MessageBoxDefaultButton.Button2) = vbNo Then
                Return False
            End If
        End If



        Return True

    End Function
#End Region

#Region "コントロール検索"

    Private Function FindControl(ByVal strName As String) As Control

        Select Case InputCount
            Case 1
                For Each ctl As Control In Me.SplitCnt.Panel1.Controls
                    If ctl.Name = strName Then
                        Return ctl
                    End If
                Next
            Case 2
                For Each ctl As Control In Me.SplitCnt.Panel2.Controls
                    If ctl.Name = strName Then
                        Return ctl
                    End If
                Next
        End Select
        Return Nothing

    End Function
#End Region

#Region "コンボボックスを入力値にあわせる"

    Private Sub InputIndex(ByVal cmb As ComboBox, ByVal txt As CstTxt)
        If txt.Text.Trim = String.Empty Then
            cmb.Text = String.Empty
            Exit Sub
        End If

        '入力した数字がコンボのアイテム数を超えない時、インデックスを合わせる
        If cmb.Items.Count > txt.Text Then
            cmb.SelectedIndex = txt.Text
        Else
            '上記以外は空文字
            cmb.Text = String.Empty
            Exit Sub
        End If
    End Sub
#End Region

#Region "入力済データ参照"

    Private Sub btnRef_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRef.Click
        Dim frmref As New frmRef(strProjID, strMonth, Me)
        frmref.Show()
    End Sub
#End Region


#Region "被保険者名コピー"

    Private Sub txtJuryoNm1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtJuryoNm1.KeyDown
        If e.KeyCode = Keys.ControlKey Then
            e.Handled = False
            txtJuryoNm1.Text = txtHihoNm1.Text
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtJuryoNm2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtJuryoNm2.KeyDown
        If e.KeyCode = Keys.ControlKey Then
            e.Handled = False
            txtJuryoNm2.Text = txtHihoNm2.Text
        Else
            e.Handled = True
        End If
    End Sub
#End Region

#Region "一時負担金自動算出"

    Private Sub txtSeikyu11_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _
        txtSeikyu11.KeyDown, txtSeikyu12.KeyDown, txtSeikyu13.KeyDown, _
        txtSeikyu21.KeyDown, txtSeikyu22.KeyDown, txtSeikyu23.KeyDown

        If DirectCast(sender, CstTxt).Text = String.Empty Then Exit Sub

        Select Case e.KeyCode
            Case Keys.Enter : CalcFutan(sender)

        End Select

    End Sub

    '負担金自動算出
    Private Sub CalcFutan(ByVal sender As CstTxt)
        Dim num As String
        num = sender.Name.Substring(sender.Name.Length - 2, 2)

        Dim pnl As SplitterPanel = Nothing
        Select Case InputCount
            Case 1
                pnl = Me.SplitCnt.Panel1
            Case 2
                pnl = Me.SplitCnt.Panel2
        End Select

        Dim ctlFutan As Control = pnl.Controls("txtFutan" & num)
        Dim ctlGokei As Control = pnl.Controls("txtGokei" & num)

        DirectCast(ctlFutan, CstTxt).Text = DirectCast(ctlGokei, CstTxt).Text - sender.Text

    End Sub
#End Region


    '20160304100824 st ////////////////////////
    'チェックされている疑義項目の番号を取得（SQL用）
#Region "疑義の取得"
    Private Function GetGigi() As String
        Dim c As Control = Nothing
        Dim res As String = String.Empty

        '入力回数によって見るパネルを変更
        Select Case InputCount
            Case 1
                For Each c In Me.SplitCnt.Panel1.Controls
                    If TypeOf (c) Is CheckBox Then
                        If DirectCast(c, CheckBox).Checked Then
                            'チェックボックス名の番号のみ取得
                            res &= c.Name.Substring(8) & ","
                        End If
                    End If
                Next
            Case 2
                For Each c In Me.SplitCnt.Panel2.Controls
                    If TypeOf (c) Is CheckBox Then
                        If DirectCast(c, CheckBox).Checked Then
                            'チェックボックス名の番号のみ取得
                            res &= c.Name.Substring(8) & ","
                        End If
                    End If
                Next

        End Select

        '最後のカンマを取る
        If res.Length > 1 Then res = res.Substring(0, res.Length - 1)

        Return res

    End Function
#End Region
    '20160304100824 ed ////////////////////////


    '20160304104905 st ////////////////////////
    'チェックボックスをEnterで次へ移動
#Region "チェックボックスをEnterで次へ"

    Private Sub chkGigi_EnterDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
        Handles chkGigi100.KeyDown, chkGigi101.KeyDown, chkGigi102.KeyDown, chkGigi103.KeyDown, chkGigi104.KeyDown, _
                chkGigi105.KeyDown, chkGigi106.KeyDown, chkGigi107.KeyDown, chkGigi108.KeyDown, chkGigi109.KeyDown, _
                chkGigi200.KeyDown, chkGigi201.KeyDown, chkGigi202.KeyDown, chkGigi203.KeyDown, chkGigi204.KeyDown, _
                chkGigi205.KeyDown, chkGigi206.KeyDown, chkGigi207.KeyDown, chkGigi208.KeyDown, chkGigi209.KeyDown


        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{tab}")
        End If
    End Sub
#End Region
    '20160304104905 ed ////////////////////////

    '20160322162013 st ////////////////////////
    '削除ボタン有効化チェックボックス
#Region "削除ボタン活性化チェック"
    Private Sub chk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDel.CheckedChanged
        Me.btnDel.Enabled = chkDel.Checked
    End Sub
#End Region
    '20160322162013 ed ////////////////////////


    '20160415162746 st ////////////////////////
    '最初のフォーカス移動するコントロールを取得
#Region "フォーカスの最初のコントロールを取得"
    Private Sub GetFirstCtl()

        Dim clsDB As New clsDB
        Dim dt As New DataTable
        dt = clsDB.SQL_Data("select CtlName1,CtlName2 from UseFields where FlgUse='1' order by OrderNo limit 1")
        ctlFirstFocus1.Name = dt.Rows(0)("CtlName1").ToString
        ctlFirstFocus2.Name = dt.Rows(0)("CtlName2").ToString

        For Each ctl As Control In Me.SplitCnt.Panel1.Controls
            If ctl.Name = ctlFirstFocus1.Name Then
                ctlFirstFocus1 = ctl
                Exit For
            End If
        Next
        For Each ctl As Control In Me.SplitCnt.Panel2.Controls
            If ctl.Name = ctlFirstFocus2.Name Then
                ctlFirstFocus2 = ctl
                Exit For
            End If
        Next

        dt.Dispose()


    End Sub
#End Region
    '20160415162746 ed ////////////////////////

End Class
