﻿Imports ForExcel
Imports System.Data.SQLite

Public Class frmMenu
    Dim clsDB As New clsDB

#Region "案件追加ボタン"

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim strSQL As String = String.Empty

        If Me.cmbProj.Text = String.Empty Then Exit Sub
        If Me.cmbMonth.Text = String.Empty Then Exit Sub

        '案件存在確認
        strSQL = String.Empty
        strSQL &= "select * from proj where F002='" & Me.cmbProj.Text.Trim & "' and F003='" & Me.cmbMonth.Text.Trim & "'"
        If clsDB.SQL_ExecuteScalar(strSQL) <> String.Empty Then
            MessageBox.Show("すでに登録されています。リストから選択してください。", _
                            Application.ProductName, _
                            MessageBoxButtons.OK, _
                            MessageBoxIcon.Exclamation)
            Exit Sub

        End If



        '案件ID取得
        strSQL = String.Empty
        strSQL &= "select max(F001) as id from proj"
        Dim id As String = clsDB.SQL_ExecuteScalar(strSQL)
        If id = String.Empty Then
            id = "0000"
        Else
            id = (Integer.Parse(id) + 1).ToString("0000")
        End If

        '追加
        strSQL = String.Empty
        strSQL &= "insert into proj values('" & id & "','" & Me.cmbProj.Text & "','" & Me.cmbMonth.Text & "')"

        If Not clsDB.SQL_Execute(strSQL) Then Exit Sub

        MessageBox.Show("追加しました", _
                        Application.ProductName, _
                        MessageBoxButtons.OK, _
                        MessageBoxIcon.Information)

        LoadData()

    End Sub
#End Region

#Region "フォームロード"

    Private Sub frmMenu_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadData()

    End Sub
#End Region

#Region "案件コンボ"

    Private Sub cmbProj_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProj.SelectedIndexChanged

        '案件コンボ変更時、点検月検索
        Dim strsql As String = String.Empty
        strsql = "select F001,F003 from proj where F002='" & Me.cmbProj.Text & _
                "'  order by cast(F003 as integer) desc"
        Dim dt As DataTable = clsDB.SQL_Data(strsql)

        If IsNothing(dt) Then Exit Sub

        cmbMonth.DataSource = dt
        cmbMonth.DisplayMember = "F003"
        cmbMonth.ValueMember = "F001"

    End Sub
#End Region

#Region "開始ボタン"

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        If IsNothing(Me.cmbProj.SelectedValue) Then
            MessageBox.Show("案件登録してください", _
                            Application.ProductName, _
                            MessageBoxButtons.OK, _
                            MessageBoxIcon.Information)
            Exit Sub
        End If

        If txtname.Text = String.Empty Then
            MessageBox.Show("入力者名を入力してください", _
                            Application.ProductName, _
                            MessageBoxButtons.OK, _
                            MessageBoxIcon.Exclamation)
            Exit Sub

        End If


        '//20170428100421 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得
        Dim frm As New frmComp(Me.cmbProj.Text, _
                               Me.cmbMonth.Text, _
                               GetProjID, _
                               Me.txtname.Text)


        'Dim frm As New frmComp(Me.cmbProj.Text, _
        '                       Me.cmbMonth.Text, _
        '                       Me.cmbProj.SelectedValue, _
        '                       Me.txtname.Text)
        '//20170428100421 furukawa ed ////////////////////////

        frm.Show()

    End Sub
#End Region

#Region "疑義メンテボタン"

    Private Sub btnGigi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGigi.Click
        If Me.cmbProj.Text = String.Empty Then
            Exit Sub
        End If
        If Me.cmbMonth.Text = String.Empty Then
            Exit Sub
        End If

        '//20170428100526 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得
        Dim dlg As New dlgGigi(GetProjID, Me.cmbMonth.Text.ToString, Me.cmbProj.Text)
        'Dim dlg As New dlgGigi(Me.cmbProj.SelectedValue.ToString, Me.cmbMonth.Text.ToString, Me.cmbProj.Text)
        '//20170428100526 furukawa ed ////////////////////////

        dlg.ShowDialog()

    End Sub

#End Region

#Region "案件削除ボタン"

    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        Dim arrsql As New ArrayList
        Dim trans As SQLiteTransaction = Nothing


        If Me.cmbProj.Text = String.Empty Then Exit Sub
        If Me.cmbMonth.Text = String.Empty Then Exit Sub

        '//20170428100709 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得し、削除

        '削除
        arrsql.Add("delete from  proj where F001='" & GetProjID() & "'")
        arrsql.Add("delete from inp1 where F001='" & GetProjID() & "'")
        arrsql.Add("delete from inp2 where F001='" & GetProjID() & "'")
        arrsql.Add("delete from gigi where F001='" & GetProjID() & "'")
        arrsql.Add("delete from output where F001='" & GetProjID() & "'")


        'arrsql.Add("delete from  proj where F001='" & Me.cmbProj.SelectedValue & _
        '"' and F002='" & Me.cmbProj.Text & "' and F003='" & Me.cmbMonth.Text & "'")
        'arrsql.Add("delete from inp1 where F001='" & Me.cmbProj.SelectedValue & "'")
        'arrsql.Add("delete from inp2 where F001='" & Me.cmbProj.SelectedValue & "'")
        'arrsql.Add("delete from gigi where F001='" & Me.cmbProj.SelectedValue & "'")
        'arrsql.Add("delete from output where F001='" & Me.cmbProj.SelectedValue & "'")

        '//20170428100709 furukawa ed ////////////////////////


        If MessageBox.Show("この案件を削除しますか？" & vbCrLf & _
                           "削除すると、この案件の入力データ・疑義・出力用データも削除されます。" & vbCrLf & _
                           "よろしいですか？", _
                           Application.ProductName, _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question, _
                           MessageBoxDefaultButton.Button2) = vbNo Then Exit Sub


        If Not clsDB.SQL_Execute(arrsql, trans) Then Exit Sub

        MessageBox.Show("削除しました", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        LoadData()

    End Sub
#End Region

#Region "使用項目設定ボタン"

    Private Sub btnFields_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFields.Click
        dlgFields.ShowDialog()

    End Sub
#End Region


#Region "コンボEnter押下時"

    Private Sub cmbMonth_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles _
    cmbMonth.KeyDown, cmbProj.KeyDown


        If e.KeyCode = Keys.Enter Then
            SendKeys.Send("{tab}")
        End If

    End Sub
#End Region


#Region "データロード"

    Private Sub LoadData()
        '案件コンボ作成
        Dim strsql As String = String.Empty
        strsql = "select F001,F002 from proj group by F002 order by F002,F003 "
        Dim dt As DataTable = clsDB.SQL_Data(strsql)

        cmbProj.Text = String.Empty
        cmbMonth.Text = String.Empty


        If IsNothing(dt) Then Exit Sub

        cmbProj.DataSource = dt
        cmbProj.DisplayMember = "F002"
        cmbProj.ValueMember = "F001"

        lblVer.Text = "Ver.1.02"
    End Sub
#End Region

#Region "出力ボタン"

    Private Sub btnOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOut.Click
        Dim clsdb As New clsDB
        Dim strsql As String = String.Empty
        Dim arrsql As New ArrayList
        Dim trans As SQLite.SQLiteTransaction = Nothing


        '件数チェック
        Dim cnt1 As Integer = 0
        Dim cnt2 As Integer = 0

        '//20170428100826 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得
        strsql = "select count(F002) as cnt1 from inp1 where F001='" & GetProjID() & "'"
        cnt1 = clsdb.SQL_ExecuteScalar(strsql).ToString

        strsql = "select count(F002) as cnt1 from inp2 where F001='" & GetProjID() & "'"
        cnt2 = clsdb.SQL_ExecuteScalar(strsql).ToString

        'strsql = "select count(F002) as cnt1 from inp1 where F001='" & Me.cmbProj.SelectedValue & "'"
        'cnt1 = clsdb.SQL_ExecuteScalar(strsql).ToString

        'strsql = "select count(F002) as cnt1 from inp2 where F001='" & Me.cmbProj.SelectedValue & "'"
        'cnt2 = clsdb.SQL_ExecuteScalar(strsql).ToString
        '//20170428100826 furukawa ed ////////////////////////

        '件数
        If cnt1 = 0 Then
            MessageBox.Show("出力件数が０件です", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        '件数整合性
        If cnt1 <> cnt2 Then
            MessageBox.Show("１回目と２回目の件数が違います", Application.ProductName, _
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        '入力チェック
        If Not ChkInp() Then
            MessageBox.Show("１回目と２回目の入力が合致しません。再度確認してください。", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub

        End If


        '出力テーブル作成
        strsql = "delete from output"
        arrsql.Add(strsql)

        '出力テーブルに、１回めのテーブルをコピー
        strsql = String.Empty
        strsql &= "insert into output ("
        strsql &= "F001,"
        strsql &= "F002,"
        strsql &= "F003,"
        strsql &= "F004,"
        strsql &= "F005,"
        strsql &= "F006,"
        strsql &= "F007,"
        strsql &= "F008,"
        strsql &= "F009,"
        strsql &= "F010,"
        strsql &= "F011,"
        strsql &= "F012,"
        strsql &= "F013,"
        strsql &= "F014,"
        strsql &= "F015,"
        strsql &= "F016,"

        '20160107162237 st ////////////////////////
        'Excelに全部出す
        strsql &= "F017,"
        strsql &= "F018,"
        strsql &= "F019,"
        strsql &= "F020,"
        strsql &= "F021,"
        strsql &= "F022,"
        strsql &= "F023,"
        strsql &= "F024,"
        strsql &= "F025 "
        '20160107162237 ed ////////////////////////

        strsql &= ")"
        strsql &= "select "
        strsql &= "i.F001,"
        strsql &= "i.F002,"
        strsql &= "'" & Me.cmbMonth.Text.Trim & "',"
        strsql &= "i.F003,"
        strsql &= "i.F004,"
        strsql &= "i.F005,"
        strsql &= "case when i.F006<>'' then substr(i.F006,1,3)||'-'||substr(i.F006,4,4) else null end, "
        strsql &= "i.F007,"

        strsql &= "i.F008,"
        'strsql &= "g.F003,"

        strsql &= "i.F009,"
        strsql &= "case when i.F010<>'' then substr(i.F010,1,2)||'年'||substr(i.F010,3,2)||'月' else null end,"
        strsql &= "case when i.F011<>'' then substr(i.F011,1,2)||'年'||substr(i.F011,3,2)||'月' else null end,"
        strsql &= "case when i.F012<>'' then substr(i.F012,1,2)||'年'||substr(i.F012,3,2)||'月' else null end,"
        strsql &= "i.F013,"
        strsql &= "i.F014,"
        strsql &= "i.F015,"

        '20160107162237 st ////////////////////////
        'Excelに全部出す
        strsql &= "i.F016,"
        strsql &= "i.F017,"
        strsql &= "i.F018,"
        strsql &= "i.F019,"
        strsql &= "i.F020,"
        strsql &= "i.F021,"
        strsql &= "i.F022,"
        strsql &= "i.F023,"
        strsql &= "i.F024 "

        '20160107162237 ed ////////////////////////




        strsql &= " FROM INP1 I LEFT JOIN GIGI G ON I.F001=G.F001 AND I.F008=G.F002"

        '//20170428100933 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得し、出力データ作成
        strsql &= " WHERE I.F001='" & GetProjID() & "' ORDER BY I.F002"
        'strsql &= " WHERE I.F001='" & Me.cmbProj.SelectedValue.ToString & "' ORDER BY I.F002"
        '//20170428100933 furukawa ed ////////////////////////

        arrsql.Add(strsql)

        If Not clsdb.SQL_Execute(arrsql, trans) Then Exit Sub

        '疑義更新
        If Not UpdateGigi(trans) Then Exit Sub



        Dim dt As New DataTable
        dt = clsDB.SQL_Data("select F002,F003,F004,F005,F006,F007,F008,F009,F010,F011,F012,F013," & _
                             "F014,F015,F016,F017,F018,F019,F020,F021,F022,F023,F024,F025 from output")

        Dim dr As DataRow = dt.NewRow
        ' dr("F001") = cmbProj.Text
        dr("F002") = "No."
        dr("F003") = "点検月"
        dr("F004") = "記号番号"
        dr("F005") = "被保険者名"
        dr("F006") = "受療者名"
        dr("F007") = "郵便番号"
        dr("F008") = "住所"
        dr("F009") = "疑義内容"
        dr("F010") = "施術院"
        dr("F011") = "診療月1"
        dr("F012") = "診療月2"
        dr("F013") = "診療月3"
        dr("F014") = "実日数1"
        dr("F015") = "実日数2"
        dr("F016") = "実日数3"

        '20160107162237 st ////////////////////////
        'Excelに全部出す
        dr("F017") = "合計1"
        dr("F018") = "合計2"
        dr("F019") = "合計3"
        dr("F020") = "一部負担金1"
        dr("F021") = "一部負担金2"
        dr("F022") = "一部負担金3"
        dr("F023") = "請求金額1"
        dr("F024") = "請求金額2"
        dr("F025") = "請求金額3"
        '20160107162237 ed ////////////////////////

        'ヘッダ文字列
        dt.Rows.InsertAt(dr, 0)


        '出力方法選択
        Select Case True
            Case Me.r1.Checked
                'excel
                Dim clsex As New clsExcel

                If Not clsex.OpenWorkBook(True) Then
                    MessageBox.Show("Excel出力を中止しました", _
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                clsex.ExportData(dt)
                clsex.SaveWorkBook(False)

            Case Me.r2.Checked
                'csv
                Dim sfd As New SaveFileDialog
                sfd.FileName = Now.ToString("yyyyMMddHHmmss") & "_" & Me.cmbProj.Text & Me.cmbMonth.Text
                sfd.AddExtension = True
                sfd.DefaultExt = ".csv"
                sfd.Filter = "CSVファイル|*.csv"

                If sfd.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    MessageBox.Show("CSV出力を中止しました", _
                                   Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Exit Sub
                End If

                Dim sw As New System.IO.StreamWriter(sfd.FileName, False, System.Text.Encoding.GetEncoding("SHIFT-JIS"))

                For r As Integer = 0 To dt.Rows.Count - 1
                    Dim tmp As String = String.Empty
                    For c As Integer = 0 To dt.Columns.Count - 1
                        tmp &= """" & dt.Rows(r)(c).ToString & """" & ","
                    Next
                    tmp = tmp.Substring(0, tmp.Length - 1)
                    sw.WriteLine(tmp)
                Next
                sw.Close()
                MessageBox.Show("CSVを出力しました。", _
                                Application.ProductName, _
                                MessageBoxButtons.OK, _
                                MessageBoxIcon.Information)


        End Select



    End Sub
#End Region

#Region "疑義更新"
    '20160308145715 st ////////////////////////
    '入力された疑義を文言に更新する
    Private Function UpdateGigi(ByRef trans As SQLiteTransaction) As Boolean

        '疑義内容変換SQL
        Dim strsql As String = String.Empty

        strsql = String.Empty

        '//20170428101153 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得
        strsql &= "select * from output where F001='" & GetProjID() & "'"
        'strsql &= "select * from output where F001='" & Me.cmbProj.SelectedValue & "'"
        '//20170428101153 furukawa ed ////////////////////////

        Dim dtOut As New DataTable
        dtOut = clsDB.SQL_Data(strsql)

        Dim arrGigiSQL As New ArrayList

        For Each drOut As DataRow In dtOut.Rows
            '入力番号
            Dim inNo As Integer = 0
            '疑義の値
            Dim strgigi() As String = Nothing

            '値がある場合
            If Not String.IsNullOrEmpty(drOut("F009").ToString) Then

                '入力番号取得
                inNo = drOut("F002").ToString

                '疑義の値をカンマで区切って取得
                strgigi = drOut("F009").ToString.Split(",")

                Dim sbSQL As New System.Text.StringBuilder
                Dim res As String = String.Empty

                '区切った各疑義の値で名称を取得
                For r As Integer = 0 To strgigi.Length - 1
                    sbSQL.Remove(0, sbSQL.ToString.Length)

                    '20160318162525 st ////////////////////////
                    '疑義取得SQL条件にプロジェクトを追加
                    'sbSQL.AppendFormat("select F003 from gigi where F002='{0}'", strgigi(r))

                    '//20170428101307 furukawa st ////////////////////////
                    '//プロジェクトIDを関数から取得
                    sbSQL.AppendFormat("select F003 from gigi where F001='{0}' and F002='{1}'", _
                                       GetProjID, strgigi(r))

                    'sbSQL.AppendFormat("select F003 from gigi where F001='{0}' and F002='{1}'", _
                    '                   Me.cmbProj.SelectedValue, strgigi(r))
                    '//20170428101307 furukawa ed ////////////////////////


                    '20160318162525 ed ////////////////////////

                    '疑義の値で名称を取得
                    res &= clsDB.SQL_ExecuteScalar(sbSQL.ToString)
                    'カンマつなぎ
                    res &= ","
                Next

                res = res.Substring(0, res.Length - 1)

                sbSQL.Remove(0, sbSQL.ToString.Length)

                '//20170428101357 furukawa st ////////////////////////
                '//プロジェクトIDを関数から取得

                sbSQL.AppendFormat("update output set F009='{0}' where F001='{1}' and F002='{2}'", _
                                   res, GetProjID, inNo)

                'sbSQL.AppendFormat("update output set F009='{0}' where F001='{1}' and F002='{2}'", _
                '                   res, Me.cmbProj.SelectedValue, inNo)
                '//20170428101357 furukawa ed ////////////////////////

                arrGigiSQL.Add(sbSQL.ToString)

            End If
        Next
        '疑義の更新
        If Not clsDB.SQL_Execute(arrGigiSQL, trans) Then Return False
        Return True


    End Function
    '20160308145715 ed ////////////////////////

#End Region

#Region "最終入力チェック"

    Private Function ChkInp() As Boolean
        ' Dim clsdb As New clsDB
        Dim strsql As String = String.Empty
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable

        '//20170428101454 furukawa st ////////////////////////
        '//プロジェクトIDを関数から取得

        strsql = "select * from inp1 where F001='" & GetProjID() & "' order by F001,F002"
        dt1 = clsdb.SQL_Data(strsql)
        strsql = "select * from inp2 where F001='" & GetProjID() & "' order by F001,F002"
        dt2 = clsdb.SQL_Data(strsql)


        'strsql = "select * from inp1 where F001='" & Me.cmbProj.SelectedValue & "' order by F001,F002"
        'dt1 = clsDB.SQL_Data(strsql)
        'strsql = "select * from inp2 where F001='" & Me.cmbProj.SelectedValue & "' order by F001,F002"
        'dt2 = clsDB.SQL_Data(strsql)
        '//20170428101454 furukawa ed ////////////////////////


        '全項目を文字列比較し、差異があったら警告
        For r As Integer = 0 To dt1.Rows.Count - 1
            For c As Integer = 0 To dt1.Columns.Count - 4
                If String.Compare(dt1.Rows(r)(c).ToString, dt2.Rows(r)(c).ToString, False) Then
                    Return False
                End If
            Next
        Next

        Return True
    End Function
#End Region


#Region "プロジェクトID取得 20170428100324 furukawa"
    '//プロジェクト名と年月からプロジェクトIDを取得（コンボのSelectedValueをやめる）
    Private Function GetProjID() As String

        Dim strsql As String = String.Empty

        strsql = "select F001,F002 from proj where F002='" & Me.cmbProj.Text & _
                "' and F003='" & Me.cmbMonth.Text & "'" & _
                " group by F002 order by F002,F003 "

        Dim strproj As String = clsDB.SQL_ExecuteScalar(strsql)

        Return strproj


    End Function
#End Region


End Class