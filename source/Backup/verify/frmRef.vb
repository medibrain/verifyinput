﻿Public Class frmRef

    Dim _strProjID As String
    Dim _strMonth As String
    Dim _pfrm As frmComp

    Private Sub frmRef_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim clsdb As New clsDB
        Dim dt As New DataTable
        Dim strsql As String = String.Empty

        Dim dcs1 As New DataGridViewCellStyle
        dcs1.BackColor = Color.Lavender
        Dim dcs2 As New DataGridViewCellStyle
        dcs2.BackColor = Color.MintCream




        Me.dgv.Font = New Font("ＭＳ　ゴシック", 12, FontStyle.Regular, GraphicsUnit.Pixel)

        strsql = "select a.F001 as 案件番号1,a.F002 as 連番1,a.F003 as 記号番号1,a.F004 as 被保険者氏名1,a.F005 as 受療者1," & _
                          "b.F001 as 案件番号2,b.F002 as 連番2,b.F003 as 記号番号2,b.F004 as 被保険者氏名2,b.F005 as 受療者2 " & _
                  "from inp1 a left join inp2 b on a.F001=b.F001 and a.F002=b.F002 where a.F001='" & _strProjID & "'" & _
                  "group by a.F001,a.F002,a.F003,a.F004,a.F005,b.F001,b.F002,b.F003,b.F004,b.F005"

        dt = clsdb.SQL_Data(strsql)
        Me.dgv.DataSource = dt

        For c As Integer = 0 To 4
            Me.dgv.Columns(c).DefaultCellStyle = dcs1
        Next
        For c As Integer = 5 To 9
            Me.dgv.Columns(c).DefaultCellStyle = dcs2
        Next


    End Sub

    Public Sub New(ByVal strProjID As String, ByVal strMonth As String, ByRef pfrm As Form)

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。
        _strProjID = strProjID
        _strMonth = strMonth
        _pfrm = pfrm

    End Sub



    Private Sub dgv_CellMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgv.CellMouseDoubleClick
        If DirectCast(dgv.DataSource, DataTable).Rows.Count = 0 Then Exit Sub

        _pfrm.page = dgv.CurrentRow.Cells(1).Value.ToString

    End Sub
End Class