﻿'Imports Microsoft.Office.Interop.Excel
'Excelバージョン差異吸収のため、遅延バインディング化

Imports System.Runtime.InteropServices.Marshal
Imports System.Data
Imports System.Windows.Forms

Public Class clsExcel : Implements IDisposable

    Dim objExcel As Object
    Dim objWorkBooks As Object
    Dim objWorkBook As Object
    Dim objWorkSheets As Object
    Dim objWorkSheet As Object
    Dim strSavePath As String = String.Empty

    ''' <summary>
    ''' エクセルのセルの水平配置位置の値
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum HAlign As Integer
        Left = -4131
        Right = -4152
        Center = -4108
    End Enum

    ''' <summary>
    ''' 出力ヘッダの位置
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum HeaderPosition As Integer
        Left
        Right
        Center
    End Enum

#Region "Excelオブジェクト作成"

    ''' <summary>
    ''' Excelオブジェクト作成
    ''' </summary>
    ''' <returns></returns>

    Private Function CreateExcel() As Boolean

        Try
            GetSavePath()
            If strSavePath = String.Empty Then Return False

            objExcel = CreateObject("Excel.Application")

            Return True
        Catch ex As Exception
            MsgBox("Excelオブジェクトの作成に失敗しました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            Return False
        Finally

        End Try

    End Function
#End Region

#Region "保存先設定"

    ''' <summary>
    ''' 保存先設定
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetSavePath(Optional ByVal strFileName As String = "Book1.xls")
        Dim fd As New SaveFileDialog

        Try
            fd.Title = "保存先選択"
            fd.Filter = "*.xls(Excelファイル)|*.xls"
            fd.FilterIndex = 0
            fd.DefaultExt = "*.xls"
            fd.FileName = strFileName
            If fd.ShowDialog = DialogResult.OK Then
                strSavePath = fd.FileName
            End If

        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "既存のワークブックを開く"

    ''' <summary>
    ''' 既存のワークブック開く
    ''' </summary>
    ''' <param name="strOpenPath">既存のファイルパス</param>
    ''' <param name="flgVisible">可視化フラグ</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <returns>True:成功,False:失敗、キャンセル</returns>
    ''' <remarks></remarks>
    Public Overloads Function OpenWorkBook(ByVal strOpenPath As String, _
                                           ByVal flgVisible As Boolean, _
                                           Optional ByVal strSheetName As String = "Sheet1") As Boolean


        Try
            If Not CreateExcel() Then Return False

            objExcel.Visible = flgVisible

            objWorkBooks = objExcel.Workbooks
            objWorkBook = objWorkBooks.Open(strOpenPath)
            objWorkSheet = objWorkBook.Worksheets(strSheetName)

            Return True

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Return False
        End Try

    End Function
#End Region

#Region "新しいワークブックを作成"

    ''' <summary>
    ''' 新しいワークブックを作成
    ''' </summary>
    ''' <param name="flgVisible">可視化フラグ</param>
    ''' <returns>True:成功,False:失敗、キャンセル</returns>
    ''' <remarks></remarks>
    Public Overloads Function OpenWorkBook(ByVal flgVisible As Boolean) As Boolean

        Try
            If Not CreateExcel() Then Return False

            objExcel.Visible = flgVisible

            objWorkBooks = objExcel.Workbooks
            objWorkBook = objWorkBooks.Add
            objWorkSheet = objWorkBook.Worksheets(1)
            Return True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)

            Return False
        End Try

    End Function
#End Region

#Region "出力文字列"
    ''' <summary>
    ''' 出力文字列
    ''' </summary>
    ''' <param name="strOut">出力文字列</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <param name="StartColumn">出力する列番号</param>
    ''' <param name="StartRow">出力する行番号</param>
    ''' <param name="strFontName">フォント名</param>
    ''' <param name="intFontSize">フォントサイズ</param>
    ''' <param name="intHAlign">水平位置</param>
    ''' <param name="flgAutoFit">オートフィットフラグ</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OutputExtensionString(ByVal strOut As String, _
                                ByVal strSheetName As String, _
                                ByVal StartColumn As Integer, _
                                ByVal StartRow As Integer, _
                                Optional ByVal strFontName As String = "ＭＳ ゴシック", _
                                Optional ByVal intFontSize As Integer = 20, _
                                Optional ByVal intHAlign As HAlign = HAlign.Left, _
                                Optional ByVal flgAutoFit As Boolean = True) As Boolean

        Try
            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択
            With objWorkSheet.cells(StartRow, StartColumn)

                .NumberFormatLocal = "G/標準"
                .FormulaR1C1 = String.Empty
                .font.name = strFontName
                .font.size = intFontSize
                .HorizontalAlignment = intHAlign
                .wraptext = False '折り返して表示を解除
                .ShrinkToFit = False '縮小して全体を表示を解除
                .value = strOut

                'オートフィット
                If flgAutoFit Then objWorkSheet.rows(StartRow).entirerow.autofit()


            End With
            Return True

        Catch ex As Exception
            MsgBox("文字列出力" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation)
            CloseExcelNoSave()
            Return False
        Finally

        End Try
    End Function
#End Region


#Region "フィールド名出力"


    ''' <summary>
    ''' フィールド名出力
    ''' </summary>
    ''' <param name="arrOut">出力する項目の配列</param>
    ''' <param name="strSheetName">出力するシート名</param>
    ''' <param name="StartRow">最初の行</param>
    ''' <param name="StartColumn">最初の列</param>
    ''' <param name="strFontName">フォント名</param>
    ''' <param name="intFontSize">フォントサイズ</param>
    ''' <param name="intHAlign">文字位置</param>
    ''' <param name="flgAutoFit">オートフィット</param>
    ''' <param name="flgInsert">挿入フラグ</param>
    ''' <returns>True：成功、False：失敗</returns>
    ''' <remarks></remarks>
    Public Function OutputFieldStrings(ByVal arrOut As ArrayList, _
                               Optional ByVal strSheetName As String = "Sheet1", _
                               Optional ByVal StartRow As Integer = 1, _
                               Optional ByVal StartColumn As Integer = 1, _
                               Optional ByVal strFontName As String = "ＭＳ ゴシック", _
                               Optional ByVal intFontSize As Integer = 11, _
                               Optional ByVal intHAlign As HAlign = HAlign.Left, _
                               Optional ByVal flgAutoFit As Boolean = True, _
                               Optional ByVal flgInsert As Boolean = True) As Boolean

        Try
            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択

            If flgInsert Then objWorkSheet.rows(StartRow).insert()

            With objWorkSheet.rows(StartRow)
                .NumberFormatLocal = "G/標準"
                .FormulaR1C1 = String.Empty
                .font.name = strFontName
                .font.size = intFontSize
                .HorizontalAlignment = intHAlign
                .wraptext = False '折り返して表示を解除
                .ShrinkToFit = False '縮小して全体を表示を解除

                For c As Integer = StartColumn To arrOut.Count
                    objWorkSheet.cells(StartRow, c).value = arrOut(c - 1)
                Next

                'オートフィット
                If flgAutoFit Then objWorkSheet.rows(StartRow).entirerow.autofit()


            End With
            Return True

        Catch ex As Exception
            MsgBox("文字列出力" & vbCrLf & ex.Message, MsgBoxStyle.Exclamation)
            CloseExcelNoSave()
            Return False
        Finally

        End Try
    End Function
#End Region

#Region "シートヘッダ出力"
    ''' <summary>
    ''' シートヘッダ出力
    ''' </summary>
    ''' <param name="strOut">出力文字列</param>
    ''' <param name="strSheetName">出力シート名</param>
    ''' <param name="HeaderPosition">位置</param>
    ''' <param name="strFontName">フォント名</param>
    ''' <param name="intFontSize">フォントサイズ</param>
    ''' <param name="intHAlign"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function OutHeader(ByVal strOut As String, _
                                ByVal strSheetName As String, _
                                ByVal HeaderPosition As HeaderPosition, _
                                Optional ByVal strFontName As String = "ＭＳ ゴシック", _
                                Optional ByVal intFontSize As Integer = 20, _
                                Optional ByVal intHAlign As HAlign = HAlign.Left) As Boolean

        Try
            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択
            With objWorkSheet.pagesetup
                Select Case HeaderPosition
                    Case clsExcel.HeaderPosition.Left
                        .LeftHeader = strOut

                    Case clsExcel.HeaderPosition.Right
                        .RightHeader = strOut

                    Case clsExcel.HeaderPosition.Center
                        .CenterHeader = strOut

                End Select

            End With
            Return True
        Catch ex As Exception

            Return False

        End Try
    End Function
#End Region

#Region "一覧出力"

    ''' <summary>
    ''' エクセルへ一覧出力
    ''' </summary>
    ''' <param name="dt">datatable</param>
    ''' <param name="lstInt">数値型の列番号(0始まり)</param>
    ''' <param name="strSheetName">シート名</param>
    ''' <param name="StartColumn">開始列番号</param>
    ''' <param name="StartRow">開始行番号</param>
    ''' <returns>成功=true、失敗=false</returns>
    ''' <remarks></remarks>
    Public Overloads Function ExportData(ByVal dt As Data.DataTable, _
                               Optional ByVal lstInt As List(Of Integer) = Nothing, _
                               Optional ByVal strSheetName As String = "Sheet1", _
                               Optional ByVal StartColumn As Integer = 1, _
                               Optional ByVal StartRow As Integer = 1) As Boolean

        Try

            'DataTableから2次元配列にコピー
            'Excelに貼り付ける時、この方が高速
            Dim r As Integer, c As Integer
            Dim tmpArr(dt.Rows.Count, dt.Columns.Count) As Object

            For r = 0 To dt.Rows.Count - 1
                For c = 0 To dt.Columns.Count - 1
                    '数値型の列がある場合はダブルコーテーションつけない
                    If Not IsNothing(lstInt) AndAlso lstInt.Contains(c) Then
                        If Not IsDBNull(dt.Rows(r)(c)) Then tmpArr(r, c) = dt.Rows(r)(c)
                    Else
                        If Not IsDBNull(dt.Rows(r)(c)) Then tmpArr(r, c) = "" & dt.Rows(r)(c) & ""
                    End If
                    'If Not IsDBNull(dt.Rows(r)(c)) Then tmpArr(r, c) = "" & dt.Rows(r)(c) & ""
                Next
            Next

            objWorkSheet = objWorkBook.worksheets(strSheetName) 'シートの選択

            objWorkSheet.Cells.NumberFormat = "@" '値を文字列に
            objWorkSheet.pagesetup.printarea = "" '印刷範囲リセット

            Dim rng1 As Object = Nothing '開始セル
            Dim rng2 As Object = Nothing '終了セル
            rng1 = objWorkSheet.Cells(StartRow, StartColumn)

            'dtのレコードが０件の場合、エクセルに貼り付ける範囲を調整
            If dt.Rows.Count = 0 Then
                rng2 = objWorkSheet.Cells(StartRow, StartColumn)
            Else
                rng2 = objWorkSheet.Cells(StartRow + (r - 1), StartColumn + (c - 1))
            End If


            Dim rng As Object = Nothing
            rng = objWorkSheet.Range(rng1, rng2)
            rng.Value = tmpArr

            '罫線
            rng.Borders.LineStyle = 1
            rng.Borders.Weight = 1


            '改ページ
            'For r = StartRow To dt.Rows.Count - 1
            '    If Split(objWorkSheet.cells(r, 1).value, "-")(1) Mod 40 = 0 Then objWorkSheet.rows(r).pagebreak = -4135
            'Next


            ReleaseComObject(rng)
            ReleaseComObject(rng2)
            ReleaseComObject(rng1)

            Return True
        Catch ex As Exception
            MsgBox("ExportData" & vbCrLf & "出力に失敗ました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            CloseExcelNoSave()

            Return False
        Finally

            GC.Collect()

        End Try
    End Function
#End Region

#Region "ワークブック保存"

    ''' <summary>
    ''' ワークブックを保存する
    ''' </summary>
    ''' <param name="flgClose">保存後閉じる＝True</param>
    ''' <returns>成功＝True,失敗＝False</returns>
    ''' <remarks></remarks>
    Public Function SaveWorkBook(Optional ByVal flgClose As Boolean = True) As Boolean
        Try
            objExcel.displayalerts = False
            objWorkBook.SaveAs(Filename:=strSavePath, FileFormat:=56) 'XlFileFormat.xlExcel8)
            objExcel.displayalerts = True
            If flgClose Then
                objExcel.visible = False
            Else
                objExcel.visible = True
            End If

            Return True
        Catch ex As Exception
            MsgBox("保存に失敗ました。" & vbCrLf & _
                   ex.Message, MsgBoxStyle.Exclamation)
            Return False
        Finally
            If flgClose Then CloseExcel()

        End Try

    End Function
#End Region

#Region "エクセル解放"

    ''' <summary>
    ''' Excelの破棄
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CloseExcel()

        If Not IsNothing(objWorkSheet) Then ReleaseComObject(objWorkSheet)
        If Not IsNothing(objWorkSheets) Then ReleaseComObject(objWorkSheets)
        'If Not IsNothing(objWorkBook) Then
        '    objWorkBook.close()
        '    ReleaseComObject(objWorkBook)
        'End If

        If Not IsNothing(objWorkBooks) Then ReleaseComObject(objWorkBooks)
        If Not IsNothing(objExcel) Then
            objExcel.Quit()
            ReleaseComObject(objExcel)
        End If

        GC.Collect()


    End Sub
#End Region

#Region "エクセルを保存せず破棄"

    Public Sub CloseExcelNoSave()
        objExcel.displayalerts = False
        objWorkBook.close()
        objExcel.displayalerts = True

        CloseExcel()

    End Sub
#End Region

    Private disposedValue As Boolean = False        ' 重複する呼び出しを検出するには

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: 他の状態を解放します (マネージ オブジェクト)。
            End If

            ' TODO: ユーザー独自の状態を解放します (アンマネージ オブジェクト)。
            GC.Collect()

            ' TODO: 大きなフィールドを null に設定します。
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' このコードは、破棄可能なパターンを正しく実装できるように Visual Basic によって追加されました。
    Public Sub Dispose() Implements IDisposable.Dispose
        ' このコードを変更しないでください。クリーンアップ コードを上の Dispose(ByVal disposing As Boolean) に記述します。
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
