﻿Imports System.Windows.Forms

Public Class dlgFields

    'postgres用クラス作ったので、clsDB.csはプロジェクトに含めてない
    'dim clsdb As new clsDB
    Dim clsdb As New clsDB_pos

#Region "フォームロード"

    Private Sub dlgFields_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dt As New DataTable("data")
        Dim strsql As String = String.Empty

        strsql = "select flgUse as 使用,FieldName as 項目名 from UseFields order by OrderNo"

        dt = clsdb.SQL_Data(strsql)
        Me.dgv.DataSource = dt
        Me.dgv.Columns("項目名").ReadOnly = True


    End Sub
#End Region

#Region "全選択ボタン"

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllSel.Click
        For r As Integer = 0 To Me.dgv.Rows.Count - 1
            DirectCast(Me.dgv.Rows(r).Cells(0), DataGridViewCheckBoxCell).Value = 1
        Next
    End Sub
#End Region

#Region "全解除ボタン"

    Private Sub btnAllDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAllDel.Click
        For r As Integer = 0 To Me.dgv.Rows.Count - 1
            DirectCast(Me.dgv.Rows(r).Cells(0), DataGridViewCheckBoxCell).Value = 0
        Next
    End Sub
#End Region

#Region "更新ボタン"

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim arrSQL As New ArrayList
        Dim strsql As String = String.Empty
        Dim trans As Npgsql.NpgsqlTransaction = Nothing

        'グリッドループ
        For r As Integer = 0 To Me.dgv.Rows.Count - 1

            strsql = String.Empty
            strsql = "update UseFields set "
            strsql &= " FlgUse="
            If dgv.Rows(r).Cells(0).Value = True Then

                '//20201015182732 furukawa st ////////////////////////
                '//SQLiteからPostgresになってTrue/Falseに
                strsql &= "true"
                'strsql &= "1"
                '//20201015182732 furukawa ed ////////////////////////
            Else

                '//20201015182825 furukawa st ////////////////////////
                strsql &= "false"
                'strsql &= "0"
                '//SQLiteからPostgresになってTrue/Falseに
                '//20201015182825 furukawa ed ////////////////////////

            End If
            strsql &= " where FieldName='" & dgv.Rows(r).Cells(1).Value & "'"

            '配列に追加
            arrSQL.Add(strsql)

        Next

        If clsdb.SQL_Execute(arrSQL, trans) Then
            MessageBox.Show("更新しました", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        Me.Close()

    End Sub
#End Region

End Class
