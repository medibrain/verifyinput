﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmComp
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComp))
        Me.SplitCnt = New System.Windows.Forms.SplitContainer()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.btnEnt1 = New System.Windows.Forms.Button()
        Me.chkGigi105 = New System.Windows.Forms.CheckBox()
        Me.chkGigi104 = New System.Windows.Forms.CheckBox()
        Me.chkGigi109 = New System.Windows.Forms.CheckBox()
        Me.btnDel1 = New System.Windows.Forms.Button()
        Me.chkGigi103 = New System.Windows.Forms.CheckBox()
        Me.chkGigi108 = New System.Windows.Forms.CheckBox()
        Me.chkGigi107 = New System.Windows.Forms.CheckBox()
        Me.chkGigi106 = New System.Windows.Forms.CheckBox()
        Me.chkGigi102 = New System.Windows.Forms.CheckBox()
        Me.chkGigi101 = New System.Windows.Forms.CheckBox()
        Me.chkGigi100 = New System.Windows.Forms.CheckBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnEnt2 = New System.Windows.Forms.Button()
        Me.chkGigi200 = New System.Windows.Forms.CheckBox()
        Me.chkGigi205 = New System.Windows.Forms.CheckBox()
        Me.chkGigi209 = New System.Windows.Forms.CheckBox()
        Me.btnDel2 = New System.Windows.Forms.Button()
        Me.chkGigi204 = New System.Windows.Forms.CheckBox()
        Me.chkGigi208 = New System.Windows.Forms.CheckBox()
        Me.chkGigi207 = New System.Windows.Forms.CheckBox()
        Me.chkGigi206 = New System.Windows.Forms.CheckBox()
        Me.chkGigi203 = New System.Windows.Forms.CheckBox()
        Me.chkGigi202 = New System.Windows.Forms.CheckBox()
        Me.chkGigi201 = New System.Windows.Forms.CheckBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblProj = New System.Windows.Forms.Label()
        Me.lblMonth = New System.Windows.Forms.Label()
        Me.chkInp = New System.Windows.Forms.CheckBox()
        Me.btnSpin1 = New System.Windows.Forms.Button()
        Me.btnSpin2 = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.btnRef = New System.Windows.Forms.Button()
        Me.tt = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnDel = New System.Windows.Forms.Button()
        Me.chkDel = New System.Windows.Forms.CheckBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtFind = New verify.CstTxt()
        Me.txtHanyo1 = New verify.CstTxt()
        Me.txtSeikyu13 = New verify.CstTxt()
        Me.txtFutan13 = New verify.CstTxt()
        Me.txtGokei13 = New verify.CstTxt()
        Me.txtSeikyu12 = New verify.CstTxt()
        Me.txtFutan12 = New verify.CstTxt()
        Me.txtGokei12 = New verify.CstTxt()
        Me.txtSeikyu11 = New verify.CstTxt()
        Me.txtFutan11 = New verify.CstTxt()
        Me.txtGokei11 = New verify.CstTxt()
        Me.txtMax1 = New verify.CstTxt()
        Me.txtInNo1 = New verify.CstTxt()
        Me.txtKigo1 = New verify.CstTxt()
        Me.txttime1 = New verify.CstTxt()
        Me.txtName1 = New verify.CstTxt()
        Me.txtPC1 = New verify.CstTxt()
        Me.txtNissu13 = New verify.CstTxt()
        Me.txtNissu12 = New verify.CstTxt()
        Me.txtNissu11 = New verify.CstTxt()
        Me.txtMonth13 = New verify.CstTxt()
        Me.txtMonth12 = New verify.CstTxt()
        Me.txtMonth11 = New verify.CstTxt()
        Me.txtSejutu1 = New verify.CstTxt()
        Me.txtadd1 = New verify.CstTxt()
        Me.txtzip1 = New verify.CstTxt()
        Me.txtJuryoNm1 = New verify.CstTxt()
        Me.txtHihoNm1 = New verify.CstTxt()
        Me.txtMax2 = New verify.CstTxt()
        Me.txtSeikyu23 = New verify.CstTxt()
        Me.txtFutan23 = New verify.CstTxt()
        Me.txtGokei23 = New verify.CstTxt()
        Me.txtSeikyu22 = New verify.CstTxt()
        Me.txtFutan22 = New verify.CstTxt()
        Me.txtGokei22 = New verify.CstTxt()
        Me.txtSeikyu21 = New verify.CstTxt()
        Me.txtFutan21 = New verify.CstTxt()
        Me.txtGokei21 = New verify.CstTxt()
        Me.txtNissu23 = New verify.CstTxt()
        Me.txtNissu22 = New verify.CstTxt()
        Me.txtNissu21 = New verify.CstTxt()
        Me.txtMonth23 = New verify.CstTxt()
        Me.txtMonth22 = New verify.CstTxt()
        Me.txtMonth21 = New verify.CstTxt()
        Me.txtInNo2 = New verify.CstTxt()
        Me.txtKigo2 = New verify.CstTxt()
        Me.txttime2 = New verify.CstTxt()
        Me.txtname2 = New verify.CstTxt()
        Me.txtpc2 = New verify.CstTxt()
        Me.txtSejutu2 = New verify.CstTxt()
        Me.txtadd2 = New verify.CstTxt()
        Me.txtzip2 = New verify.CstTxt()
        Me.txtJuryoNm2 = New verify.CstTxt()
        Me.txtHihoNm2 = New verify.CstTxt()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.txtHanyo2 = New verify.CstTxt()
        CType(Me.SplitCnt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitCnt.Panel1.SuspendLayout()
        Me.SplitCnt.Panel2.SuspendLayout()
        Me.SplitCnt.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitCnt
        '
        Me.SplitCnt.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitCnt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ep.SetIconAlignment(Me.SplitCnt, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.SplitCnt.Location = New System.Drawing.Point(12, 127)
        Me.SplitCnt.Name = "SplitCnt"
        '
        'SplitCnt.Panel1
        '
        Me.SplitCnt.Panel1.BackColor = System.Drawing.Color.Lavender
        Me.SplitCnt.Panel1.Controls.Add(Me.Label51)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtHanyo1)
        Me.SplitCnt.Panel1.Controls.Add(Me.btnEnt1)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi105)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi104)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi109)
        Me.SplitCnt.Panel1.Controls.Add(Me.btnDel1)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi103)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi108)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi107)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi106)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi102)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi101)
        Me.SplitCnt.Panel1.Controls.Add(Me.chkGigi100)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label49)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label47)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label35)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtSeikyu13)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label36)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtFutan13)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label37)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtGokei13)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label32)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtSeikyu12)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label33)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtFutan12)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label34)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtGokei12)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label31)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtSeikyu11)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label30)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtFutan11)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label29)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtGokei11)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtMax1)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtInNo1)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtKigo1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label27)
        Me.SplitCnt.Panel1.Controls.Add(Me.txttime1)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtName1)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtPC1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label14)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtNissu13)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label15)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtNissu12)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label16)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtNissu11)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label13)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtMonth13)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label12)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtMonth12)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label52)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label11)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtMonth11)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label9)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtSejutu1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label8)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label7)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtadd1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label6)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtzip1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label5)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtJuryoNm1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label4)
        Me.SplitCnt.Panel1.Controls.Add(Me.txtHihoNm1)
        Me.SplitCnt.Panel1.Controls.Add(Me.Label3)
        Me.SplitCnt.Panel1.ImeMode = System.Windows.Forms.ImeMode.Off
        '
        'SplitCnt.Panel2
        '
        Me.SplitCnt.Panel2.BackColor = System.Drawing.Color.MintCream
        Me.SplitCnt.Panel2.Controls.Add(Me.Label54)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtHanyo2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label53)
        Me.SplitCnt.Panel2.Controls.Add(Me.btnEnt2)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi200)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi205)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi209)
        Me.SplitCnt.Panel2.Controls.Add(Me.btnDel2)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi204)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi208)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi207)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi206)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi203)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi202)
        Me.SplitCnt.Panel2.Controls.Add(Me.chkGigi201)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label50)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label48)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtMax2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label1)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtSeikyu23)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label2)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtFutan23)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label10)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtGokei23)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label17)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtSeikyu22)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label18)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtFutan22)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label19)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtGokei22)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label38)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtSeikyu21)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label39)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtFutan21)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label40)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtGokei21)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label41)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtNissu23)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label42)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtNissu22)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label43)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtNissu21)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label44)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtMonth23)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label45)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtMonth22)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label46)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtMonth21)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtInNo2)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtKigo2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label28)
        Me.SplitCnt.Panel2.Controls.Add(Me.txttime2)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtname2)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtpc2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label20)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtSejutu2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label21)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label22)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtadd2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label23)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtzip2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label24)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtJuryoNm2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label25)
        Me.SplitCnt.Panel2.Controls.Add(Me.txtHihoNm2)
        Me.SplitCnt.Panel2.Controls.Add(Me.Label26)
        Me.SplitCnt.Size = New System.Drawing.Size(1240, 667)
        Me.SplitCnt.SplitterDistance = 616
        Me.SplitCnt.SplitterWidth = 7
        Me.SplitCnt.TabIndex = 0
        Me.SplitCnt.TabStop = False
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label51.Location = New System.Drawing.Point(23, 436)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(50, 28)
        Me.Label51.TabIndex = 101
        Me.Label51.Text = "汎用"
        '
        'btnEnt1
        '
        Me.btnEnt1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEnt1.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnEnt1.ForeColor = System.Drawing.Color.Blue
        Me.btnEnt1.Location = New System.Drawing.Point(3, 613)
        Me.btnEnt1.Name = "btnEnt1"
        Me.btnEnt1.Size = New System.Drawing.Size(108, 49)
        Me.btnEnt1.TabIndex = 41
        Me.btnEnt1.Text = "登録(PageUp)"
        Me.btnEnt1.UseVisualStyleBackColor = True
        '
        'chkGigi105
        '
        Me.chkGigi105.AutoSize = True
        Me.chkGigi105.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi105.Location = New System.Drawing.Point(358, 468)
        Me.chkGigi105.Name = "chkGigi105"
        Me.chkGigi105.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi105.TabIndex = 36
        Me.chkGigi105.Text = "chk01"
        Me.chkGigi105.UseVisualStyleBackColor = True
        Me.chkGigi105.Visible = False
        '
        'chkGigi104
        '
        Me.chkGigi104.AutoSize = True
        Me.chkGigi104.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi104.Location = New System.Drawing.Point(130, 582)
        Me.chkGigi104.Name = "chkGigi104"
        Me.chkGigi104.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi104.TabIndex = 35
        Me.chkGigi104.Text = "chk01"
        Me.chkGigi104.UseVisualStyleBackColor = True
        Me.chkGigi104.Visible = False
        '
        'chkGigi109
        '
        Me.chkGigi109.AutoSize = True
        Me.chkGigi109.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi109.Location = New System.Drawing.Point(358, 582)
        Me.chkGigi109.Name = "chkGigi109"
        Me.chkGigi109.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi109.TabIndex = 40
        Me.chkGigi109.Text = "chk01"
        Me.chkGigi109.UseVisualStyleBackColor = True
        Me.chkGigi109.Visible = False
        '
        'btnDel1
        '
        Me.btnDel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDel1.Font = New System.Drawing.Font("MS UI Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnDel1.ForeColor = System.Drawing.Color.Red
        Me.btnDel1.Location = New System.Drawing.Point(3, 567)
        Me.btnDel1.Name = "btnDel1"
        Me.btnDel1.Size = New System.Drawing.Size(64, 35)
        Me.btnDel1.TabIndex = 99
        Me.btnDel1.TabStop = False
        Me.btnDel1.Text = "削除"
        Me.btnDel1.UseVisualStyleBackColor = True
        Me.btnDel1.Visible = False
        '
        'chkGigi103
        '
        Me.chkGigi103.AutoSize = True
        Me.chkGigi103.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi103.Location = New System.Drawing.Point(130, 553)
        Me.chkGigi103.Name = "chkGigi103"
        Me.chkGigi103.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi103.TabIndex = 34
        Me.chkGigi103.Text = "chk01"
        Me.chkGigi103.UseVisualStyleBackColor = True
        Me.chkGigi103.Visible = False
        '
        'chkGigi108
        '
        Me.chkGigi108.AutoSize = True
        Me.chkGigi108.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi108.Location = New System.Drawing.Point(358, 553)
        Me.chkGigi108.Name = "chkGigi108"
        Me.chkGigi108.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi108.TabIndex = 39
        Me.chkGigi108.Text = "chk01"
        Me.chkGigi108.UseVisualStyleBackColor = True
        Me.chkGigi108.Visible = False
        '
        'chkGigi107
        '
        Me.chkGigi107.AutoSize = True
        Me.chkGigi107.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi107.Location = New System.Drawing.Point(358, 524)
        Me.chkGigi107.Name = "chkGigi107"
        Me.chkGigi107.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi107.TabIndex = 38
        Me.chkGigi107.Text = "chk01"
        Me.chkGigi107.UseVisualStyleBackColor = True
        Me.chkGigi107.Visible = False
        '
        'chkGigi106
        '
        Me.chkGigi106.AutoSize = True
        Me.chkGigi106.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi106.Location = New System.Drawing.Point(358, 496)
        Me.chkGigi106.Name = "chkGigi106"
        Me.chkGigi106.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi106.TabIndex = 37
        Me.chkGigi106.Text = "chk01"
        Me.chkGigi106.UseVisualStyleBackColor = True
        Me.chkGigi106.Visible = False
        '
        'chkGigi102
        '
        Me.chkGigi102.AutoSize = True
        Me.chkGigi102.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi102.Location = New System.Drawing.Point(130, 525)
        Me.chkGigi102.Name = "chkGigi102"
        Me.chkGigi102.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi102.TabIndex = 33
        Me.chkGigi102.Text = "chk01"
        Me.chkGigi102.UseVisualStyleBackColor = True
        Me.chkGigi102.Visible = False
        '
        'chkGigi101
        '
        Me.chkGigi101.AutoSize = True
        Me.chkGigi101.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi101.Location = New System.Drawing.Point(130, 496)
        Me.chkGigi101.Name = "chkGigi101"
        Me.chkGigi101.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi101.TabIndex = 32
        Me.chkGigi101.Text = "chk01"
        Me.chkGigi101.UseVisualStyleBackColor = True
        Me.chkGigi101.Visible = False
        '
        'chkGigi100
        '
        Me.chkGigi100.AutoSize = True
        Me.chkGigi100.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi100.Location = New System.Drawing.Point(130, 468)
        Me.chkGigi100.Name = "chkGigi100"
        Me.chkGigi100.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi100.TabIndex = 31
        Me.chkGigi100.Text = "chk01"
        Me.chkGigi100.UseVisualStyleBackColor = True
        Me.chkGigi100.Visible = False
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(400, 111)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(160, 13)
        Me.Label49.TabIndex = 51
        Me.Label49.Text = "Ctrlキーで被保険者氏名をコピー"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("MS UI Gothic", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label47.Location = New System.Drawing.Point(231, 9)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(19, 19)
        Me.Label47.TabIndex = 50
        Me.Label47.Text = "/"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label35.Location = New System.Drawing.Point(418, 407)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(84, 24)
        Me.Label35.TabIndex = 49
        Me.Label35.Text = "請求金額3"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label36.Location = New System.Drawing.Point(418, 376)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(100, 24)
        Me.Label36.TabIndex = 47
        Me.Label36.Text = "一部負担金3"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label37.Location = New System.Drawing.Point(418, 344)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(52, 24)
        Me.Label37.TabIndex = 45
        Me.Label37.Text = "合計3"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label32.Location = New System.Drawing.Point(223, 407)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(84, 24)
        Me.Label32.TabIndex = 43
        Me.Label32.Text = "請求金額2"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label33.Location = New System.Drawing.Point(223, 376)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(100, 24)
        Me.Label33.TabIndex = 41
        Me.Label33.Text = "一部負担金2"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label34.Location = New System.Drawing.Point(223, 344)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(52, 24)
        Me.Label34.TabIndex = 39
        Me.Label34.Text = "合計2"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label31.Location = New System.Drawing.Point(24, 407)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(84, 24)
        Me.Label31.TabIndex = 37
        Me.Label31.Text = "請求金額1"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label30.Location = New System.Drawing.Point(24, 376)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(100, 24)
        Me.Label30.TabIndex = 35
        Me.Label30.Text = "一部負担金1"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label29.Location = New System.Drawing.Point(24, 344)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(52, 24)
        Me.Label29.TabIndex = 33
        Me.Label29.Text = "合計1"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label27.Location = New System.Drawing.Point(23, 5)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(88, 28)
        Me.Label27.TabIndex = 31
        Me.Label27.Text = "入力番号"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.Location = New System.Drawing.Point(418, 313)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 24)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "実日数3"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.Location = New System.Drawing.Point(223, 313)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 24)
        Me.Label15.TabIndex = 25
        Me.Label15.Text = "実日数2"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label16.Location = New System.Drawing.Point(24, 313)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 24)
        Me.Label16.TabIndex = 23
        Me.Label16.Text = "実日数1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label13.Location = New System.Drawing.Point(418, 269)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(84, 24)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "診療年月3"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label12.Location = New System.Drawing.Point(223, 269)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 24)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "診療年月2"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label11.Location = New System.Drawing.Point(24, 269)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 24)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "診療年月1"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.Location = New System.Drawing.Point(23, 237)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 28)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "施術所名"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.Location = New System.Drawing.Point(23, 468)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 28)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "疑義内容"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.Location = New System.Drawing.Point(23, 161)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(50, 28)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "住所"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.Location = New System.Drawing.Point(23, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 28)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "郵便番号"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.Location = New System.Drawing.Point(23, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 28)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "受療者氏名"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.Location = New System.Drawing.Point(23, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(126, 28)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "被保険者氏名"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.Location = New System.Drawing.Point(23, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 28)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "記号番号"
        '
        'btnEnt2
        '
        Me.btnEnt2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEnt2.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnEnt2.ForeColor = System.Drawing.Color.Blue
        Me.btnEnt2.Location = New System.Drawing.Point(5, 603)
        Me.btnEnt2.Name = "btnEnt2"
        Me.btnEnt2.Size = New System.Drawing.Size(108, 49)
        Me.btnEnt2.TabIndex = 51
        Me.btnEnt2.Text = "登録(PageUp)"
        Me.btnEnt2.UseVisualStyleBackColor = True
        '
        'chkGigi200
        '
        Me.chkGigi200.AutoSize = True
        Me.chkGigi200.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi200.Location = New System.Drawing.Point(127, 471)
        Me.chkGigi200.Name = "chkGigi200"
        Me.chkGigi200.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi200.TabIndex = 31
        Me.chkGigi200.Text = "chk01"
        Me.chkGigi200.UseVisualStyleBackColor = True
        Me.chkGigi200.Visible = False
        '
        'chkGigi205
        '
        Me.chkGigi205.AutoSize = True
        Me.chkGigi205.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi205.Location = New System.Drawing.Point(355, 471)
        Me.chkGigi205.Name = "chkGigi205"
        Me.chkGigi205.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi205.TabIndex = 36
        Me.chkGigi205.Text = "chk01"
        Me.chkGigi205.UseVisualStyleBackColor = True
        Me.chkGigi205.Visible = False
        '
        'chkGigi209
        '
        Me.chkGigi209.AutoSize = True
        Me.chkGigi209.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi209.Location = New System.Drawing.Point(355, 586)
        Me.chkGigi209.Name = "chkGigi209"
        Me.chkGigi209.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi209.TabIndex = 40
        Me.chkGigi209.Text = "chk01"
        Me.chkGigi209.UseVisualStyleBackColor = True
        Me.chkGigi209.Visible = False
        '
        'btnDel2
        '
        Me.btnDel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDel2.Font = New System.Drawing.Font("MS UI Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnDel2.ForeColor = System.Drawing.Color.Red
        Me.btnDel2.Location = New System.Drawing.Point(5, 556)
        Me.btnDel2.Name = "btnDel2"
        Me.btnDel2.Size = New System.Drawing.Size(64, 35)
        Me.btnDel2.TabIndex = 99
        Me.btnDel2.TabStop = False
        Me.btnDel2.Text = "削除"
        Me.btnDel2.UseVisualStyleBackColor = True
        Me.btnDel2.Visible = False
        '
        'chkGigi204
        '
        Me.chkGigi204.AutoSize = True
        Me.chkGigi204.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi204.Location = New System.Drawing.Point(127, 586)
        Me.chkGigi204.Name = "chkGigi204"
        Me.chkGigi204.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi204.TabIndex = 35
        Me.chkGigi204.Text = "chk01"
        Me.chkGigi204.UseVisualStyleBackColor = True
        Me.chkGigi204.Visible = False
        '
        'chkGigi208
        '
        Me.chkGigi208.AutoSize = True
        Me.chkGigi208.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi208.Location = New System.Drawing.Point(355, 558)
        Me.chkGigi208.Name = "chkGigi208"
        Me.chkGigi208.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi208.TabIndex = 39
        Me.chkGigi208.Text = "chk01"
        Me.chkGigi208.UseVisualStyleBackColor = True
        Me.chkGigi208.Visible = False
        '
        'chkGigi207
        '
        Me.chkGigi207.AutoSize = True
        Me.chkGigi207.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi207.Location = New System.Drawing.Point(355, 529)
        Me.chkGigi207.Name = "chkGigi207"
        Me.chkGigi207.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi207.TabIndex = 38
        Me.chkGigi207.Text = "chk01"
        Me.chkGigi207.UseVisualStyleBackColor = True
        Me.chkGigi207.Visible = False
        '
        'chkGigi206
        '
        Me.chkGigi206.AutoSize = True
        Me.chkGigi206.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi206.Location = New System.Drawing.Point(355, 500)
        Me.chkGigi206.Name = "chkGigi206"
        Me.chkGigi206.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi206.TabIndex = 37
        Me.chkGigi206.Text = "chk01"
        Me.chkGigi206.UseVisualStyleBackColor = True
        Me.chkGigi206.Visible = False
        '
        'chkGigi203
        '
        Me.chkGigi203.AutoSize = True
        Me.chkGigi203.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi203.Location = New System.Drawing.Point(127, 558)
        Me.chkGigi203.Name = "chkGigi203"
        Me.chkGigi203.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi203.TabIndex = 34
        Me.chkGigi203.Text = "chk01"
        Me.chkGigi203.UseVisualStyleBackColor = True
        Me.chkGigi203.Visible = False
        '
        'chkGigi202
        '
        Me.chkGigi202.AutoSize = True
        Me.chkGigi202.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi202.Location = New System.Drawing.Point(127, 529)
        Me.chkGigi202.Name = "chkGigi202"
        Me.chkGigi202.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi202.TabIndex = 33
        Me.chkGigi202.Text = "chk01"
        Me.chkGigi202.UseVisualStyleBackColor = True
        Me.chkGigi202.Visible = False
        '
        'chkGigi201
        '
        Me.chkGigi201.AutoSize = True
        Me.chkGigi201.Font = New System.Drawing.Font("メイリオ", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkGigi201.Location = New System.Drawing.Point(127, 500)
        Me.chkGigi201.Name = "chkGigi201"
        Me.chkGigi201.Size = New System.Drawing.Size(66, 24)
        Me.chkGigi201.TabIndex = 32
        Me.chkGigi201.Text = "chk01"
        Me.chkGigi201.UseVisualStyleBackColor = True
        Me.chkGigi201.Visible = False
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(398, 112)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(160, 13)
        Me.Label50.TabIndex = 51
        Me.Label50.Text = "Ctrlキーで被保険者氏名をコピー"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("MS UI Gothic", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label48.Location = New System.Drawing.Point(227, 8)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(19, 19)
        Me.Label48.TabIndex = 90
        Me.Label48.Text = "/"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(414, 408)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 24)
        Me.Label1.TabIndex = 88
        Me.Label1.Text = "請求金額3"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(414, 374)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 24)
        Me.Label2.TabIndex = 87
        Me.Label2.Text = "一部負担金3"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label10.Location = New System.Drawing.Point(414, 344)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(52, 24)
        Me.Label10.TabIndex = 86
        Me.Label10.Text = "合計3"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label17.Location = New System.Drawing.Point(219, 408)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(84, 24)
        Me.Label17.TabIndex = 85
        Me.Label17.Text = "請求金額2"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label18.Location = New System.Drawing.Point(219, 374)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(100, 24)
        Me.Label18.TabIndex = 84
        Me.Label18.Text = "一部負担金2"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label19.Location = New System.Drawing.Point(219, 344)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(52, 24)
        Me.Label19.TabIndex = 83
        Me.Label19.Text = "合計2"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label38.Location = New System.Drawing.Point(20, 409)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(84, 24)
        Me.Label38.TabIndex = 82
        Me.Label38.Text = "請求金額1"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label39.Location = New System.Drawing.Point(20, 376)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(100, 24)
        Me.Label39.TabIndex = 81
        Me.Label39.Text = "一部負担金1"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label40.Location = New System.Drawing.Point(20, 345)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(52, 24)
        Me.Label40.TabIndex = 80
        Me.Label40.Text = "合計1"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label41.Location = New System.Drawing.Point(414, 311)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(68, 24)
        Me.Label41.TabIndex = 79
        Me.Label41.Text = "実日数3"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label42.Location = New System.Drawing.Point(219, 311)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(68, 24)
        Me.Label42.TabIndex = 78
        Me.Label42.Text = "実日数2"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label43.Location = New System.Drawing.Point(20, 316)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(68, 24)
        Me.Label43.TabIndex = 77
        Me.Label43.Text = "実日数1"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label44.Location = New System.Drawing.Point(414, 269)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(84, 24)
        Me.Label44.TabIndex = 76
        Me.Label44.Text = "診療年月3"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label45.Location = New System.Drawing.Point(219, 269)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(84, 24)
        Me.Label45.TabIndex = 72
        Me.Label45.Text = "診療年月2"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label46.Location = New System.Drawing.Point(20, 270)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(84, 24)
        Me.Label46.TabIndex = 69
        Me.Label46.Text = "診療年月1"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label28.Location = New System.Drawing.Point(19, 5)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(88, 28)
        Me.Label28.TabIndex = 58
        Me.Label28.Text = "入力番号"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label20.Location = New System.Drawing.Point(19, 235)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(88, 28)
        Me.Label20.TabIndex = 41
        Me.Label20.Text = "施術所名"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label21.Location = New System.Drawing.Point(20, 468)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(88, 28)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "疑義内容"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label22.Location = New System.Drawing.Point(19, 161)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(50, 28)
        Me.Label22.TabIndex = 38
        Me.Label22.Text = "住所"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label23.Location = New System.Drawing.Point(19, 133)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(88, 28)
        Me.Label23.TabIndex = 36
        Me.Label23.Text = "郵便番号"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label24.Location = New System.Drawing.Point(19, 104)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(107, 28)
        Me.Label24.TabIndex = 34
        Me.Label24.Text = "受療者氏名"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label25.Location = New System.Drawing.Point(19, 73)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(126, 28)
        Me.Label25.TabIndex = 32
        Me.Label25.Text = "被保険者氏名"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label26.Location = New System.Drawing.Point(19, 40)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(88, 28)
        Me.Label26.TabIndex = 30
        Me.Label26.Text = "記号番号"
        '
        'lblProj
        '
        Me.lblProj.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProj.BackColor = System.Drawing.Color.SeaGreen
        Me.lblProj.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProj.Font = New System.Drawing.Font("メイリオ", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblProj.ForeColor = System.Drawing.Color.Yellow
        Me.lblProj.Location = New System.Drawing.Point(12, 10)
        Me.lblProj.Name = "lblProj"
        Me.lblProj.Size = New System.Drawing.Size(1240, 36)
        Me.lblProj.TabIndex = 1
        Me.lblProj.Text = "Label1"
        Me.lblProj.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMonth
        '
        Me.lblMonth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonth.BackColor = System.Drawing.Color.SeaGreen
        Me.lblMonth.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMonth.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblMonth.ForeColor = System.Drawing.Color.Gold
        Me.lblMonth.Location = New System.Drawing.Point(12, 46)
        Me.lblMonth.Name = "lblMonth"
        Me.lblMonth.Size = New System.Drawing.Size(1240, 36)
        Me.lblMonth.TabIndex = 2
        Me.lblMonth.Text = "Label2"
        Me.lblMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkInp
        '
        Me.chkInp.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkInp.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkInp.ForeColor = System.Drawing.Color.Blue
        Me.chkInp.Location = New System.Drawing.Point(12, 83)
        Me.chkInp.Name = "chkInp"
        Me.chkInp.Size = New System.Drawing.Size(205, 41)
        Me.chkInp.TabIndex = 3
        Me.chkInp.TabStop = False
        Me.chkInp.Text = "入力回数　1回目"
        Me.chkInp.UseVisualStyleBackColor = True
        '
        'btnSpin1
        '
        Me.btnSpin1.Location = New System.Drawing.Point(572, 93)
        Me.btnSpin1.Name = "btnSpin1"
        Me.btnSpin1.Size = New System.Drawing.Size(30, 24)
        Me.btnSpin1.TabIndex = 9
        Me.btnSpin1.TabStop = False
        Me.btnSpin1.Text = "<"
        Me.btnSpin1.UseVisualStyleBackColor = True
        '
        'btnSpin2
        '
        Me.btnSpin2.Location = New System.Drawing.Point(660, 93)
        Me.btnSpin2.Name = "btnSpin2"
        Me.btnSpin2.Size = New System.Drawing.Size(30, 24)
        Me.btnSpin2.TabIndex = 9
        Me.btnSpin2.TabStop = False
        Me.btnSpin2.Text = ">"
        Me.btnSpin2.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnNew.Location = New System.Drawing.Point(227, 87)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(85, 37)
        Me.btnNew.TabIndex = 10
        Me.btnNew.TabStop = False
        Me.btnNew.Text = "新規入力"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'ep
        '
        Me.ep.ContainerControl = Me.SplitCnt
        '
        'lblInfo
        '
        Me.lblInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblInfo.Location = New System.Drawing.Point(12, 804)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(536, 18)
        Me.lblInfo.TabIndex = 11
        Me.lblInfo.Text = " "
        '
        'btnRef
        '
        Me.btnRef.Location = New System.Drawing.Point(465, 87)
        Me.btnRef.Name = "btnRef"
        Me.btnRef.Size = New System.Drawing.Size(101, 36)
        Me.btnRef.TabIndex = 12
        Me.btnRef.TabStop = False
        Me.btnRef.Text = "入力済データ"
        Me.btnRef.UseVisualStyleBackColor = True
        '
        'tt
        '
        Me.tt.AutoPopDelay = 5000
        Me.tt.InitialDelay = 5
        Me.tt.ReshowDelay = 100
        Me.tt.ShowAlways = True
        '
        'btnDel
        '
        Me.btnDel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDel.Enabled = False
        Me.btnDel.Font = New System.Drawing.Font("MS UI Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnDel.ForeColor = System.Drawing.Color.Red
        Me.btnDel.Location = New System.Drawing.Point(641, 800)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(64, 27)
        Me.btnDel.TabIndex = 99
        Me.btnDel.TabStop = False
        Me.btnDel.Text = "削除"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'chkDel
        '
        Me.chkDel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkDel.AutoSize = True
        Me.chkDel.Location = New System.Drawing.Point(620, 809)
        Me.chkDel.Name = "chkDel"
        Me.chkDel.Size = New System.Drawing.Size(15, 14)
        Me.chkDel.TabIndex = 100
        Me.chkDel.TabStop = False
        Me.chkDel.UseVisualStyleBackColor = True
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("メイリオ", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label52.Location = New System.Drawing.Point(25, 294)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(175, 17)
        Me.Label52.TabIndex = 17
        Me.Label52.Text = "年号番号（50105型）で入力可能"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("メイリオ", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label53.Location = New System.Drawing.Point(22, 295)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(175, 17)
        Me.Label53.TabIndex = 100
        Me.Label53.Text = "年号番号（50105型）で入力可能"
        '
        'txtFind
        '
        Me.txtFind.AllowHyphen = False
        Me.txtFind.AutoTab = False
        Me.txtFind.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFind.Location = New System.Drawing.Point(608, 93)
        Me.txtFind.Name = "txtFind"
        Me.txtFind.NumberOnly = True
        Me.txtFind.Size = New System.Drawing.Size(46, 23)
        Me.txtFind.TabIndex = 8
        Me.txtFind.TabStop = False
        Me.txtFind.Text = "0"
        Me.txtFind.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtHanyo1
        '
        Me.txtHanyo1.AllowHyphen = False
        Me.txtHanyo1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtHanyo1.AutoTab = False
        Me.txtHanyo1.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtHanyo1.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtHanyo1.Location = New System.Drawing.Point(130, 438)
        Me.txtHanyo1.MaxLength = 255
        Me.txtHanyo1.Multiline = True
        Me.txtHanyo1.Name = "txtHanyo1"
        Me.txtHanyo1.NumberOnly = False
        Me.txtHanyo1.Size = New System.Drawing.Size(472, 29)
        Me.txtHanyo1.TabIndex = 22
        Me.txtHanyo1.Text = "あいうえお"
        Me.txtHanyo1.WordWrap = False
        '
        'txtSeikyu13
        '
        Me.txtSeikyu13.AllowHyphen = False
        Me.txtSeikyu13.AutoTab = False
        Me.txtSeikyu13.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSeikyu13.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtSeikyu13.Location = New System.Drawing.Point(523, 408)
        Me.txtSeikyu13.MaxLength = 7
        Me.txtSeikyu13.Name = "txtSeikyu13"
        Me.txtSeikyu13.NumberOnly = True
        Me.txtSeikyu13.Size = New System.Drawing.Size(79, 23)
        Me.txtSeikyu13.TabIndex = 21
        Me.txtSeikyu13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFutan13
        '
        Me.txtFutan13.AllowHyphen = False
        Me.txtFutan13.AutoTab = False
        Me.txtFutan13.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFutan13.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtFutan13.Location = New System.Drawing.Point(523, 376)
        Me.txtFutan13.MaxLength = 7
        Me.txtFutan13.Name = "txtFutan13"
        Me.txtFutan13.NumberOnly = True
        Me.txtFutan13.Size = New System.Drawing.Size(79, 23)
        Me.txtFutan13.TabIndex = 20
        Me.txtFutan13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGokei13
        '
        Me.txtGokei13.AllowHyphen = False
        Me.txtGokei13.AutoTab = False
        Me.txtGokei13.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtGokei13.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtGokei13.Location = New System.Drawing.Point(523, 345)
        Me.txtGokei13.MaxLength = 7
        Me.txtGokei13.Name = "txtGokei13"
        Me.txtGokei13.NumberOnly = True
        Me.txtGokei13.Size = New System.Drawing.Size(79, 23)
        Me.txtGokei13.TabIndex = 19
        Me.txtGokei13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSeikyu12
        '
        Me.txtSeikyu12.AllowHyphen = False
        Me.txtSeikyu12.AutoTab = False
        Me.txtSeikyu12.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSeikyu12.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtSeikyu12.Location = New System.Drawing.Point(327, 408)
        Me.txtSeikyu12.MaxLength = 7
        Me.txtSeikyu12.Name = "txtSeikyu12"
        Me.txtSeikyu12.NumberOnly = True
        Me.txtSeikyu12.Size = New System.Drawing.Size(79, 23)
        Me.txtSeikyu12.TabIndex = 16
        Me.txtSeikyu12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFutan12
        '
        Me.txtFutan12.AllowHyphen = False
        Me.txtFutan12.AutoTab = False
        Me.txtFutan12.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFutan12.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtFutan12.Location = New System.Drawing.Point(327, 376)
        Me.txtFutan12.MaxLength = 7
        Me.txtFutan12.Name = "txtFutan12"
        Me.txtFutan12.NumberOnly = True
        Me.txtFutan12.Size = New System.Drawing.Size(79, 23)
        Me.txtFutan12.TabIndex = 15
        Me.txtFutan12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGokei12
        '
        Me.txtGokei12.AllowHyphen = False
        Me.txtGokei12.AutoTab = False
        Me.txtGokei12.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtGokei12.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtGokei12.Location = New System.Drawing.Point(327, 345)
        Me.txtGokei12.MaxLength = 7
        Me.txtGokei12.Name = "txtGokei12"
        Me.txtGokei12.NumberOnly = True
        Me.txtGokei12.Size = New System.Drawing.Size(79, 23)
        Me.txtGokei12.TabIndex = 14
        Me.txtGokei12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSeikyu11
        '
        Me.txtSeikyu11.AllowHyphen = False
        Me.txtSeikyu11.AutoTab = False
        Me.txtSeikyu11.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSeikyu11.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtSeikyu11.Location = New System.Drawing.Point(130, 408)
        Me.txtSeikyu11.MaxLength = 7
        Me.txtSeikyu11.Name = "txtSeikyu11"
        Me.txtSeikyu11.NumberOnly = True
        Me.txtSeikyu11.Size = New System.Drawing.Size(79, 23)
        Me.txtSeikyu11.TabIndex = 10
        Me.txtSeikyu11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFutan11
        '
        Me.txtFutan11.AllowHyphen = False
        Me.txtFutan11.AutoTab = False
        Me.txtFutan11.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFutan11.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtFutan11.Location = New System.Drawing.Point(130, 376)
        Me.txtFutan11.MaxLength = 7
        Me.txtFutan11.Name = "txtFutan11"
        Me.txtFutan11.NumberOnly = True
        Me.txtFutan11.Size = New System.Drawing.Size(79, 23)
        Me.txtFutan11.TabIndex = 9
        Me.txtFutan11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGokei11
        '
        Me.txtGokei11.AllowHyphen = False
        Me.txtGokei11.AutoTab = False
        Me.txtGokei11.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtGokei11.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtGokei11.Location = New System.Drawing.Point(130, 345)
        Me.txtGokei11.MaxLength = 7
        Me.txtGokei11.Name = "txtGokei11"
        Me.txtGokei11.NumberOnly = True
        Me.txtGokei11.Size = New System.Drawing.Size(79, 23)
        Me.txtGokei11.TabIndex = 8
        Me.txtGokei11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMax1
        '
        Me.txtMax1.AllowHyphen = False
        Me.txtMax1.AutoTab = False
        Me.txtMax1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMax1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMax1.Location = New System.Drawing.Point(252, 5)
        Me.txtMax1.MaxLength = 4
        Me.txtMax1.Name = "txtMax1"
        Me.txtMax1.NumberOnly = True
        Me.txtMax1.ReadOnly = True
        Me.txtMax1.Size = New System.Drawing.Size(66, 26)
        Me.txtMax1.TabIndex = 30
        Me.txtMax1.TabStop = False
        Me.txtMax1.Text = "1234"
        Me.txtMax1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInNo1
        '
        Me.txtInNo1.AllowHyphen = False
        Me.txtInNo1.AutoTab = False
        Me.txtInNo1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtInNo1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtInNo1.Location = New System.Drawing.Point(163, 5)
        Me.txtInNo1.MaxLength = 4
        Me.txtInNo1.Name = "txtInNo1"
        Me.txtInNo1.NumberOnly = True
        Me.txtInNo1.ReadOnly = True
        Me.txtInNo1.Size = New System.Drawing.Size(66, 26)
        Me.txtInNo1.TabIndex = 30
        Me.txtInNo1.TabStop = False
        Me.txtInNo1.Text = "1234"
        Me.txtInNo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKigo1
        '
        Me.txtKigo1.AllowHyphen = True
        Me.txtKigo1.AutoTab = False
        Me.txtKigo1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtKigo1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtKigo1.Location = New System.Drawing.Point(163, 40)
        Me.txtKigo1.MaxLength = 16
        Me.txtKigo1.Name = "txtKigo1"
        Me.txtKigo1.NumberOnly = True
        Me.txtKigo1.Size = New System.Drawing.Size(188, 26)
        Me.txtKigo1.TabIndex = 0
        '
        'txttime1
        '
        Me.txttime1.AllowHyphen = False
        Me.txttime1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttime1.AutoTab = False
        Me.txttime1.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txttime1.Location = New System.Drawing.Point(405, 26)
        Me.txttime1.Name = "txttime1"
        Me.txttime1.NumberOnly = False
        Me.txttime1.ReadOnly = True
        Me.txttime1.Size = New System.Drawing.Size(206, 20)
        Me.txttime1.TabIndex = 29
        Me.txttime1.TabStop = False
        Me.txttime1.Text = "yyyy/mm/dd hh:mm:ss"
        Me.txttime1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtName1
        '
        Me.txtName1.AllowHyphen = False
        Me.txtName1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtName1.AutoTab = False
        Me.txtName1.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtName1.Location = New System.Drawing.Point(405, 3)
        Me.txtName1.Name = "txtName1"
        Me.txtName1.NumberOnly = False
        Me.txtName1.ReadOnly = True
        Me.txtName1.Size = New System.Drawing.Size(100, 20)
        Me.txtName1.TabIndex = 29
        Me.txtName1.TabStop = False
        Me.txtName1.Text = "あいうえお"
        Me.txtName1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPC1
        '
        Me.txtPC1.AllowHyphen = False
        Me.txtPC1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPC1.AutoTab = False
        Me.txtPC1.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPC1.Location = New System.Drawing.Point(511, 3)
        Me.txtPC1.Name = "txtPC1"
        Me.txtPC1.NumberOnly = False
        Me.txtPC1.ReadOnly = True
        Me.txtPC1.Size = New System.Drawing.Size(100, 20)
        Me.txtPC1.TabIndex = 29
        Me.txtPC1.TabStop = False
        Me.txtPC1.Text = "PC-xxx"
        Me.txtPC1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNissu13
        '
        Me.txtNissu13.AllowHyphen = False
        Me.txtNissu13.AutoTab = False
        Me.txtNissu13.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtNissu13.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNissu13.Location = New System.Drawing.Point(523, 314)
        Me.txtNissu13.MaxLength = 4
        Me.txtNissu13.Name = "txtNissu13"
        Me.txtNissu13.NumberOnly = True
        Me.txtNissu13.Size = New System.Drawing.Size(79, 23)
        Me.txtNissu13.TabIndex = 18
        Me.txtNissu13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNissu12
        '
        Me.txtNissu12.AllowHyphen = False
        Me.txtNissu12.AutoTab = False
        Me.txtNissu12.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtNissu12.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNissu12.Location = New System.Drawing.Point(327, 314)
        Me.txtNissu12.MaxLength = 4
        Me.txtNissu12.Name = "txtNissu12"
        Me.txtNissu12.NumberOnly = True
        Me.txtNissu12.Size = New System.Drawing.Size(79, 23)
        Me.txtNissu12.TabIndex = 13
        Me.txtNissu12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNissu11
        '
        Me.txtNissu11.AllowHyphen = False
        Me.txtNissu11.AutoTab = False
        Me.txtNissu11.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtNissu11.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNissu11.Location = New System.Drawing.Point(130, 314)
        Me.txtNissu11.MaxLength = 4
        Me.txtNissu11.Name = "txtNissu11"
        Me.txtNissu11.NumberOnly = True
        Me.txtNissu11.Size = New System.Drawing.Size(79, 23)
        Me.txtNissu11.TabIndex = 7
        Me.txtNissu11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMonth13
        '
        Me.txtMonth13.AllowHyphen = False
        Me.txtMonth13.AutoTab = False
        Me.txtMonth13.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMonth13.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMonth13.Location = New System.Drawing.Point(523, 270)
        Me.txtMonth13.MaxLength = 5
        Me.txtMonth13.Name = "txtMonth13"
        Me.txtMonth13.NumberOnly = True
        Me.txtMonth13.Size = New System.Drawing.Size(79, 23)
        Me.txtMonth13.TabIndex = 17
        '
        'txtMonth12
        '
        Me.txtMonth12.AllowHyphen = False
        Me.txtMonth12.AutoTab = False
        Me.txtMonth12.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMonth12.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMonth12.Location = New System.Drawing.Point(327, 270)
        Me.txtMonth12.MaxLength = 5
        Me.txtMonth12.Name = "txtMonth12"
        Me.txtMonth12.NumberOnly = True
        Me.txtMonth12.Size = New System.Drawing.Size(79, 23)
        Me.txtMonth12.TabIndex = 12
        '
        'txtMonth11
        '
        Me.txtMonth11.AllowHyphen = False
        Me.txtMonth11.AutoTab = False
        Me.txtMonth11.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMonth11.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMonth11.Location = New System.Drawing.Point(130, 270)
        Me.txtMonth11.MaxLength = 5
        Me.txtMonth11.Name = "txtMonth11"
        Me.txtMonth11.NumberOnly = True
        Me.txtMonth11.Size = New System.Drawing.Size(79, 23)
        Me.txtMonth11.TabIndex = 6
        '
        'txtSejutu1
        '
        Me.txtSejutu1.AllowHyphen = False
        Me.txtSejutu1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSejutu1.AutoTab = False
        Me.txtSejutu1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSejutu1.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtSejutu1.Location = New System.Drawing.Point(163, 237)
        Me.txtSejutu1.MaxLength = 128
        Me.txtSejutu1.Name = "txtSejutu1"
        Me.txtSejutu1.NumberOnly = False
        Me.txtSejutu1.Size = New System.Drawing.Size(430, 26)
        Me.txtSejutu1.TabIndex = 5
        '
        'txtadd1
        '
        Me.txtadd1.AllowHyphen = False
        Me.txtadd1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtadd1.AutoTab = False
        Me.txtadd1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtadd1.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtadd1.Location = New System.Drawing.Point(163, 164)
        Me.txtadd1.MaxLength = 255
        Me.txtadd1.Multiline = True
        Me.txtadd1.Name = "txtadd1"
        Me.txtadd1.NumberOnly = False
        Me.txtadd1.Size = New System.Drawing.Size(430, 69)
        Me.txtadd1.TabIndex = 4
        '
        'txtzip1
        '
        Me.txtzip1.AllowHyphen = False
        Me.txtzip1.AutoTab = False
        Me.txtzip1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtzip1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtzip1.Location = New System.Drawing.Point(163, 134)
        Me.txtzip1.MaxLength = 7
        Me.txtzip1.Name = "txtzip1"
        Me.txtzip1.NumberOnly = True
        Me.txtzip1.Size = New System.Drawing.Size(94, 26)
        Me.txtzip1.TabIndex = 3
        '
        'txtJuryoNm1
        '
        Me.txtJuryoNm1.AllowHyphen = False
        Me.txtJuryoNm1.AutoTab = False
        Me.txtJuryoNm1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtJuryoNm1.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtJuryoNm1.Location = New System.Drawing.Point(163, 103)
        Me.txtJuryoNm1.MaxLength = 32
        Me.txtJuryoNm1.Name = "txtJuryoNm1"
        Me.txtJuryoNm1.NumberOnly = False
        Me.txtJuryoNm1.Size = New System.Drawing.Size(231, 26)
        Me.txtJuryoNm1.TabIndex = 2
        Me.tt.SetToolTip(Me.txtJuryoNm1, "Ctrlキーで被保険者名をコピー")
        '
        'txtHihoNm1
        '
        Me.txtHihoNm1.AllowHyphen = False
        Me.txtHihoNm1.AutoTab = False
        Me.txtHihoNm1.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtHihoNm1.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtHihoNm1.Location = New System.Drawing.Point(163, 72)
        Me.txtHihoNm1.MaxLength = 32
        Me.txtHihoNm1.Name = "txtHihoNm1"
        Me.txtHihoNm1.NumberOnly = False
        Me.txtHihoNm1.Size = New System.Drawing.Size(231, 26)
        Me.txtHihoNm1.TabIndex = 1
        '
        'txtMax2
        '
        Me.txtMax2.AllowHyphen = False
        Me.txtMax2.AutoTab = False
        Me.txtMax2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMax2.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMax2.Location = New System.Drawing.Point(248, 5)
        Me.txtMax2.MaxLength = 4
        Me.txtMax2.Name = "txtMax2"
        Me.txtMax2.NumberOnly = True
        Me.txtMax2.ReadOnly = True
        Me.txtMax2.Size = New System.Drawing.Size(66, 26)
        Me.txtMax2.TabIndex = 89
        Me.txtMax2.TabStop = False
        Me.txtMax2.Text = "1234"
        Me.txtMax2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSeikyu23
        '
        Me.txtSeikyu23.AllowHyphen = False
        Me.txtSeikyu23.AutoTab = False
        Me.txtSeikyu23.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSeikyu23.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtSeikyu23.Location = New System.Drawing.Point(519, 409)
        Me.txtSeikyu23.MaxLength = 7
        Me.txtSeikyu23.Name = "txtSeikyu23"
        Me.txtSeikyu23.NumberOnly = True
        Me.txtSeikyu23.Size = New System.Drawing.Size(79, 23)
        Me.txtSeikyu23.TabIndex = 20
        Me.txtSeikyu23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFutan23
        '
        Me.txtFutan23.AllowHyphen = False
        Me.txtFutan23.AutoTab = False
        Me.txtFutan23.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFutan23.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtFutan23.Location = New System.Drawing.Point(519, 376)
        Me.txtFutan23.MaxLength = 7
        Me.txtFutan23.Name = "txtFutan23"
        Me.txtFutan23.NumberOnly = True
        Me.txtFutan23.Size = New System.Drawing.Size(79, 23)
        Me.txtFutan23.TabIndex = 19
        Me.txtFutan23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGokei23
        '
        Me.txtGokei23.AllowHyphen = False
        Me.txtGokei23.AutoTab = False
        Me.txtGokei23.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtGokei23.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtGokei23.Location = New System.Drawing.Point(519, 345)
        Me.txtGokei23.MaxLength = 7
        Me.txtGokei23.Name = "txtGokei23"
        Me.txtGokei23.NumberOnly = True
        Me.txtGokei23.Size = New System.Drawing.Size(79, 23)
        Me.txtGokei23.TabIndex = 18
        Me.txtGokei23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSeikyu22
        '
        Me.txtSeikyu22.AllowHyphen = False
        Me.txtSeikyu22.AutoTab = False
        Me.txtSeikyu22.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSeikyu22.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtSeikyu22.Location = New System.Drawing.Point(323, 409)
        Me.txtSeikyu22.MaxLength = 7
        Me.txtSeikyu22.Name = "txtSeikyu22"
        Me.txtSeikyu22.NumberOnly = True
        Me.txtSeikyu22.Size = New System.Drawing.Size(79, 23)
        Me.txtSeikyu22.TabIndex = 15
        Me.txtSeikyu22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFutan22
        '
        Me.txtFutan22.AllowHyphen = False
        Me.txtFutan22.AutoTab = False
        Me.txtFutan22.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFutan22.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtFutan22.Location = New System.Drawing.Point(323, 376)
        Me.txtFutan22.MaxLength = 7
        Me.txtFutan22.Name = "txtFutan22"
        Me.txtFutan22.NumberOnly = True
        Me.txtFutan22.Size = New System.Drawing.Size(79, 23)
        Me.txtFutan22.TabIndex = 14
        Me.txtFutan22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGokei22
        '
        Me.txtGokei22.AllowHyphen = False
        Me.txtGokei22.AutoTab = False
        Me.txtGokei22.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtGokei22.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtGokei22.Location = New System.Drawing.Point(323, 345)
        Me.txtGokei22.MaxLength = 7
        Me.txtGokei22.Name = "txtGokei22"
        Me.txtGokei22.NumberOnly = True
        Me.txtGokei22.Size = New System.Drawing.Size(79, 23)
        Me.txtGokei22.TabIndex = 13
        Me.txtGokei22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSeikyu21
        '
        Me.txtSeikyu21.AllowHyphen = False
        Me.txtSeikyu21.AutoTab = False
        Me.txtSeikyu21.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSeikyu21.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtSeikyu21.Location = New System.Drawing.Point(126, 410)
        Me.txtSeikyu21.MaxLength = 7
        Me.txtSeikyu21.Name = "txtSeikyu21"
        Me.txtSeikyu21.NumberOnly = True
        Me.txtSeikyu21.Size = New System.Drawing.Size(79, 23)
        Me.txtSeikyu21.TabIndex = 10
        Me.txtSeikyu21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFutan21
        '
        Me.txtFutan21.AllowHyphen = False
        Me.txtFutan21.AutoTab = False
        Me.txtFutan21.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtFutan21.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtFutan21.Location = New System.Drawing.Point(126, 377)
        Me.txtFutan21.MaxLength = 7
        Me.txtFutan21.Name = "txtFutan21"
        Me.txtFutan21.NumberOnly = True
        Me.txtFutan21.Size = New System.Drawing.Size(79, 23)
        Me.txtFutan21.TabIndex = 9
        Me.txtFutan21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGokei21
        '
        Me.txtGokei21.AllowHyphen = False
        Me.txtGokei21.AutoTab = False
        Me.txtGokei21.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtGokei21.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtGokei21.Location = New System.Drawing.Point(126, 346)
        Me.txtGokei21.MaxLength = 7
        Me.txtGokei21.Name = "txtGokei21"
        Me.txtGokei21.NumberOnly = True
        Me.txtGokei21.Size = New System.Drawing.Size(79, 23)
        Me.txtGokei21.TabIndex = 8
        Me.txtGokei21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNissu23
        '
        Me.txtNissu23.AllowHyphen = False
        Me.txtNissu23.AutoTab = False
        Me.txtNissu23.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtNissu23.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNissu23.Location = New System.Drawing.Point(519, 312)
        Me.txtNissu23.MaxLength = 4
        Me.txtNissu23.Name = "txtNissu23"
        Me.txtNissu23.NumberOnly = True
        Me.txtNissu23.Size = New System.Drawing.Size(79, 23)
        Me.txtNissu23.TabIndex = 17
        Me.txtNissu23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNissu22
        '
        Me.txtNissu22.AllowHyphen = False
        Me.txtNissu22.AutoTab = False
        Me.txtNissu22.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtNissu22.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNissu22.Location = New System.Drawing.Point(323, 312)
        Me.txtNissu22.MaxLength = 4
        Me.txtNissu22.Name = "txtNissu22"
        Me.txtNissu22.NumberOnly = True
        Me.txtNissu22.Size = New System.Drawing.Size(79, 23)
        Me.txtNissu22.TabIndex = 12
        Me.txtNissu22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNissu21
        '
        Me.txtNissu21.AllowHyphen = False
        Me.txtNissu21.AutoTab = False
        Me.txtNissu21.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtNissu21.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNissu21.Location = New System.Drawing.Point(126, 317)
        Me.txtNissu21.MaxLength = 4
        Me.txtNissu21.Name = "txtNissu21"
        Me.txtNissu21.NumberOnly = True
        Me.txtNissu21.Size = New System.Drawing.Size(79, 23)
        Me.txtNissu21.TabIndex = 7
        Me.txtNissu21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMonth23
        '
        Me.txtMonth23.AllowHyphen = False
        Me.txtMonth23.AutoTab = False
        Me.txtMonth23.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMonth23.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMonth23.Location = New System.Drawing.Point(519, 270)
        Me.txtMonth23.MaxLength = 5
        Me.txtMonth23.Name = "txtMonth23"
        Me.txtMonth23.NumberOnly = True
        Me.txtMonth23.Size = New System.Drawing.Size(79, 23)
        Me.txtMonth23.TabIndex = 16
        '
        'txtMonth22
        '
        Me.txtMonth22.AllowHyphen = False
        Me.txtMonth22.AutoTab = False
        Me.txtMonth22.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMonth22.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMonth22.Location = New System.Drawing.Point(323, 270)
        Me.txtMonth22.MaxLength = 5
        Me.txtMonth22.Name = "txtMonth22"
        Me.txtMonth22.NumberOnly = True
        Me.txtMonth22.Size = New System.Drawing.Size(79, 23)
        Me.txtMonth22.TabIndex = 11
        '
        'txtMonth21
        '
        Me.txtMonth21.AllowHyphen = False
        Me.txtMonth21.AutoTab = False
        Me.txtMonth21.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtMonth21.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtMonth21.Location = New System.Drawing.Point(126, 271)
        Me.txtMonth21.MaxLength = 5
        Me.txtMonth21.Name = "txtMonth21"
        Me.txtMonth21.NumberOnly = True
        Me.txtMonth21.Size = New System.Drawing.Size(79, 23)
        Me.txtMonth21.TabIndex = 6
        '
        'txtInNo2
        '
        Me.txtInNo2.AllowHyphen = False
        Me.txtInNo2.AutoTab = False
        Me.txtInNo2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtInNo2.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtInNo2.Location = New System.Drawing.Point(159, 5)
        Me.txtInNo2.MaxLength = 4
        Me.txtInNo2.Name = "txtInNo2"
        Me.txtInNo2.NumberOnly = True
        Me.txtInNo2.ReadOnly = True
        Me.txtInNo2.Size = New System.Drawing.Size(66, 26)
        Me.txtInNo2.TabIndex = 57
        Me.txtInNo2.TabStop = False
        Me.txtInNo2.Text = "1234"
        Me.txtInNo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtKigo2
        '
        Me.txtKigo2.AllowHyphen = True
        Me.txtKigo2.AutoTab = False
        Me.txtKigo2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtKigo2.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtKigo2.Location = New System.Drawing.Point(159, 40)
        Me.txtKigo2.MaxLength = 16
        Me.txtKigo2.Name = "txtKigo2"
        Me.txtKigo2.NumberOnly = True
        Me.txtKigo2.Size = New System.Drawing.Size(188, 26)
        Me.txtKigo2.TabIndex = 0
        '
        'txttime2
        '
        Me.txttime2.AllowHyphen = False
        Me.txttime2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttime2.AutoTab = False
        Me.txttime2.BackColor = System.Drawing.SystemColors.Control
        Me.txttime2.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txttime2.Location = New System.Drawing.Point(394, 26)
        Me.txttime2.Name = "txttime2"
        Me.txttime2.NumberOnly = False
        Me.txttime2.ReadOnly = True
        Me.txttime2.Size = New System.Drawing.Size(206, 20)
        Me.txttime2.TabIndex = 56
        Me.txttime2.TabStop = False
        Me.txttime2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtname2
        '
        Me.txtname2.AllowHyphen = False
        Me.txtname2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtname2.AutoTab = False
        Me.txtname2.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtname2.Location = New System.Drawing.Point(394, 3)
        Me.txtname2.Name = "txtname2"
        Me.txtname2.NumberOnly = False
        Me.txtname2.ReadOnly = True
        Me.txtname2.Size = New System.Drawing.Size(100, 20)
        Me.txtname2.TabIndex = 55
        Me.txtname2.TabStop = False
        Me.txtname2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtpc2
        '
        Me.txtpc2.AllowHyphen = False
        Me.txtpc2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtpc2.AutoTab = False
        Me.txtpc2.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtpc2.Location = New System.Drawing.Point(500, 3)
        Me.txtpc2.Name = "txtpc2"
        Me.txtpc2.NumberOnly = False
        Me.txtpc2.ReadOnly = True
        Me.txtpc2.Size = New System.Drawing.Size(100, 20)
        Me.txtpc2.TabIndex = 54
        Me.txtpc2.TabStop = False
        Me.txtpc2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSejutu2
        '
        Me.txtSejutu2.AllowHyphen = False
        Me.txtSejutu2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSejutu2.AutoTab = False
        Me.txtSejutu2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSejutu2.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtSejutu2.Location = New System.Drawing.Point(159, 237)
        Me.txtSejutu2.MaxLength = 128
        Me.txtSejutu2.Name = "txtSejutu2"
        Me.txtSejutu2.NumberOnly = False
        Me.txtSejutu2.Size = New System.Drawing.Size(409, 26)
        Me.txtSejutu2.TabIndex = 5
        '
        'txtadd2
        '
        Me.txtadd2.AllowHyphen = False
        Me.txtadd2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtadd2.AutoTab = False
        Me.txtadd2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtadd2.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtadd2.Location = New System.Drawing.Point(159, 162)
        Me.txtadd2.MaxLength = 255
        Me.txtadd2.Multiline = True
        Me.txtadd2.Name = "txtadd2"
        Me.txtadd2.NumberOnly = False
        Me.txtadd2.Size = New System.Drawing.Size(409, 69)
        Me.txtadd2.TabIndex = 4
        '
        'txtzip2
        '
        Me.txtzip2.AllowHyphen = False
        Me.txtzip2.AutoTab = False
        Me.txtzip2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtzip2.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtzip2.Location = New System.Drawing.Point(159, 134)
        Me.txtzip2.MaxLength = 7
        Me.txtzip2.Name = "txtzip2"
        Me.txtzip2.NumberOnly = True
        Me.txtzip2.Size = New System.Drawing.Size(94, 26)
        Me.txtzip2.TabIndex = 3
        '
        'txtJuryoNm2
        '
        Me.txtJuryoNm2.AllowHyphen = False
        Me.txtJuryoNm2.AutoTab = False
        Me.txtJuryoNm2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtJuryoNm2.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtJuryoNm2.Location = New System.Drawing.Point(159, 104)
        Me.txtJuryoNm2.MaxLength = 32
        Me.txtJuryoNm2.Name = "txtJuryoNm2"
        Me.txtJuryoNm2.NumberOnly = False
        Me.txtJuryoNm2.Size = New System.Drawing.Size(231, 26)
        Me.txtJuryoNm2.TabIndex = 2
        Me.tt.SetToolTip(Me.txtJuryoNm2, "Ctrlキーで被保険者名をコピー")
        '
        'txtHihoNm2
        '
        Me.txtHihoNm2.AllowHyphen = False
        Me.txtHihoNm2.AutoTab = False
        Me.txtHihoNm2.Font = New System.Drawing.Font("ＭＳ ゴシック", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtHihoNm2.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtHihoNm2.Location = New System.Drawing.Point(159, 73)
        Me.txtHihoNm2.MaxLength = 32
        Me.txtHihoNm2.Name = "txtHihoNm2"
        Me.txtHihoNm2.NumberOnly = False
        Me.txtHihoNm2.Size = New System.Drawing.Size(231, 26)
        Me.txtHihoNm2.TabIndex = 1
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("メイリオ", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label54.Location = New System.Drawing.Point(19, 436)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(50, 28)
        Me.Label54.TabIndex = 103
        Me.Label54.Text = "汎用"
        '
        'txtHanyo2
        '
        Me.txtHanyo2.AllowHyphen = False
        Me.txtHanyo2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtHanyo2.AutoTab = False
        Me.txtHanyo2.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtHanyo2.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtHanyo2.Location = New System.Drawing.Point(126, 438)
        Me.txtHanyo2.MaxLength = 255
        Me.txtHanyo2.Multiline = True
        Me.txtHanyo2.Name = "txtHanyo2"
        Me.txtHanyo2.NumberOnly = False
        Me.txtHanyo2.Size = New System.Drawing.Size(472, 29)
        Me.txtHanyo2.TabIndex = 25
        Me.txtHanyo2.Text = "あいうえお"
        Me.txtHanyo2.WordWrap = False
        '
        'frmComp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(1264, 832)
        Me.Controls.Add(Me.chkDel)
        Me.Controls.Add(Me.btnRef)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.btnSpin2)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.chkInp)
        Me.Controls.Add(Me.btnSpin1)
        Me.Controls.Add(Me.lblMonth)
        Me.Controls.Add(Me.lblProj)
        Me.Controls.Add(Me.txtFind)
        Me.Controls.Add(Me.SplitCnt)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmComp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "入力"
        Me.SplitCnt.Panel1.ResumeLayout(False)
        Me.SplitCnt.Panel1.PerformLayout()
        Me.SplitCnt.Panel2.ResumeLayout(False)
        Me.SplitCnt.Panel2.PerformLayout()
        CType(Me.SplitCnt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitCnt.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitCnt As System.Windows.Forms.SplitContainer
    Friend WithEvents lblProj As System.Windows.Forms.Label
    Friend WithEvents lblMonth As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtzip1 As verify.CstTxt
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtJuryoNm1 As verify.CstTxt
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtHihoNm1 As verify.CstTxt
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKigo1 As verify.CstTxt
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtMonth13 As verify.CstTxt
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtMonth12 As verify.CstTxt
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtMonth11 As verify.CstTxt
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSejutu1 As verify.CstTxt
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtadd1 As verify.CstTxt
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtNissu13 As verify.CstTxt
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtNissu12 As verify.CstTxt
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtNissu11 As verify.CstTxt
    Friend WithEvents chkInp As System.Windows.Forms.CheckBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtSejutu2 As verify.CstTxt
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtadd2 As verify.CstTxt
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtzip2 As verify.CstTxt
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtJuryoNm2 As verify.CstTxt
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtHihoNm2 As verify.CstTxt
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtKigo2 As verify.CstTxt
    Friend WithEvents btnEnt1 As System.Windows.Forms.Button
    Friend WithEvents btnEnt2 As System.Windows.Forms.Button
    Friend WithEvents txttime1 As verify.CstTxt
    Friend WithEvents txtName1 As verify.CstTxt
    Friend WithEvents txtPC1 As verify.CstTxt
    Friend WithEvents txttime2 As verify.CstTxt
    Friend WithEvents txtname2 As verify.CstTxt
    Friend WithEvents txtpc2 As verify.CstTxt
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtInNo1 As verify.CstTxt
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtInNo2 As verify.CstTxt
    Friend WithEvents btnSpin1 As System.Windows.Forms.Button
    Friend WithEvents btnSpin2 As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtSeikyu11 As verify.CstTxt
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtFutan11 As verify.CstTxt
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtGokei11 As verify.CstTxt
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtSeikyu13 As verify.CstTxt
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtFutan13 As verify.CstTxt
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtGokei13 As verify.CstTxt
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtSeikyu12 As verify.CstTxt
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtFutan12 As verify.CstTxt
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtGokei12 As verify.CstTxt
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSeikyu23 As verify.CstTxt
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFutan23 As verify.CstTxt
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtGokei23 As verify.CstTxt
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtSeikyu22 As verify.CstTxt
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtFutan22 As verify.CstTxt
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtGokei22 As verify.CstTxt
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtSeikyu21 As verify.CstTxt
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtFutan21 As verify.CstTxt
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtGokei21 As verify.CstTxt
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtNissu23 As verify.CstTxt
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtNissu22 As verify.CstTxt
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents txtNissu21 As verify.CstTxt
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtMonth23 As verify.CstTxt
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtMonth22 As verify.CstTxt
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtMonth21 As verify.CstTxt
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents txtMax1 As verify.CstTxt
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtMax2 As verify.CstTxt
    Friend WithEvents btnRef As System.Windows.Forms.Button
    Friend WithEvents txtFind As verify.CstTxt
    Friend WithEvents tt As System.Windows.Forms.ToolTip
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents chkGigi201 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi208 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi207 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi206 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi203 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi202 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi200 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi205 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi209 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi204 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi105 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi104 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi109 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi103 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi108 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi107 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi106 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi102 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi101 As System.Windows.Forms.CheckBox
    Friend WithEvents chkGigi100 As System.Windows.Forms.CheckBox
    Friend WithEvents btnDel1 As System.Windows.Forms.Button
    Friend WithEvents btnDel2 As System.Windows.Forms.Button
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents chkDel As System.Windows.Forms.CheckBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents txtHanyo1 As verify.CstTxt
    Friend WithEvents Label52 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents Label54 As Label
    Friend WithEvents txtHanyo2 As CstTxt
End Class
