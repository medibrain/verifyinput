﻿Imports System.Windows.Forms

Public Class dlgGigi
    Dim _strProjID As String = String.Empty
    Dim _strMonth As String = String.Empty
    Dim _strProj As String = String.Empty

#Region "フォームロード"

    Private Sub dlgGigi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        LoadData()
        Me.lblProj.Text = _strProj

    End Sub

#End Region
#Region "コンストラクタ"

    Public Sub New(ByVal strProjID As String, ByVal strMonth As String, ByVal strProj As String)

        ' この呼び出しは、Windows フォーム デザイナで必要です。
        InitializeComponent()

        ' InitializeComponent() 呼び出しの後で初期化を追加します。

        _strProjID = strProjID
        _strMonth = strMonth
        _strProj = strProj

    End Sub
#End Region

#Region "追加ボタン"

    Private Sub btnadd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        Dim clsDB_pos As New clsDB_pos
        Try
            Dim strsql As String = String.Empty

            If Me.txtEnt.Text = String.Empty Then Exit Sub

            strsql = String.Empty
            strsql = "select count(*) from  gigi where F001='" & _strProjID & "'"
            Dim cnt As Integer = clsDB_pos.SQL_ExecuteScalar(strsql)
            If cnt > 10 Then
                MessageBox.Show("10以上は設定できません", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If


            strsql = String.Empty
            strsql = "insert into gigi values('" & _strProjID & "','" & GetMaxID() & "','" & Me.txtEnt.Text & "')"
            clsDB_pos.SQL_Execute(strsql)

            LoadData()

            Me.txtEnt.Focus()

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
        End Try

    End Sub
#End Region
#Region "更新ボタン"

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click

        Dim clsDB_pos As New clsDB_pos
        Dim dt As New DataTable
        Try
            Dim strsql As String = String.Empty

            If Me.txtEnt.Text = String.Empty Then Exit Sub
            If IsDBNull(lst.SelectedValue) Then Exit Sub


            strsql = "select F003 from gigi where F001='" & _strProjID &
                    "' and F002='" & Me.lst.SelectedValue & "'"

            '存在する場合は更新
            If clsDB_pos.SQL_ExecuteScalar(strsql) <> String.Empty Then

                strsql = String.Empty
                strsql = "update gigi set F003='" & Me.txtEnt.Text & "' where F001='" &
                        _strProjID & "' and F002='" & Me.lst.SelectedValue & "'"

                If MessageBox.Show("置換しますか？",
                                   Application.ProductName,
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question,
                                   MessageBoxDefaultButton.Button1) =
                                   DialogResult.No Then Exit Sub

                clsDB_pos.SQL_Execute(strsql)

            End If

            LoadData()

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
        End Try


    End Sub
#End Region
#Region "削除ボタン"

    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        Dim clsDB_pos As New clsDB_pos
        Dim dt As New DataTable

        Try
            Dim strsql As String = String.Empty

            strsql = String.Empty
            strsql = "delete from gigi where F003='" & lst.Text &
                                        "' and F001='" & _strProjID &
                                        "' and F002='" & lst.SelectedValue & "'"

            If MessageBox.Show("削除しますか？",
                               Application.ProductName,
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Question,
                               MessageBoxDefaultButton.Button1) =
                               DialogResult.No Then Exit Sub

            clsDB_pos.SQL_Execute(strsql)

            '//20170919113557 furukawa st ////////////////////////
            '//疑義削除時の連番抜け対応
            strsql = String.Empty
            strsql = "select * from gigi  where F001='" & _strProjID &
                                        "' order by cast(F002 as integer)"
            dt = clsDB_pos.SQL_Data(strsql)

            '削除後、削除した次の連番を1ずつ上に上げる（1ずつマイナス）
            For r As Integer = Integer.Parse(lst.SelectedValue) + 1 To dt.Rows.Count
                strsql = String.Empty
                strsql = "update gigi set F002=cast('0' || (cast(F002 as integer)-1) as varchar) " &
                "where F001='" & _strProjID &
                "' and F002='" & r.ToString("00") & "'"
                clsDB_pos.SQL_ExecuteScalar(strsql)

            Next
            '//20170919113557 furukawa ed ////////////////////////

            LoadData()

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
        End Try

    End Sub



#End Region

#Region "疑義取得"

    Private Sub LoadData()
        Dim clsDB_pos As New clsDB_pos
        Dim dt As New DataTable
        Try
            Dim strsql As String = String.Empty
            strsql = "select F002,F003 from gigi where F001='" & _strProjID &
                        "' order by cast(F002 as integer) asc"
            dt = clsDB_pos.SQL_Data(strsql)
            lst.DataSource = dt
            lst.DisplayMember = "F003"
            lst.ValueMember = "F002"
            txtEnt.Text = String.Empty

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
        End Try


    End Sub
#End Region

#Region "最大ID取得"

    'IDの最大を取得。なければ00を返す
    Private Function GetMaxID() As String
        Dim clsDB_pos As New clsDB_pos
        Dim id As String = String.Empty

        Try
            Dim strsql As String = String.Empty
            strsql = "select max(cast(F002 as integer))+1 from gigi where F001='" & _strProjID & "'"

            id = clsDB_pos.SQL_ExecuteScalar(strsql)
            If id = String.Empty Then
                Return "00"
            Else
                Return Integer.Parse(id).ToString("00")
            End If

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return String.Empty

        End Try
    End Function

#End Region

End Class
