﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.Runtime.InteropServices
Imports System.Runtime.CompilerServices

Public Class DateTimeEx
    Public Shared DateTimeNull As DateTime = DateTime.MinValue
    Public Shared ErrorMessage As String = String.Empty
    Public Shared Property EraString As String
    Shared EraList As List(Of EraJ) = New List(Of EraJ)()
    Public Shared culture As System.Globalization.CultureInfo

    Private Sub New()
        culture = New System.Globalization.CultureInfo("ja-JP", True)
        culture.DateTimeFormat.Calendar = New System.Globalization.JapaneseCalendar()
        GetEraList()
    End Sub

    Friend Class EraJ
        Friend SDate As DateTime = DateTime.MaxValue
        Friend EDate As DateTime = DateTime.MaxValue
        Friend Name As String = String.Empty
        Friend ShortName As String = String.Empty
        Friend Initial As String = String.Empty
        Friend Number As Integer = 0
        Friend Difference As Integer = 0
        Friend Max As Integer = 0
    End Class

    Private Sub GetEraList()
        Dim m = New EraJ()
        m.SDate = New DateTime(1868, 9, 8)
        m.EDate = New DateTime(1912, 7, 29)
        m.Name = "明治"
        m.ShortName = "明"
        m.Number = 1
        m.Initial = "M"
        m.Difference = 1867
        m.Max = 45
        EraList.Add(m)
        Dim t = New EraJ()
        t.SDate = New DateTime(1912, 7, 30)
        t.EDate = New DateTime(1926, 12, 24)
        t.Name = "大正"
        t.ShortName = "大"
        t.Number = 2
        t.Initial = "T"
        t.Difference = 1911
        t.Max = 15
        EraList.Add(t)
        Dim s = New EraJ()
        s.SDate = New DateTime(1926, 12, 25)
        s.EDate = New DateTime(1989, 1, 7)
        s.Name = "昭和"
        s.ShortName = "昭"
        s.Number = 3
        s.Initial = "S"
        s.Difference = 1925
        s.Max = 64
        EraList.Add(s)
        Dim h = New EraJ()
        h.SDate = New DateTime(1989, 1, 8)
        h.EDate = New DateTime(2019, 4, 30)
        h.Name = "平成"
        h.ShortName = "平"
        h.Number = 4
        h.Initial = "H"
        h.Difference = 1988
        h.Max = 31
        EraList.Add(h)
        Dim r = New EraJ()
        r.SDate = New DateTime(2019, 5, 1)
        r.EDate = DateTime.MaxValue
        r.Name = "令和"
        r.ShortName = "令"
        r.Number = 5
        r.Initial = "R"
        r.Difference = 2018
        r.Max = 99
        EraList.Add(r)

        For Each item In EraList
            EraString += item.ShortName
            EraString += item.Initial
            EraString += item.Initial.ToLower()
        Next
    End Sub

    Public Function GetEraShort(ByVal dt As DateTime) As String
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).ShortName
        Next

        Return String.Empty
    End Function

    Public Function GetInitialEra(ByVal dt As DateTime) As String
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Initial
        Next

        Return String.Empty
    End Function

    Public Function GetEra(ByVal dt As DateTime) As String
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Name
        Next

        Return String.Empty
    End Function

    Public Function GetEraNumber(ByVal dt As DateTime) As Integer
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Number
        Next

        Return 0
    End Function

    Public Function GetJpYear(ByVal dt As DateTime) As Integer
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return dt.Year - EraList(i).Difference
        Next

        Return 0
    End Function

    Public Function GetJpYearFromYM(ByVal yyyyMM As Integer) As Integer
        Dim dt = New DateTime(yyyyMM / 100, yyyyMM Mod 100, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return dt.Year - EraList(i).Difference
        Next

        Return 0
    End Function

    Public Function GetEraJpYearMonth(ByVal dt As DateTime) As String
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Name & (dt.Year - EraList(i).Difference).ToString("00年") & dt.ToString("MM月")
        Next

        Return String.Empty
    End Function

    Public Function GetEraJpYearMonth(ByVal yyyymm As Integer) As String
        Dim dt = New DateTime(yyyymm / 100, yyyymm Mod 100, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Name & (dt.Year - EraList(i).Difference).ToString("00年") & dt.ToString("MM月")
        Next

        Return String.Empty
    End Function

    Public Function GetInitialEraJpYearMonth(ByVal yyyymm As Integer) As String
        Dim y = yyyymm / 100
        Dim m = yyyymm Mod 100
        If Not IsDate(y, m, 1) Then Return String.Empty
        Dim dt = New DateTime(y, m, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Initial & (dt.Year - EraList(i).Difference).ToString("00") & "/" & dt.ToString("MM")
        Next

        Return String.Empty
    End Function

    Function GetEraJpYear(ByVal dt As DateTime) As String
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Name & (dt.Year - EraList(i).Difference).ToString("00")
        Next

        Return String.Empty
    End Function

    Public Function GetInitialEraJpYear(ByVal dt As DateTime) As String
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Initial & (dt.Year - EraList(i).Difference).ToString("00")
        Next

        Return String.Empty
    End Function

    Public Function GetInitialEraJpYear(ByVal yyyymm As Integer) As String
        Dim dt = New DateTime(yyyymm / 100, yyyymm Mod 100, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Initial & (dt.Year - EraList(i).Difference).ToString("00")
        Next

        Return String.Empty
    End Function

    Public Function GetEraNumberYear(ByVal dt As DateTime) As Integer
        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Number * 100 + (dt.Year - EraList(i).Difference)
        Next

        Return 0
    End Function

    Public Function GetEraNumberYearFromYYYYMM(ByVal yyyyMM As Integer) As Integer
        If Not IsDate(yyyyMM / 100, yyyyMM Mod 100, 1) Then Return 0
        Dim dt = New DateTime(yyyyMM / 100, yyyyMM Mod 100, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Number * 100 + (dt.Year - EraList(i).Difference)
        Next

        Return 0
    End Function

    Public Function GetEraNumber(ByVal era As String) As Integer
        Dim number As Integer = 0
        If era Is Nothing OrElse era.Length < 1 OrElse era.Length > 3 Then Return number

        For Each item In EraList

            If era.Length = 1 Then

                If item.ShortName = era Then
                    number = item.Number
                    Exit For
                End If
            Else

                If item.Name = era Then
                    number = item.Number
                    Exit For
                End If
            End If
        Next

        Return number
    End Function

    Function GetAdYear(ByVal jyear As String) As Integer
        Dim year As Integer = 0
        If jyear.Length < 3 Then Return 0
        Dim era As String = jyear.Remove(2)
        If Not Integer.TryParse(jyear.Substring(2), year) Then Return 0

        For Each item In EraList

            If era.Contains(item.Initial) OrElse era.Contains(item.Initial.ToLower()) OrElse era.Contains(item.ShortName) Then
                If item.Max < year Then Return 0
                Return year + item.Difference
            End If
        Next

        Return 0
    End Function

    Function GetAdYearMonthFromJyymm(ByVal jyymm As Integer) As Integer
        If jyymm <= 10000 OrElse 60000 <= jyymm Then Return 0
        Dim era As Integer = jyymm / 10000
        Dim yymm As Integer = jyymm Mod 10000

        If era >= 1 AndAlso era <= 3 Then
            Dim diff As Integer = 0

            For Each item In EraList

                If item.Number = era Then
                    diff = item.Difference
                    Exit For
                End If
            Next

            If diff = 0 Then Return 0

            Return yymm = yymm + diff * 100 'CSharpImpl.__Assign(yymm, diff * 100)
        End If

        If era = 4 OrElse era = 5 Then
            Dim diff As Integer = 0
            Dim y As Integer = yymm / 100
            Dim m As Integer = yymm Mod 100

            For Each item In EraList

                If item.Number = era Then

                    Select Case era
                        Case 4
                            diff = EraList(era - 1).Difference
                            Continue For
                        Case 5

                            If y = 1 AndAlso m >= 5 Then
                                diff = EraList(era - 1).Difference
                                Continue For
                            ElseIf y = 1 AndAlso m <= 4 Then
                                diff = EraList(era - 1).Difference
                                Continue For
                            ElseIf y >= 31 Then
                                diff = EraList(era - 2).Difference
                                Continue For
                            Else
                                diff = EraList(era - 1).Difference
                                Continue For
                            End If
                    End Select
                End If
            Next

            If diff = 0 Then Return 0

            Return yymm = yymm + diff * 100 'CSharpImpl.__Assign(yymm, diff * 100)
        Else
            Return 0
        End If
    End Function

    Function GetJpYearStr(ByVal yearStr4 As String) As String
        Dim year As Integer
        Integer.TryParse(yearStr4, year)
        If year = 0 Then Return String.Empty
        Dim dt = New DateTime(year, 1, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Name & (dt.Year - EraList(i).Difference).ToString("00")
        Next

        Return String.Empty
    End Function

    Function GetJpYearStr(ByVal year As Integer) As String
        If year = 0 Then Return String.Empty
        Dim dt = New DateTime(year, 1, 1)

        For i As Integer = 0 To EraList.Count - 1
            If EraList(i).SDate > dt Then Continue For
            If EraList(i).EDate < dt Then Continue For
            Return EraList(i).Name & (dt.Year - EraList(i).Difference).ToString("00")
        Next

        Return String.Empty
    End Function

    Function TryGetYear(ByVal jyear As String, <Out> ByRef year As Integer) As Boolean
        year = 0
        If jyear.Length < 3 Then Return False
        Dim era As String = jyear.Remove(2)
        If Not Integer.TryParse(jyear.Substring(2), year) Then Return False
        Dim diff As Integer = 0

        For Each item In EraList

            If era.Contains(item.Initial) OrElse era.Contains(item.Initial.ToLower()) OrElse era.Contains(item.ShortName) Then
                diff = item.Difference
                Exit For
            End If
        Next

        If diff = 0 Then
            year = 0
            Return False
        End If

        year += diff
        Return True
    End Function

    Function TryGetYearMonthTimeFromJdate(ByVal jDate As String, <Out> ByRef dt As DateTime) As Boolean
        dt = DateTimeNull
        jDate = jDate.Replace("_"c, " "c)
        Dim sb As StringBuilder = New StringBuilder()
        Const separator As String = "年月・."

        For Each c In jDate

            If separator.Contains(c) Then
                sb.Append("/"c)
            Else
                sb.Append(c)
            End If
        Next

        jDate = sb.ToString()
        sb.Clear()
        jDate = KanaToHalf(jDate)
        Dim dateStr = jDate.Split("/"c)

        If dateStr.Length = 1 Then
            dateStr = New String(1) {}

            If jDate.Length = 6 Then
                dateStr(0) = jDate.Remove(2)
                dateStr(1) = jDate.Substring(2, 2)
            ElseIf jDate.Length = 7 Then
                dateStr(0) = jDate.Remove(3)
                dateStr(1) = jDate.Substring(3, 2)
            ElseIf jDate.Length = 8 Then
                dateStr(0) = jDate.Remove(4)
                dateStr(1) = jDate.Substring(4, 2)
            Else
                Return False
            End If
        End If

        If dateStr.Length < 2 Then Return False
        Const numStr As String = "0123456789"

        For Each c In dateStr(0)
            If numStr.Contains(c) Then sb.Append(c)
        Next

        Dim yearNumStr As String = sb.ToString()

        If yearNumStr.Length = 3 Then
            Dim eraNum As Integer
            If Not Integer.TryParse(yearNumStr.Remove(1), eraNum) Then Return False

            For Each item In EraList

                If item.Number = eraNum Then
                    dateStr(0) = item.Initial & yearNumStr.Substring(1)
                    yearNumStr = yearNumStr.Substring(1)
                    Exit For
                End If
            Next
        End If

        Dim year, month As Integer
        If Not Integer.TryParse(yearNumStr, year) Then Return False
        If Not Integer.TryParse(dateStr(1), month) Then Return False

        For Each item In EraList

            If dateStr(0).Contains(item.Initial) OrElse dateStr(0).Contains(item.Initial.ToLower()) OrElse dateStr(0).Contains(item.ShortName) Then
                year += item.Difference
                Exit For
            End If
        Next

        If Not IsDate(year, month, 1) Then Return False
        dt = New DateTime(year, month, 1)
        Return True
    End Function

    Function GetDateFromJstr7(ByVal jdate7 As String) As DateTime
        Dim jdateInt7 As Integer
        If Not Integer.TryParse(jdate7, jdateInt7) Then Return DateTime.MinValue
        Return GetDateFromJInt7(jdateInt7)
    End Function

    Function GetDateFromJInt7(ByVal jdateInt7 As Integer) As DateTime
        Dim eraNum = jdateInt7 / 1000000
        Dim y = jdateInt7 / 10000 Mod 100

        If jdateInt7 / 100 >= 43105 AndAlso jdateInt7.ToString().Substring(0, 1) <> "5" Then
            eraNum = 5
            y -= 30
        End If

        Dim era = EraList.FirstOrDefault(Function(e) e.Number = eraNum)
        If era Is Nothing Then Return DateTime.MinValue
        y = y + era.Difference
        Dim m = jdateInt7 / 100 Mod 100
        Dim d = jdateInt7 Mod 100
        Return If(IsDate(y, m, d), New DateTime(y, m, d), DateTime.MinValue)
    End Function

    Function TryGetDateTimeFromJdate(ByVal jDate As String, <Out> ByRef dt As DateTime) As Boolean
        dt = DateTimeNull
        jDate = jDate.Replace("_"c, " "c)
        Dim sb As StringBuilder = New StringBuilder()
        Const separator As String = "年月日・."

        For Each c In jDate

            If separator.Contains(c) Then
                sb.Append("/"c)
            Else
                sb.Append(c)
            End If
        Next

        jDate = sb.ToString()
        sb.Clear()
        jDate = KanaToHalf(jDate)
        Dim dateStr = jDate.Split("/"c)

        If dateStr.Length = 1 Then
            dateStr = New String(2) {}

            If jDate.Length = 6 Then
                dateStr(0) = jDate.Remove(2)
                dateStr(1) = jDate.Substring(2, 2)
                dateStr(2) = jDate.Substring(4, 2)
            ElseIf jDate.Length = 7 Then
                dateStr(0) = jDate.Remove(3)
                dateStr(1) = jDate.Substring(3, 2)
                dateStr(2) = jDate.Substring(5, 2)
            ElseIf jDate.Length = 8 Then
                dateStr(0) = jDate.Remove(4)
                dateStr(1) = jDate.Substring(4, 2)
                dateStr(2) = jDate.Substring(6, 2)
            Else
                Return False
            End If
        End If

        If dateStr.Length < 3 Then Return False
        Const numStr As String = "0123456789"

        For Each c In dateStr(0)
            If numStr.Contains(c) Then sb.Append(c)
        Next

        Dim yearNumStr As String = sb.ToString()

        If yearNumStr.Length = 3 Then
            Dim eraNum As Integer
            If Not Integer.TryParse(yearNumStr.Remove(1), eraNum) Then Return False

            For Each item In EraList

                If item.Number = eraNum Then
                    dateStr(0) = item.Initial & yearNumStr.Substring(1)
                    yearNumStr = yearNumStr.Substring(1)
                    Exit For
                End If
            Next
        End If

        Dim year, month, day As Integer
        If Not Integer.TryParse(yearNumStr, year) Then Return False
        If Not Integer.TryParse(dateStr(1), month) Then Return False
        If Not Integer.TryParse(dateStr(2), day) Then Return False

        For Each item In EraList

            If dateStr(0).Contains(item.Initial) OrElse dateStr(0).Contains(item.Initial.ToLower()) OrElse dateStr(0).Contains(item.ShortName) Then
                year += item.Difference
                Exit For
            End If
        Next

        If Not IsDate(year, month, day) Then Return False
        dt = New DateTime(year, month, day)
        Return True
    End Function

    Public Function IsDate(ByVal iYear As Integer, ByVal iMonth As Integer, ByVal iDay As Integer) As Boolean
        If (DateTime.MinValue.Year > iYear) OrElse (iYear > DateTime.MaxValue.Year) Then
            Return False
        End If

        If (DateTime.MinValue.Month > iMonth) OrElse (iMonth > DateTime.MaxValue.Month) Then
            Return False
        End If

        Dim iLastDay As Integer = DateTime.DaysInMonth(iYear, iMonth)

        If (DateTime.MinValue.Day > iDay) OrElse (iDay > iLastDay) Then
            Return False
        End If

        Return True
    End Function

    Function GetJpDateStr(ByVal dt As DateTime) As String
        Dim year = GetEraJpYear(dt)
        If year = String.Empty Then Return String.Empty
        Return year & "年" & dt.Month.ToString("00") & "月" & dt.Day.ToString("00") & "日"
    End Function

    Function GetShortJpDateStr(ByVal dt As DateTime) As String
        Dim year = GetInitialEraJpYear(dt)
        If year = String.Empty Then Return String.Empty
        Return year & "/" & dt.Month.ToString("00") & "/" & dt.Day.ToString("00")
    End Function

    Function GetIntJpDateWithEraNumber(ByVal dt As DateTime) As Integer
        Dim year = GetEraNumberYear(dt)
        If year = 0 Then Return 0
        Return year * 10000 + dt.Month * 100 + dt.Day
    End Function

    Function GetEraName(ByVal codeStr As String) As String
        codeStr = codeStr.ToUpper()
        Dim code As Integer

        For Each item In EraList
            If codeStr.Contains(item.Initial) Then Return item.Name

            If Integer.TryParse(codeStr, code) Then
                If code = item.Number Then Return item.Name
            End If
        Next

        Return String.Empty
    End Function


    Public Function GetTheYear(ByVal dt As DateTime) As Integer
        Dim year As Integer = dt.Year
        If dt.Month < 4 Then year -= 1
        Return year
    End Function


    Public Function GetTheYear2(ByVal dt As DateTime) As Integer
        Dim year As Integer = dt.Year
        If dt.Month < 4 Then year -= 1
        year = year Mod 100
        Return year
    End Function


    Public Function ToDateTime(ByVal dateInt8 As Integer) As DateTime
        Dim year As Integer = dateInt8 / 10000
        Dim month As Integer = (dateInt8 Mod 10000) / 100
        Dim day As Integer = dateInt8 Mod 100
        If Not IsDate(year, month, day) Then Return DateTimeNull
        Return New DateTime(year, month, day)
    End Function


    Public Function ToDateTime6(ByVal dateInt6 As Integer) As DateTime
        Dim year As Integer = dateInt6 / 100
        Dim month As Integer = dateInt6 Mod 100
        Dim day As Integer = 1
        If Not IsDate(year, month, day) Then Return DateTimeNull
        Return New DateTime(year, month, day)
    End Function


    Public Function ToDateTime(ByVal dateStr As String) As DateTime
        Dim dateInt8 As Integer
        If dateStr Is Nothing Then Return DateTimeNull
        If Not Integer.TryParse(dateStr, dateInt8) Then Return DateTimeNull
        Return ToDateTime(dateInt8)
    End Function


    Public Function ToDateTime6(ByVal dateStr6 As String) As DateTime
        Dim dateInt6 As Integer
        If dateStr6 Is Nothing Then Return DateTimeNull
        If Not Integer.TryParse(dateStr6, dateInt6) Then Return DateTimeNull
        Return ToDateTime6(dateInt6)
    End Function


    Public Function ToInt(ByVal dt As DateTime) As Integer
        Dim di As Integer = dt.Day
        di = di + dt.Month * 100
        di = di + dt.Year * 10000
        Return di
    End Function


    Public Function TointWithNullDate(ByVal dt As DateTime) As Integer
        If dt = DateTimeNull Then Return 0
        Dim di As Integer = dt.Day
        di = di + dt.Month * 100
        di = di + dt.Year * 10000
        Return di
    End Function


    Public Function IsNullDate(ByVal dt As DateTime) As Boolean
        Return dt = DateTimeNull
    End Function


    Public Function ToJDateShortStr(ByVal dt As DateTime) As String
        Return GetShortJpDateStr(dt)
    End Function


    Public Function ToJDateStr(ByVal dt As DateTime) As String
        Return GetJpDateStr(dt)
    End Function


    Public Function ToJyearMonthShortStr(ByVal dt As DateTime) As String
        Dim year = GetInitialEraJpYear(dt)
        If year = String.Empty Then Return String.Empty
        Return year & "/" & dt.Month.ToString("00")
    End Function


    Public Function ToJyearShortStr(ByVal dt As DateTime) As String
        Dim year = GetInitialEraJpYear(dt)
        If year = String.Empty Then Return String.Empty
        Return year
    End Function


    Public Function ToJyearMonthStr(ByVal dt As DateTime) As String
        Dim year = GetEraJpYear(dt)
        If year = String.Empty Then Return String.Empty
        Return year & "年" & dt.Month.ToString("00") & "月"
    End Function


    Public Function GetMonthFirstDate(ByVal dt As DateTime) As DateTime
        Return New DateTime(dt.Year, dt.Month, 1)
    End Function

    Private Function KanaToHalf(ByVal str As String) As String
        Return Strings.StrConv(str, VbStrConv.Narrow)
    End Function

    Public Function GetAdYearFromHs(ByVal eemm As Integer) As Integer
        Dim y As Integer = eemm / 100
        GetADYearFromWareki(eemm, y)
        Return y
    End Function

    Private Sub GetADYearFromWareki(ByVal eemm As Integer, <Out> ByRef cng_y As Integer)
        Dim strgeemm As String = eemm.ToString().PadLeft(4, "0"c)
        Dim y As Integer = Integer.Parse(strgeemm.Substring(0, 2))
        Dim m As Integer = Integer.Parse(strgeemm.Substring(2, 2))
        cng_y = y
        Dim clsEraJ As EraJ = New EraJ()

        If y >= 20 AndAlso y < 31 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "H")
            cng_y = y + clsEraJ.Difference
            Return
        End If

        If y = 31 AndAlso m <= 4 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "H")
            cng_y = y + clsEraJ.Difference
            Return
        End If

        If y = 31 AndAlso m >= 5 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "H")
            y -= (clsEraJ.Max - 1)
            Dim diff As EraJ = EraList.First(Function(e) e.Initial = "R")
            cng_y = y + diff.Difference
            Return
        End If

        If y = 1 AndAlso m <= 4 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "H")
            y += (clsEraJ.Max - 1)
            Dim diff As EraJ = EraList.First(Function(e) e.Initial = "H")
            cng_y = y + clsEraJ.Difference
            Return
        End If

        If y = 1 AndAlso m >= 5 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "R")
            cng_y = y + clsEraJ.Difference
            Return
        End If

        If y >= 32 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "H")
            cng_y = y + clsEraJ.Difference
            Return
        End If

        If y > 1 AndAlso m >= 1 Then
            clsEraJ = EraList.First(Function(e) e.Initial = "R")
            cng_y = y + clsEraJ.Difference
            Return
        End If
    End Sub

    Function GetHsYearFromAd(ByVal adYear As Integer, ByVal mm As Integer) As Integer
        Return GetJPYaerFromAD(adYear, mm)
    End Function

    Private Function GetJPYaerFromAD(ByVal adYear As Integer, ByVal mm As Integer) As Integer
        Dim era As EraJ = New EraJ()

        If adYear = 2019 Then
            If mm < 5 Then era = EraList.First(Function(e) e.Initial = "H")
            If mm >= 5 Then era = EraList.First(Function(e) e.Initial = "R")
        Else
            era = EraList.Last(Function(e) e.Difference < adYear)
        End If


        Return adYear = adYear - era.Difference        'CSharpImpl.__Assign(adYear, era.Difference)
    End Function

    Function GetGyymmFromAdYM(ByVal adYearMonth As Integer) As Integer
        Dim y = adYearMonth / 100
        Dim m = adYearMonth Mod 100
        Dim dt = New DateTime(y, m, 1)
        Dim g = GetEraNumber(dt)
        Dim jy = GetJpYear(dt)
        Return g * 10000 + jy * 100 + m
    End Function

    Function GetAge(ByVal birth As DateTime, ByVal dtDate As DateTime) As Integer
        If dtDate < birth Then Return -1
        Dim age As Integer = dtDate.Year - birth.Year

        If dtDate.Month < birth.Month OrElse (dtDate.Month = birth.Month AndAlso dtDate.Day < birth.Day) Then
            age -= 1
        End If

        Return age
    End Function

    Function GetAge(ByVal birth As DateTime, ByVal ym As Integer) As Integer
        Dim y As Integer = ym / 100
        Dim m As Integer = ym Mod 100
        If Not IsDate(y, m, 1) Then Return -1
        Dim dt = New DateTime(y, m, 1)
        If dt < birth Then Return -1
        Dim age As Integer = dt.Year - birth.Year

        If dt.Month < birth.Month OrElse (dt.Month = birth.Month AndAlso dt.Day < birth.Day) Then
            age -= 1
        End If

        Return age
    End Function

    Function GetDateFromAge(ByVal age As Integer) As DateTime
        If age < 0 Then Return DateTimeNull
        Dim now = DateTime.Today
        Dim year = now.Year
        Dim targetYear = year - age
        Dim result = New DateTime(targetYear, now.Month, now.Day)
        Return result
    End Function

    Function Int6YmAddMonth(ByVal yyyymm As Integer, ByVal addmonth As Integer) As Integer
        Dim m As Integer = yyyymm Mod 100
        Dim addy As Integer = addmonth / 12
        Dim addm As Integer = addmonth Mod 12
        yyyymm += addy * 100
        yyyymm += addm
        If 12 < m + addm Then yyyymm += 88
        If m + addm < 1 Then yyyymm -= 88
        Return yyyymm
    End Function

    Function GetPrevEEMM_ChangeGengo(ByVal e As Integer, ByVal m As Integer) As String
        If e = 1 AndAlso m = 5 Then
            Return "3104"
        End If

        Dim eemm2 = ""

        If m = 1 Then
            eemm2 = ((e - 1) * 100 + 12).ToString("0000")
        Else
            eemm2 = (e * 100 + (m - 1)).ToString("0000")
        End If

        Return eemm2
    End Function

    Private Class CSharpImpl
        <Obsolete("Please refactor calling code to use normal Visual Basic assignment")>
        Shared Function __Assign(Of T)(ByRef target As T, value As T) As T
            target = value
            Return value
        End Function
    End Class
End Class
