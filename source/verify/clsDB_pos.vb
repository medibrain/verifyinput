﻿'Imports System.Data.Npgsql.Npgsql

Public Class clsDB_pos

    '//20201015182419 furukawa st ////////////////////////
    '//AWS用接続文字列
    'Dim dbPath As String = Application.StartupPath & "\verify.db"

    'ローカル試験用
    'Dim strConnectionString As String = "server=localhost;port=5450;" &
    '    "database=verifyinput;user id=postgres;password=pass;commandtimeout=60;"

    'AWS
    Dim strConnectionString As String = "server=mejordb.cbr12l83psj3.ap-northeast-1.rds.amazonaws.com;port=5432;" &
        "database=verifyinput;user id=postgres;password=medibrain1128;commandtimeout=60;SSLMode=Require;"
    '//20201015182419 furukawa ed ////////////////////////

#Region "SQL実行（複数、トランザクションあり）"
    ''' <summary>
    ''' SQL実行（複数、トランザクションあり）
    ''' </summary>
    ''' <param name="arrSQL">SQL文の配列</param>
    ''' <param name="trans">トランザクション</param>
    ''' <returns>True:成功、False:失敗</returns>
    ''' <remarks></remarks>
    Public Overloads Function SQL_Execute(ByVal arrSQL As ArrayList,
                                          ByRef trans As Npgsql.NpgsqlTransaction) As Boolean

        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand


        Try

            cn.ConnectionString = strConnectionString
            cn.Open()
            trans = cn.BeginTransaction

            For r As Integer = 0 To arrSQL.Count - 1

                cmd.CommandText = arrSQL(r)
                cmd.Connection = cn
                cmd.ExecuteNonQuery()
            Next

            trans.Commit()

            Return True
        Catch ex As Exception
            trans.Rollback()
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return False

        Finally

            cn.Close()

        End Try

    End Function
#End Region

#Region "SQL実行（単一、トランザクションなし）"
    ''' <summary>
    ''' SQL実行（単一、トランザクションなし）
    ''' </summary>
    ''' <param name="strSQL">SQL文字列</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function SQL_Execute(ByVal strSQL As String) As Boolean
        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand


        Try

            cn.ConnectionString = strConnectionString
            cn.Open()
            cmd.CommandText = strSQL
            cmd.Connection = cn
            cmd.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return False

        Finally

            cn.Close()

        End Try

    End Function
#End Region

#Region "SQL実行（単一結果返す）"
    ''' <summary>
    ''' SQL実行（単一結果返す）
    ''' </summary>
    ''' <param name="strSQL">SQL文字列</param>
    ''' <returns>ExecuteSchalarの戻り値</returns>
    ''' <remarks></remarks>
    Public Function SQL_ExecuteScalar(ByVal strSQL As String) As String
        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim res As String = String.Empty

        Try

            cn.ConnectionString = strConnectionString
            cn.Open()
            cmd.CommandText = strSQL
            cmd.Connection = cn

            If Not IsDBNull(cmd.ExecuteScalar()) Then
                res = cmd.ExecuteScalar()
            End If


            Return res

        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return False

        Finally

            cn.Close()

        End Try

    End Function
#End Region

#Region "SQL実行（データ取得、DataTable返す）"
    ''' <summary>
    ''' SQL実行（データ取得、DataTable返す）
    ''' </summary>
    ''' <param name="strSQL">SQL文字列</param>
    ''' <returns>Fillで取得したDataTable</returns>
    ''' <remarks></remarks>
    Public Function SQL_Data(ByVal strSQL As String) As DataTable

        Dim cn As New Npgsql.NpgsqlConnection
        Dim cmd As New Npgsql.NpgsqlCommand
        Dim res As New DataTable
        Dim da As New Npgsql.NpgsqlDataAdapter

        Try

            cn.ConnectionString = strConnectionString
            cn.Open()
            cmd.CommandText = strSQL
            cmd.Connection = cn
            da.SelectCommand = cmd
            da.Fill(res)

            If Not IsNothing(res) Then
                Return res
            Else
                Return Nothing

            End If



        Catch ex As Exception
            ErrCst.clsErr.GetInst.ErrProc(ex)
            Return Nothing


        Finally

            cn.Close()

        End Try

    End Function
#End Region

End Class
