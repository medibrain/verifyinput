﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMenu))
        Me.cmbMonth = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblVer = New System.Windows.Forms.Label()
        Me.btnGigi = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmbProj = New System.Windows.Forms.ComboBox()
        Me.btnOut = New System.Windows.Forms.Button()
        Me.btnFields = New System.Windows.Forms.Button()
        Me.gb = New System.Windows.Forms.GroupBox()
        Me.r2 = New System.Windows.Forms.RadioButton()
        Me.r1 = New System.Windows.Forms.RadioButton()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtname = New verify.CstTxt()
        Me.gb.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbMonth
        '
        Me.cmbMonth.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbMonth.FormattingEnabled = True
        Me.cmbMonth.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.cmbMonth.Location = New System.Drawing.Point(82, 129)
        Me.cmbMonth.Name = "cmbMonth"
        Me.cmbMonth.Size = New System.Drawing.Size(163, 24)
        Me.cmbMonth.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 86)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "案件名"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 129)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "点検月"
        '
        'btnStart
        '
        Me.btnStart.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnStart.Location = New System.Drawing.Point(130, 229)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(145, 52)
        Me.btnStart.TabIndex = 3
        Me.btnStart.Text = "開始"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDel.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnDel.Location = New System.Drawing.Point(282, 412)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(113, 33)
        Me.btnDel.TabIndex = 5
        Me.btnDel.Text = "案件削除"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnAdd.Location = New System.Drawing.Point(163, 412)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(113, 33)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "案件追加"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("メイリオ", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Purple
        Me.Label3.Location = New System.Drawing.Point(0, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(407, 44)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Medi-very"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVer
        '
        Me.lblVer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblVer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblVer.Location = New System.Drawing.Point(317, 10)
        Me.lblVer.Name = "lblVer"
        Me.lblVer.Size = New System.Drawing.Size(78, 23)
        Me.lblVer.TabIndex = 6
        Me.lblVer.Text = "v"
        Me.lblVer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnGigi
        '
        Me.btnGigi.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGigi.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnGigi.Location = New System.Drawing.Point(282, 451)
        Me.btnGigi.Name = "btnGigi"
        Me.btnGigi.Size = New System.Drawing.Size(113, 33)
        Me.btnGigi.TabIndex = 6
        Me.btnGigi.Text = "疑義内容"
        Me.btnGigi.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 172)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 24)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "入力者"
        '
        'cmbProj
        '
        Me.cmbProj.Font = New System.Drawing.Font("ＭＳ ゴシック", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbProj.FormattingEnabled = True
        Me.cmbProj.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.cmbProj.Location = New System.Drawing.Point(82, 86)
        Me.cmbProj.Name = "cmbProj"
        Me.cmbProj.Size = New System.Drawing.Size(271, 24)
        Me.cmbProj.TabIndex = 0
        '
        'btnOut
        '
        Me.btnOut.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnOut.Location = New System.Drawing.Point(130, 302)
        Me.btnOut.Name = "btnOut"
        Me.btnOut.Size = New System.Drawing.Size(145, 52)
        Me.btnOut.TabIndex = 7
        Me.btnOut.Text = "出力"
        Me.btnOut.UseVisualStyleBackColor = True
        '
        'btnFields
        '
        Me.btnFields.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFields.Font = New System.Drawing.Font("メイリオ", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnFields.Location = New System.Drawing.Point(163, 451)
        Me.btnFields.Name = "btnFields"
        Me.btnFields.Size = New System.Drawing.Size(113, 33)
        Me.btnFields.TabIndex = 8
        Me.btnFields.Text = "入力項目"
        Me.btnFields.UseVisualStyleBackColor = True
        '
        'gb
        '
        Me.gb.Controls.Add(Me.r2)
        Me.gb.Controls.Add(Me.r1)
        Me.gb.Location = New System.Drawing.Point(282, 302)
        Me.gb.Name = "gb"
        Me.gb.Size = New System.Drawing.Size(96, 77)
        Me.gb.TabIndex = 9
        Me.gb.TabStop = False
        Me.gb.Text = "出力ファイル"
        '
        'r2
        '
        Me.r2.AutoSize = True
        Me.r2.Location = New System.Drawing.Point(18, 48)
        Me.r2.Name = "r2"
        Me.r2.Size = New System.Drawing.Size(46, 17)
        Me.r2.TabIndex = 1
        Me.r2.Text = "CSV"
        Me.r2.UseVisualStyleBackColor = True
        '
        'r1
        '
        Me.r1.AutoSize = True
        Me.r1.Checked = True
        Me.r1.Location = New System.Drawing.Point(18, 24)
        Me.r1.Name = "r1"
        Me.r1.Size = New System.Drawing.Size(51, 17)
        Me.r1.TabIndex = 0
        Me.r1.TabStop = True
        Me.r1.Text = "Excel"
        Me.r1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("メイリオ", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(0, 50)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(407, 20)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "ベリファイ入力プログラム"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtname
        '
        Me.txtname.AllowHyphen = False
        Me.txtname.AutoTab = False
        Me.txtname.Font = New System.Drawing.Font("ＭＳ ゴシック", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtname.ImeMode = System.Windows.Forms.ImeMode.[On]
        Me.txtname.Location = New System.Drawing.Point(82, 172)
        Me.txtname.MaxLength = 32
        Me.txtname.Name = "txtname"
        Me.txtname.NumberOnly = False
        Me.txtname.Size = New System.Drawing.Size(163, 22)
        Me.txtname.TabIndex = 2
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Honeydew
        Me.ClientSize = New System.Drawing.Size(407, 492)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.gb)
        Me.Controls.Add(Me.btnFields)
        Me.Controls.Add(Me.btnOut)
        Me.Controls.Add(Me.cmbProj)
        Me.Controls.Add(Me.txtname)
        Me.Controls.Add(Me.btnGigi)
        Me.Controls.Add(Me.lblVer)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbMonth)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "メニュー"
        Me.gb.ResumeLayout(False)
        Me.gb.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbMonth As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnStart As System.Windows.Forms.Button
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblVer As System.Windows.Forms.Label
    Friend WithEvents btnGigi As System.Windows.Forms.Button
    Friend WithEvents txtname As verify.CstTxt
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbProj As System.Windows.Forms.ComboBox
    Friend WithEvents btnOut As System.Windows.Forms.Button
    Friend WithEvents btnFields As System.Windows.Forms.Button
    Friend WithEvents gb As System.Windows.Forms.GroupBox
    Friend WithEvents r2 As System.Windows.Forms.RadioButton
    Friend WithEvents r1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
