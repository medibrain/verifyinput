﻿Imports System.Text.RegularExpressions

Public Class CstTxt
    Inherits TextBox

    Dim flgNumberOnly As Boolean '数字だけ
    Dim flgAllowHyphen As Boolean
    Dim flgAutoTab As Boolean

    ''' <summary>
    ''' 数字のみ入力可能
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property NumberOnly() As Boolean
        Get
            Return flgNumberOnly
        End Get
        Set(ByVal value As Boolean)
            flgNumberOnly = value
        End Set
    End Property

    ''' <summary>
    ''' ハイフン入力可能
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AllowHyphen() As Boolean
        Get
            Return flgAllowHyphen

        End Get
        Set(ByVal value As Boolean)
            flgAllowHyphen = value

        End Set
    End Property

    ''' <summary>
    ''' 最大文字列長に達したら自動タブ
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property AutoTab() As Boolean
        Get
            Return flgAutoTab
        End Get
        Set(ByVal value As Boolean)
            flgAutoTab = value
        End Set
    End Property

    Private Sub CstTxt_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        If flgAutoTab Then
            'タブとシフト以外の場合、最大文字列長＝文字列長となったらフォーカス移動
            If e.KeyCode <> Keys.Tab And e.KeyCode <> Keys.ShiftKey Then
                If Me.Text.Length = Me.MaxLength Then
                    SendKeys.Send("{tab}")
                End If
            End If
        End If

    End Sub

    Private Sub txt_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Validated
        Dim chkstring As String = String.Empty

        '一文字ずつ調べる
        For c As Integer = 0 To Me.Text.Length - 1
            Dim tmpchar As String = Me.Text.Substring(c, 1)

            '数字/記号の場合だけ半角
            If Regex.IsMatch(tmpchar, "^[ａ-ｚＡ-Ｚ０-９！＃＄％＆’（）｜｛｝｀＊？＿]+$") Then
                tmpchar = StrConv(tmpchar, VbStrConv.Narrow)
            End If
       
            chkstring &= tmpchar
        Next
        Me.Text = chkstring
        Me.Text = Me.Text.Trim
    End Sub

    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus
        If Not flgAutoTab Then
            Me.SelectionStart = Me.Text.Length
        Else
            Me.SelectAll()
        End If

    End Sub

    Private Sub txt_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        'ALT+eの場合は問答無用で通す
        If e.Alt Then
            If e.KeyCode = Keys.E Then
                e.Handled = False
                Exit Sub
            End If
        End If


        Select Case True
            Case flgNumberOnly And Not AllowHyphen  '数字だけの時制御

                If (e.KeyCode >= Keys.NumPad0 And e.KeyCode <= Keys.NumPad9) Then
                    'テンキーの場合入力許可
                    e.Handled = False
                ElseIf (e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.D9) Then
                    '数字キーの場合入力許可
                    e.Handled = False
                ElseIf (e.KeyCode = Keys.Back Or e.KeyCode = Keys.Delete Or e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right) Then
                    'BS、Delete、左右の場合入力許可
                    e.Handled = False

                Else
                    '上記以外入力不可
                    e.Handled = True
                    e.SuppressKeyPress = True
                End If

            Case flgNumberOnly And AllowHyphen  '数字とハイフンの時制御


                If (e.KeyCode >= Keys.NumPad0 And e.KeyCode <= Keys.NumPad9) Then
                    'テンキーの場合入力許可
                    e.Handled = False
                ElseIf (e.KeyCode >= Keys.D0 And e.KeyCode <= Keys.D9) Then
                    '数字キーの場合入力許可
                    e.Handled = False
                ElseIf (e.KeyCode = Keys.Back Or e.KeyCode = Keys.Delete Or _
                        e.KeyCode = Keys.Left Or e.KeyCode = Keys.Right Or _
                        e.KeyCode = Keys.OemMinus Or e.KeyCode = Keys.Subtract) Then
                    'BS、Delete、左右,-の場合入力許可
                    e.Handled = False
                Else
                    '上記以外入力不可
                    e.Handled = True
                    e.SuppressKeyPress = True
                End If

        End Select

        'Enter時タブを送る
        If e.KeyCode = Keys.Enter Then SendKeys.Send("{TAB}")

     
    End Sub

End Class
